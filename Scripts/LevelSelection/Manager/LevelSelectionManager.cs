using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Handles the proper selection of a level before editing can begin.
    /// 
    /// Author: Kevin Andersch
    /// Date: 31.03.22
    /// Version: 1.0
    /// </summary>
    public class LevelSelectionManager : MonoBehaviour {
        [Header("References")]
        [SerializeField] private LevelSerializationManager levelSerializationManager;
        [SerializeField] private PopupUtility popupUtility;
        [SerializeField] private LevelEditor levelEditor;

        [Header("UI Setup")]
        [SerializeField] private GameObject levelSelectionPanel;
        [SerializeField] private TextMeshProUGUI levelName;
        [SerializeField] private TextMeshProUGUI levelDifficulty;
        [SerializeField] private TextMeshProUGUI levelInitialLanes;

        private void Start() {
            UpdateLevelInfoUI();
        }

        /// <summary>
        /// Opens a popup dropdown from which the user can select an already created level.
        /// Once selected, the level will be loaded.
        /// </summary>
        public void OpenSelectLevelAssetDropdown() {
            List<string> levelNames = LevelSerializationManager.GetAllLevelNames();
            popupUtility.ShowDropdownPopup("Select Level", levelNames, 0, (choice, index) => {
                if(choice == 1) return;

                levelSerializationManager.LoadLevelAndLevelAsset(levelNames[index], LevelDifficulty.Easy);
                UpdateLevelInfoUI();
            }, "Select", "Cancel");
        }

        /// <summary>
        /// Opens the file explorer for the user to choose a .level file.
        /// Once chosen, checks if that file is within the resources folder and if so loads it.
        /// </summary>
        public void OpenSelectLevelAssetFileExplorer() {
            // TODO
        }

        /// <summary>
        /// Promts the user to choose to create a completely new level without data or a copy
        /// of the currently active level and creates a new one accordingly.
        /// The new level will then become the current one.
        /// </summary>
        public void CreateNewLevel() {
            popupUtility.ShowPopup("Create New Level", "You are about to create a new level." +
                "\nDo you want to create a blank one and start from scratch or" +
                "\ncopy the values of the currently level over?", new string[] { "Create Blank", "Create Copy", "Cancel" },
                (choice) => {
                    if(choice == 2) return;

                    levelSerializationManager.CreateNewLevel(choice == 0);
                    UpdateLevelInfoUI();
                });
        }

        /// <summary>
        /// Either begins the edit of the chosen level, opening the actual level editor for it,
        /// or creates a new level if non has been set.
        /// </summary>
        public void BeginEdit() {
            levelSelectionPanel.SetActive(false);
            levelEditor.OpenEditor();
        }

        public void BeginPlayback() {

        }

        public void OpenLevelSelection() {
            levelSelectionPanel.SetActive(true);
        }

        private void UpdateLevelInfoUI() {
            LevelData level = levelSerializationManager.Level;
            levelName.text = level.Name;
            levelDifficulty.text = level.Difficulty.ToString();
            levelInitialLanes.text = level.InitialNumberOfLanes.ToString();
        }
    }
}
