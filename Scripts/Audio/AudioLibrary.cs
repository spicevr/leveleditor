using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

/// <summary>
/// Author: Kevin Andersch
/// Date: 07.06.22
/// Version: 1.0
/// </summary>
[CreateAssetMenu(fileName = "New Audio Library", menuName = "Audio/Audio Library")]
public class AudioLibrary : ScriptableObject {
    [field: SerializeField] public AudioTrack[] Tracks { get; private set; }

    private void OnValidate() {
        CheckIfTrackIDsAreUnique();
    }

    /// <summary>
    /// Checks if the tracks of this library are unique and if there only ever is
    /// one trackID associated with one song.
    /// Does not return a boolean but prints an error if duplicates are found.
    /// </summary>
    public void CheckIfTrackIDsAreUnique() {
        HashSet<uint> hashSet = new HashSet<uint>();
        foreach(AudioTrack track in Tracks) {
            if(track == null) continue;
            if(hashSet.Contains(track.TrackID)) {
                Debug.LogError($"WARNING: There are non unique track IDs in this library: {track.TrackID}");
            } else hashSet.Add(track.TrackID);
        }
    }

    /// <summary>
    /// Returns the track with the passed in trackID.
    /// Returns null if no track matches the trackID.
    /// </summary>
    /// <param name="trackID">The trackID of the track to get.</param>
    public AudioTrack GetTrack(uint trackID) {
        foreach(AudioTrack track in Tracks) {
            if(track.TrackID == trackID) return track;
        }

        return null;
    }
}
