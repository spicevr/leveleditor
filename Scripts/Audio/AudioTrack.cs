using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using UnityEditor;

/// <summary>
/// Author: Kevin Andersch
/// Date: 07.06.22
/// Version: 1.0
/// </summary>
[CreateAssetMenu(fileName = "New Audio Track", menuName = "Audio/Audio Track")]
public class AudioTrack : ScriptableObject {
    [OnValueChanged(nameof(OnTrackIDChanged))] public uint TrackID;
    [OnValueChanged(nameof(OnTrackNameChanged))] public string TrackName = "New Audio Track";
    [OnValueChanged(nameof(OnAudioClipChanged))] public AudioClip Clip;

    [Button("Rename")]
    public void RenameTrackFile() {
        string trackName = name;
        if(trackName.StartsWith("[")) {
            string[] nameParts = trackName.Split('-');
            if(nameParts.Length == 2) {
                trackName = nameParts[1].Substring(1);
            } else {
                Debug.LogError("Error during renaming. Can't determine actual track name.");
            }
        }

        name = $"[{TrackID}] - {trackName}";

#if UNITY_EDITOR
        string path = AssetDatabase.GetAssetPath(this);
        AssetDatabase.RenameAsset(path, name);
#endif

        name = $"[{TrackID}] - {trackName}";
    }

    public void RenameTrackFile(string newName) {
        name = $"[{TrackID}] - {newName}";

#if UNITY_EDITOR
        string path = AssetDatabase.GetAssetPath(this);
        AssetDatabase.RenameAsset(path, name);
#endif

        name = $"[{TrackID}] - {newName}";
    }
    public void RenameTrackFile(uint newID) {
        name = $"[{newID}] - {TrackName}";

#if UNITY_EDITOR
        string path = AssetDatabase.GetAssetPath(this);
        AssetDatabase.RenameAsset(path, name);
#endif

        name = $"[{newID}] - {TrackName}";
    }

    private void OnTrackNameChanged() {
        RenameTrackFile(TrackName);
    }

    private void OnTrackIDChanged() {
        RenameTrackFile(TrackID);
    }

    private void OnAudioClipChanged() {
        if(TrackName.Equals("New Audio Track")) {
            TrackName = Clip.name;
            RenameTrackFile(TrackName);
        }
    }
}
