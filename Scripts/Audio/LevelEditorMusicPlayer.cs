using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Author: Kevin Andersch
    /// Date: 26.04.22
    /// Version: 1.0
    /// </summary>
    public class LevelEditorMusicPlayer : MonoBehaviour {
        public static LevelEditorMusicPlayer Instance { get; private set; }

        [Header("References")]
        [SerializeField] private PopupUtility popupUtility;
        [SerializeField] private AudioClip emptyAudioClip;

        [Header("Music Setup")]
        [SerializeField] private AudioLibrary audioLibrary;
        [SerializeField] private AudioSource backgroundMusicSource;

        [Header("SFX Setup")]
        public AudioClip BPMAdjustmentSound;
        public AudioClip PlaybackNormalNoteSound;
        public AudioClip PlaybackGazeNoteSound;
        public AudioClip PlaybackHoldNoteSound;

        private uint? trackID = null;

        private void Awake() {
            if(!Instance) Instance = this;
            else Destroy(gameObject);
        }

        private void Start() {
            audioLibrary.CheckIfTrackIDsAreUnique();
        }

        /// <summary>
        /// Changes the track ID for the music to be played for the level.
        /// Returns whether the track ID was set or not.
        /// </summary>
        /// <param name="trackID">The trackID of the new song.</param>
        /// <param name="errorType">What type of error should be displayed if no track with such an ID exists.</param>
        public bool SetTrack(uint trackID, ErrorMessageDisplayType errorType = ErrorMessageDisplayType.Ignore) {
            AudioTrack track = audioLibrary.GetTrack(trackID);
            if(track == null) {
                switch(errorType) {
                    case ErrorMessageDisplayType.Console:
                        Debug.LogError($"Tried to set the trackID to {trackID} but no such track exists in the library!");
                        break;
                    case ErrorMessageDisplayType.OnScreen:
                        popupUtility.ShowOnScreenMessage($"Tried to set the trackID to {trackID} but no such track exists in the library!", isErrorMessage: true);
                        break;
                    case ErrorMessageDisplayType.Popup:
                        popupUtility.ShowPopup("Invalid Track ID", $"Tried to set the trackID to {trackID} but no such track exists in the library!",
                            isErrorPopup: true);
                        break;
                }

                return false;
            }

            this.trackID = trackID;
            backgroundMusicSource.clip = track.Clip;
            return true;
        }

        /// <summary>
        /// Sets the music volume to the passed in value.
        /// </summary>
        /// <param name="value">The new volume to have.</param>
        public void SetBackgroundMusicVolume(float value) {
            backgroundMusicSource.volume = value;
        }

        /// <summary>
        /// Starts playing music based on the track ID set previously.
        /// </summary>
        /// <param name="startTime">The time of the song to start at (in seconds).</param>
        /// <param name="errorType">What type of error should be displayed if no audio track has been set previously.</param>
        public void PlayBackgroundMusic(float startTime = 0f, ErrorMessageDisplayType errorType = ErrorMessageDisplayType.Ignore) {
            if(trackID == null) {
                switch(errorType) {
                    case ErrorMessageDisplayType.Ignore:
                        backgroundMusicSource.clip = emptyAudioClip;
                        backgroundMusicSource.time = startTime;
                        backgroundMusicSource.Play();
                        break;
                    case ErrorMessageDisplayType.Console:
                        Debug.LogError($"Tried to play music but no song has been set yet!");
                        break;
                    case ErrorMessageDisplayType.OnScreen:
                        popupUtility.ShowOnScreenMessage($"Tried to play music but no song has been set yet!", isErrorMessage: true);
                        break;
                    case ErrorMessageDisplayType.Popup:
                        popupUtility.ShowPopup("No Music Selected", $"Tried to play music but no song has been set yet!",
                            isErrorPopup: true);
                        break;
                }
                return;
            }
            backgroundMusicSource.time = startTime;
            backgroundMusicSource.Play();
        }

        /// <summary>
        /// Stops playing music.
        /// </summary>
        public void StopBackgroundMusic() {
            backgroundMusicSource.Stop();
        }

        public float GetCurrentSongTime() => backgroundMusicSource.time;

        /// <summary>
        /// Sets the pitch (speed) of the background music.
        /// </summary>
        /// <param name="value">The new pitch to have.</param>
        public void SetPitch(float value) => backgroundMusicSource.pitch = value;
    }
}
