using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ArcBreak.LevelEditor {
    /// <summary>
    /// A note to be placed on the level editor.
    /// 
    /// Author: Kevin Andersch
    /// Date: 07.04.22
    /// Version: 1.0
    /// </summary>
    public struct EditorNote {
        public NoteType Type;
        public int BeatIndex;
        public int Lane;
        public EditorChildNote[] ChildNotes;
        public int NewLaneCountOnCompletion;
        public NoteEvent EventType;
        public byte StoryEventID;
    }

    public struct EditorChildNote {
        public int BeatIndex;
        public int Lane;

        public EditorChildNote(EditorNote note) {
            this.BeatIndex = note.BeatIndex;
            this.Lane = note.Lane;
        }
    }
}
