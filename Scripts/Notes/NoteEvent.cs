/// <summary>
/// WARNING: new cases are to be added AFTER all other ones to not break serialization!
/// 
/// Author: Kevin Andersch
/// Date: 30.05.22
/// Version: 1.0
/// </summary>
public enum NoteEvent {
    None,
    Story,
    PointIncrease,
    HitsoundChange,
    EverythingIsGazeNote,
    EverythingIsPerfectHit
}
