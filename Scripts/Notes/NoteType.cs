/// <summary>
/// Author: Kevin Andersch
/// Date: 28.03.22
/// Version: 1.0
/// </summary>
[System.Serializable]
public enum NoteType {
    Normal,
    Hold,
    Gaze,
    Event,
    LaneSwitch
}
