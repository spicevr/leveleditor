/// <summary>
/// Generic information about special notes for dynamic serialization in the future.
/// WARNING: Do not change this script unless given permission!
/// 
/// Author: Kevin Andersch
/// Date: 31.03.22
/// Version: 1.0
/// </summary>
[System.Serializable]
public struct SpecialNoteData {
    public int BeatIndex;
    public int Lane;
    public int LaneSwitchCount;
    public NoteEvent EventType;
    public byte StoryEventID;
}
