/// <summary>
/// The data of a note for the purpose of serialization.
/// WARNING: Do not change this script unless given permission to do so!
/// 
/// Author: Kevin Andersch
/// Date: 29.03.22
/// Version: 1.0
/// </summary>
[System.Serializable]
public struct NoteData {
    public int BeatIndex;
    /// <summary>
    /// The leftmost lane is at index 0. Then increments by 1 each step to the right.
    /// </summary>
    public int Lane;
    public NoteType Type;
    /// <summary>
    /// Data for notetypes that require special data.
    /// If only one data is required, the data will be at index 0;
    /// </summary>
    public SpecialNoteData[] SpecialNoteData;
    public bool IsMajorBeatNote;
}
