using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Author: Kevin Andersch
    /// Date: 11.04.22
    /// Version: 1.0
    /// </summary>
    public enum BeatMarkerTargetingMode {
        NormalBeatsOnly,
        AllBeats
    }
}
