namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Author: Kevin Andersch
    /// Date: 14.04.22
    /// Version: 1.0
    /// </summary>
    public enum ErrorMessageDisplayType {
        // dont show any errors if they occur
        Ignore,
        // debug.log it to the unity console
        Console,
        // show a timed message on screen
        OnScreen,
        // show a popup window that the user has to click away
        Popup
    }
}
