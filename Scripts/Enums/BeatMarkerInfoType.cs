namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Author: Kevin Andersch
    /// Date: 13.05.22
    /// Version: 1.0
    /// </summary>
    public enum BeatMarkerInfoType {
        None,
        BeatIndex,
        Time
    }
}
