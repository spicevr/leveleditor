namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Author: Kevin Andersch
    /// Date: 07.04.22
    /// Version: 1.0
    /// </summary>
    [System.Serializable]
    public enum NotePlacementLocation {
        Top,
        Bottom,
        UnderCursor
    }
}
