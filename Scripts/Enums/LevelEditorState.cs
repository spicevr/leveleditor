namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Author: Kevin Andersch
    /// Date: 09.05.22
    /// Version: 1.0
    /// </summary>
    public enum LevelEditorState {
        BasicNoteEditing,
        HoldNotePlacement
    }
}
