namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Author: Kevin Andersch
    /// Date: 07.04.22
    /// Version: 1.0
    /// </summary>
    [System.Serializable]
    public class LevelEditorSettingsData {
        public NotePlacementLocation NotePlacementLocation;
        public int BeatMarkerCount;
        public string LastLevelNameWorkedOn;
        public LevelDifficulty LastLevelDifficultyWorkedOn;
        public float BPMAdjustmentIndicatorVolume;
        public float BPMAdjustmentBackgroundAudioVolume;
        public float PlaybackMusicVolume;
        public float PlaybackNoteVolume;
        public bool ShowNotePreview;
        public BeatMarkerInfoType BeatMarkerInfoType;
        public bool ScrollingStepDistanceIsMajorBeats = true;
    }
}
