using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Author: Kevin Andersch
    /// Date: 10.05.22
    /// Version: 1.0
    /// </summary>
    public class HoldNoteVisuals : MonoBehaviour {
        [SerializeField] private RectTransform rect;
        [SerializeField] private GameObject highlight;

        private EditorNote previousNote;
        private EditorNote nextNote;
        private float bounds;

        private void OnEnable() {
            bounds = rect.sizeDelta.x / 2f;
        }

        private void Update() {
            if(IsPointerOverObject()) ToggleHighlight(true);
            else ToggleHighlight(false);
        }

        public void ToggleHighlight(bool on) => highlight.SetActive(on);

        private bool IsPointerOverObject() {
            return Vector2.Distance(transform.position, Input.mousePosition) <= bounds;
        }
    }
}
