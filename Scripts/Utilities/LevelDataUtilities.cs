using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Author: Kevin Andersch
    /// Date: 04.04.22
    /// Version: 1.0
    /// </summary>
    public static class LevelDataUtilities {
        /// <summary>
        /// Checks whether the given index for a NoteType-enum requires additional data in order to work.
        /// </summary>
        /// <param name="enumIndex">The index of the NoteType enum value.</param>
        public static bool CheckIfNoteTypeRequiresSpecialData(int enumIndex) {
            NoteType noteType = (NoteType)enumIndex;
            return !(noteType == NoteType.Gaze || noteType == NoteType.Normal);
        }

        /// <summary>
        /// Converts the given beatIndex of a noteData into a float time that can be used to spawn the
        /// note and compare it to time.time.
        /// </summary>
        /// <param name="beatIndex">The serialized beatIndex of the given noteData.</param>
        /// <param name="bpm">The bpm value of the levelData the note is in.</param>
        [System.Obsolete("This method is obsolete and cannot correctly calculate the time. Use the variant with levelData instead.")]
        public static float ConvertBeatIndexToTime(int beatIndex, int bpm) {
            return beatIndex * 4f;
        }

        /// <summary>
        /// Converts the given beatIndex of a noteData into a float time that can be used to spawn the
        /// note and compare it to time.time.
        /// </summary>
        /// <param name="beatIndex">The beatIndex of the note to get the time for.</param>
        /// <param name="levelData">The data of the level in which the note is saved.</param>
        public static float ConvertBeatIndexToTime(int beatIndex, in LevelData levelData) {
            float mainBeatTime = GetBPMFrequency(levelData.Bpm);
            float minorBeatTime = mainBeatTime / (levelData.MinorPerMajorBeatCount + 1);
            int mainBeatNumber = beatIndex / (levelData.MinorPerMajorBeatCount + 1);
            int minorBeatNumber = beatIndex - (mainBeatNumber * (levelData.MinorPerMajorBeatCount + 1));
            return mainBeatNumber * mainBeatTime + minorBeatNumber * minorBeatTime;
        }

        /// <summary>
        /// Calculates the time at which the passed in major beat is played, relevant to
        /// the passed in bpm.
        /// </summary>
        /// <param name="majorBeat">The major beat to get the time for.</param>
        /// <param name="bpm">The bpm in which the major beat lies.</param>
        public static float GetTimeOfMajorBeat(int majorBeat, int bpm) {
            float mainBeatTime = GetBPMFrequency(bpm);
            return majorBeat * mainBeatTime;
        }

        /// <summary>
        /// Calculates how much time in seconds a beat takes for the passed in bpm number.
        /// </summary>
        /// <param name="bpm">The passed in bpm number.</param>
        public static float GetBPMFrequency(int bpm) => 60f / bpm;
    }
}
