using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Author: Kevin Andersch
    /// Date: 22.04.22
    /// Version: 1.0
    /// </summary>
    public class OnClickInvoker : MonoBehaviour, IPointerClickHandler {
        [SerializeField] private UnityEvent onClickEvent;

        public void OnPointerClick(PointerEventData eventData) {
            onClickEvent?.Invoke();
        }
    }
}
