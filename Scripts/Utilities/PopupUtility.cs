using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;

namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Provides utilities for ingame popups and dialog options.
    /// 
    /// Author: Kevin Andersch
    /// Date: 31.03.22
    /// Version: 1.0
    /// </summary>
    public class PopupUtility : MonoBehaviour {
        [Header("References")]
        [SerializeField] private GameObject popupGameObject;
        [SerializeField] private GameObject buttonPrefab;
        [SerializeField] private HorizontalLayoutGroup buttonLayoutGroup;
        [SerializeField] private VerticalLayoutGroup popupLayoutGroup;
        [SerializeField] private GameObject[] errorUI;
        [SerializeField] private TextMeshProUGUI onScreenMessage;
        [SerializeField] private RectTransform onScreenMessageRectTransform;

        [Header("Popup UI Setup")]
        [SerializeField] private TextMeshProUGUI title;
        [SerializeField] private TextMeshProUGUI message;
        [SerializeField] private Transform buttonHolder;
        [SerializeField] private TMP_InputField stringInputField;
        [SerializeField] private TMP_InputField intInputField;
        [SerializeField] private TMP_Dropdown dropdown;

        [Header("On Screen Message Setup")]
        [SerializeField] private float popInDuration;

        public static bool PopupIsActive { get; private set; }

        public static System.Action OnPopupOpened;
        public static System.Action OnPopupClosed;

        private System.Action OnPopupClosedSinglePrompt;
        private System.Action<bool> OnPopupClosedDoublePrompt;
        private System.Action<int> OnPopupClosedMultiplePrompt;
        /// <summary>
        /// 0: the pressed button
        /// 1: what was written to the text field
        /// </summary>
        private System.Action<int, string> OnPopupClosedStringInputPrompt;
        /// <summary>
        /// 0: the pressed button
        /// 1: what was written to the text field (casted to int, null if not castable)
        /// </summary>
        private System.Action<int, int?> OnPopupClosedIntInputPrompt;
        /// <summary>
        /// 0: the pressed button
        /// 1: the selected index of the dropdown
        /// </summary>
        private System.Action<int, int> OnPopupClosedDropdownPrompt;

        private List<GameObject> activeButtons;
        private Queue<System.Action> pendingPoppups;

        /// <summary>
        /// Shows a simple popup with the given title and message.
        /// The popup only has one button (with "Ok") that closes it.
        /// </summary>
        /// <param name="title">The title to be displayed.</param>
        /// <param name="message">The message to be displayed.</param>
        /// <param name="onCloseAction">Action invoked after closing the popup.</param>
        /// <param name="isErrorPopup">Whether this popup is an error message (and should be visually highlighted).</param>
        public void ShowPopup(string title, string message, System.Action onCloseAction = null, bool isErrorPopup = false) {
            if(PopupIsActive) {
                if(pendingPoppups == null) pendingPoppups = new Queue<System.Action>();
                pendingPoppups.Enqueue(() => {
                    ShowPopup(title, message, onCloseAction, isErrorPopup);
                });
                return;
            }

            if(activeButtons == null) activeButtons = new List<GameObject>();
            this.title.text = title;
            this.message.text = message;
            ClearInvocationList();
            OnPopupClosedSinglePrompt += onCloseAction;
            for(int i = 0; i < errorUI.Length; i++) {
                errorUI[i].SetActive(isErrorPopup);
            }
            AddButton("Ok");

            ActivatePopupComponents();
        }

        /// <summary>
        /// Shows a simple popup with the given title and message.
        /// The popup can be accepted or declined, invoking the onCloseAction with true or false (respectively).
        /// </summary>
        /// <param name="title">The title to be displayed.</param>
        /// <param name="message">The message to be displayed.</param>
        /// <param name="acceptButtonText">The text for the accept button.</param>
        /// <param name="declineButtonText">The text for the decline button.</param>
        /// <param name="onCloseAction">Action invoked after closing the popup. True if accepted, false if declined.</param>
        /// <param name="isErrorPopup">Whether this popup is an error message (and should be visually highlighted).</param>
        public void ShowPopup(string title, string message, string acceptButtonText, string declineButtonText, System.Action<bool> onCloseAction = null, bool isErrorPopup = false) {
            if(PopupIsActive) {
                if(pendingPoppups == null) pendingPoppups = new Queue<System.Action>();
                pendingPoppups.Enqueue(() => {
                    ShowPopup(title, message, acceptButtonText, declineButtonText, onCloseAction, isErrorPopup);
                });
                return;
            }

            if(activeButtons == null) activeButtons = new List<GameObject>();
            this.title.text = title;
            this.message.text = message;
            ClearInvocationList();
            OnPopupClosedDoublePrompt += onCloseAction;
            for(int i = 0; i < errorUI.Length; i++) {
                errorUI[i].SetActive(isErrorPopup);
            }
            AddButton(acceptButtonText);
            AddButton(declineButtonText, 1);

            ActivatePopupComponents();
        }

        /// <summary>
        /// Shows a simple popup with the given title and message.
        /// The popup has several buttons with the choices given in the string array.
        /// The onCloseAction is invoked with the index of the choice in the choice array.
        /// </summary>
        /// <param name="title">The title to be displayed.</param>
        /// <param name="message">The message to be displayed.</param>
        /// <param name="choiceTexts">The button choices to be displayed.</param>
        /// <param name="onCloseAction">Action invoked after closing the popup. Invoked with the index of the choice that was clicked from the choices array.</param>
        /// <param name="isErrorPopup">Whether this popup is an error message (and should be visually highlighted).</param>
        public void ShowPopup(string title, string message, string[] choiceTexts, System.Action<int> onCloseAction = null, bool isErrorPopup = false) {
            if(PopupIsActive) {
                if(pendingPoppups == null) pendingPoppups = new Queue<System.Action>();
                pendingPoppups.Enqueue(() => {
                    ShowPopup(title, message, choiceTexts, onCloseAction, isErrorPopup);
                });
                return;
            }

            if(activeButtons == null) activeButtons = new List<GameObject>();
            this.title.text = title;
            this.message.text = message;
            ClearInvocationList();
            OnPopupClosedMultiplePrompt += onCloseAction;
            for(int i = 0; i < errorUI.Length; i++) {
                errorUI[i].SetActive(isErrorPopup);
            }
            if(choiceTexts == null || choiceTexts.Length < 2) {
                AddButton("Ok");
            } else {
                for(int i = 0; i < choiceTexts.Length; i++) {
                    AddButton(choiceTexts[i], i);
                }
            }

            ActivatePopupComponents();
        }

        /// <summary>
        /// Shows a popup with an input field into which the user can type a string.
        /// </summary>
        /// <param name="title">The title for this popup.</param>
        /// <param name="defaultText">The initial text of the input field.</param>
        /// <param name="onCloseAction">Called whith the button that was used to close this popup and the final text of the input field.</param>
        /// <param name="acceptButtonText">The text for the accept button.</param>
        /// <param name="declineButtonText">The text for the decline button.</param>
        public void ShowInputPopup(string title, string defaultText, System.Action<int, string> onCloseAction = null,
            string acceptButtonText = "Change", string declineButtonText = "Discard") {
            if(activeButtons == null) activeButtons = new List<GameObject>();
            this.title.text = title;
            stringInputField.text = defaultText;
            ClearInvocationList();
            OnPopupClosedStringInputPrompt += onCloseAction;
            for(int i = 0; i < errorUI.Length; i++) {
                errorUI[i].SetActive(false);
            }
            AddButton(acceptButtonText);
            AddButton(declineButtonText, 1);

            ActivatePopupComponents(stringInputField.gameObject);
        }

        /// <summary>
        /// Shows a popup with an input field into which the user can type only integers.
        /// </summary>
        /// <param name="title">The title for this popup.</param>
        /// <param name="defaultText">The initial number of the input field.</param>
        /// <param name="onCloseAction">Called whith the button that was used to close this popup and the final number of the input field.</param>
        /// <param name="acceptButtonText">The text for the accept button.</param>
        /// <param name="declineButtonText">The text for the decline button.</param>
        public void ShowInputPopup(string title, int defaultText, System.Action<int, int?> onCloseAction = null,
            string acceptButtonText = "Change", string declineButtonText = "Discard") {
            if(activeButtons == null) activeButtons = new List<GameObject>();
            this.title.text = title;
            intInputField.text = defaultText.ToString();
            ClearInvocationList();
            OnPopupClosedIntInputPrompt += onCloseAction;
            for(int i = 0; i < errorUI.Length; i++) {
                errorUI[i].SetActive(false);
            }
            AddButton(acceptButtonText);
            AddButton(declineButtonText, 1);

            ActivatePopupComponents(intInputField.gameObject);
        }

        /// <summary>
        /// Shows a popup with a dropdown menu from which the user can select one of the
        /// passed in choices.
        /// </summary>
        /// <param name="title">The title for this popup.</param>
        /// <param name="choices">The options of the dropdown in correct order.</param>
        /// <param name="initialSelectedIndex">The initially selected index from the dropdown.</param>
        /// <param name="onCloseAction">Called with the button that was used to close this popup and the selected index of the dropdown.</param>
        /// <param name="acceptButtonText">The text for the accept button.</param>
        /// <param name="declineButtonText">The text for the decline button.</param>
        public void ShowDropdownPopup(string title, List<string> choices, int initialSelectedIndex = 0, System.Action<int, int> onCloseAction = null,
            string acceptButtonText = "Change", string declineButtonText = "Discard") {
            if(activeButtons == null) activeButtons = new List<GameObject>();
            this.title.text = title;
            dropdown.ClearOptions();
            dropdown.AddOptions(choices);
            dropdown.value = initialSelectedIndex;
            ClearInvocationList();
            OnPopupClosedDropdownPrompt += onCloseAction;
            for(int i = 0; i < errorUI.Length; i++) {
                errorUI[i].SetActive(false);
            }
            AddButton(acceptButtonText);
            AddButton(declineButtonText, 1);

            ActivatePopupComponents(dropdown.gameObject);
        }

        /// <summary>
        /// Closes the popup and invokes the onClose actions.
        /// </summary>
        public void HidePopup(int value) {
            DestroyActiveButtons();
            popupGameObject.SetActive(false);
            PopupIsActive = false;
            if(pendingPoppups != null && pendingPoppups.Count > 0) {
                pendingPoppups.Dequeue()?.Invoke();
            }
            OnPopupClosedSinglePrompt?.Invoke();
            if(!PopupIsActive) OnPopupClosedSinglePrompt = null;
            OnPopupClosedDoublePrompt?.Invoke(value == 0);
            if(!PopupIsActive) OnPopupClosedDoublePrompt = null;
            OnPopupClosedMultiplePrompt?.Invoke(value);
            if(!PopupIsActive) OnPopupClosedMultiplePrompt = null;
            OnPopupClosedStringInputPrompt?.Invoke(value, stringInputField.text);
            if(!PopupIsActive) OnPopupClosedStringInputPrompt = null;
            int? input = null;
            if(int.TryParse(intInputField.text, out int intResult)) {
                input = intResult;
            }
            OnPopupClosedIntInputPrompt?.Invoke(value, input);
            if(!PopupIsActive) {
                OnPopupClosedIntInputPrompt = null;
            }
            OnPopupClosedDropdownPrompt?.Invoke(value, dropdown.value);
            if(!PopupIsActive) {
                OnPopupClosedDropdownPrompt = null;
            }

            OnPopupClosed?.Invoke();
        }

        /// <summary>
        /// Shows a quick on screen message that dissapears after a couple of seconds.
        /// </summary>
        /// <param name="message">The message to be displayed.</param>
        /// <param name="isErrorMessage">Whether this is an error message and should be highlighted accordingly.</param>
        public void ShowOnScreenMessage(string message, float duration = 2f, bool isErrorMessage = false) {
            onScreenMessage.text = message;
            if(isErrorMessage) onScreenMessage.color = Color.red;
            else onScreenMessage.color = Color.white;
            onScreenMessage.gameObject.SetActive(true);
            onScreenMessageRectTransform.DOAnchorPosY(onScreenMessageRectTransform.sizeDelta.y, popInDuration);
            Invoke(nameof(HideOnScreenMessage), duration + popInDuration);
        }

        private void ClearInvocationList() {
            OnPopupClosedSinglePrompt = null;
            OnPopupClosedDoublePrompt = null;
            OnPopupClosedMultiplePrompt = null;
            OnPopupClosedStringInputPrompt = null;
            OnPopupClosedIntInputPrompt = null;
            OnPopupClosedDropdownPrompt = null;
        }

        private void HideOnScreenMessage() {
            onScreenMessageRectTransform.anchoredPosition = Vector2.zero;
            onScreenMessage.gameObject.SetActive(false);
        }

        private void ActivatePopupComponents(GameObject specialComponent = null) {
            popupGameObject.SetActive(true);
            stringInputField.gameObject.SetActive(false);
            intInputField.gameObject.SetActive(false);
            dropdown.gameObject.SetActive(false);
            if(specialComponent != null) {
                specialComponent.SetActive(true);
                specialComponent.GetComponent<TMP_InputField>()?.Select();
                message.gameObject.SetActive(false);
            } else {
                message.gameObject.SetActive(true);
            }

            PopupIsActive = true;
            UpdateContentSizeFitters();
        }

        private void DestroyActiveButtons() {
            for(int i = 0; i < activeButtons.Count; i++) {
                Destroy(activeButtons[i]);
            }
            activeButtons.Clear();
        }

        private void AddButton(string text, int onClickValue = 0) {
            GameObject buttonGO = Instantiate(buttonPrefab, buttonHolder);
            PopupButton button = buttonGO.GetComponent<PopupButton>();
            button.Initialize(text, HidePopup, onClickValue);
            activeButtons.Add(buttonGO);
        }

        private void UpdateContentSizeFitters() {
            Canvas.ForceUpdateCanvases();
            buttonLayoutGroup.enabled = false;
            buttonLayoutGroup.enabled = true;
            popupLayoutGroup.enabled = false;
            popupLayoutGroup.enabled = true;

            OnPopupOpened?.Invoke();
        }
    }
}
