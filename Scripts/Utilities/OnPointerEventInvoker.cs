using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Author: Kevin Andersch
    /// Date: 26.04.22
    /// Version: 1.0
    /// </summary>
    public class OnPointerEventInvoker : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler {
        [SerializeField] private UnityEvent onPointerEnterCallback;
        [SerializeField] private UnityEvent onPointerExitCallback;
        [SerializeField] private UnityEvent onPointerDownCallback;
        [SerializeField] private UnityEvent onPointerUpCallback;
        [SerializeField] private UnityEvent onPointerClickCallback;
        
        public void OnPointerClick(PointerEventData eventData) {
            onPointerClickCallback?.Invoke();
        }

        public void OnPointerDown(PointerEventData eventData) {
            onPointerDownCallback?.Invoke();
        }

        public void OnPointerEnter(PointerEventData eventData) {
            onPointerEnterCallback?.Invoke();
        }

        public void OnPointerExit(PointerEventData eventData) {
            onPointerExitCallback?.Invoke();
        }

        public void OnPointerUp(PointerEventData eventData) {
            onPointerUpCallback?.Invoke();
        }
    }
}
