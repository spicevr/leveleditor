using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Manages all lanes and their chunks in the level editor.
    /// 
    /// Author: Kevin Andersch
    /// Date: 04.04.22
    /// Version: 1.0
    /// </summary>
    public class LaneManager : MonoBehaviour {
        [Header("Prefabs")]
        [SerializeField] private GameObject laneChunkPrefab;
        [SerializeField] private GameObject lanePrefab;

        [Header("Lane Setup")]
        [SerializeField] private Transform laneChunkHolder;
        [SerializeField] private float lanePanelWidth;
        [SerializeField] private float lanePanelHeight;
        [SerializeField] private float addedLaneChunkWidthScaling;

        /// <summary>
        /// Values should be < 1 as the final value is regarded as being 1f.
        /// </summary>
        private static readonly float[] laneWidthLaneCountDependantScaling = { 0.2f, 0.4f, 0.6f, 0.8f };

        private List<LaneChunk> activeLaneChunks;
        private LevelEditor levelEditor;

        private System.Action OnMoveNotesToCheck;

        // Events
        /// <summary>
        /// 0: number of lanes
        /// </summary>
        public static System.Action<int> OnLaneChunkWasSpawned;

        private void OnEnable() {
            LevelEditor.OnBeforeEditorWasClosed += ResetChunks;
            LevelEditor.OnLaneChunkWasModified += OnLaneChunksWereModifiedCallbackReceiver;
        }

        private void OnDisable() {
            LevelEditor.OnBeforeEditorWasClosed -= ResetChunks;
            LevelEditor.OnLaneChunkWasModified -= OnLaneChunksWereModifiedCallbackReceiver;
        }

        /// <summary>
        /// Spawns a new lane chunk with the passed in amount of lanes and adds it to the lane chunks
        /// of this level.
        /// </summary>
        /// <param name="numberOfLanes">The number of lanes for the new chunk to have.</param>
        /// <param name="levelEditorReference">The level editor of the scene.</param>
        /// <param name="startBeatIndex">The beat index at which the new chunk begins.</param>
        public void CreateNewLaneChunk(int numberOfLanes, LevelEditor levelEditorReference, int startBeatIndex) {
            if(activeLaneChunks == null) activeLaneChunks = new List<LaneChunk>();
            if(numberOfLanes < 1) numberOfLanes = 1;

            // TODO: check if notes would get deleted by the new chunk
            // as in: if there are notes further along the track on chunks that now get shortened

            // TODO: handle creation of lane chunks at topbeatindex

            levelEditor = levelEditorReference;
            int endBeatIndex = levelEditorReference.GetNextLaneChunkStartBeatIndex(startBeatIndex);
            if(!levelEditorReference.AddLaneChunk(startBeatIndex, numberOfLanes)) return;
            LaneChunk laneChunk = Instantiate(laneChunkPrefab, laneChunkHolder).GetComponent<LaneChunk>();

            // scale to proper width
            if(numberOfLanes <= laneWidthLaneCountDependantScaling.Length) {
                laneChunk.RectTransform.localScale = new Vector3(laneWidthLaneCountDependantScaling[numberOfLanes - 1], 1f, 1f);
            } else if(numberOfLanes > laneWidthLaneCountDependantScaling.Length + 1) {
                int difference = numberOfLanes - (laneWidthLaneCountDependantScaling.Length + 1);
                laneChunk.RectTransform.localScale = new Vector3(1 + (addedLaneChunkWidthScaling * difference), 1f, 1f);
            }

            laneChunk.Initialize(numberOfLanes, lanePrefab, levelEditorReference, startBeatIndex, endBeatIndex);
            activeLaneChunks.Add(laneChunk);

            UpdateLaneChunkData();
            RescaleLaneChunks();

            OnLaneChunkWasSpawned?.Invoke(numberOfLanes);
        }

        /// <summary>
        /// Creates a new temporary lane chunk. Similar to CreateNewLaneChunk, but tells the lane chunk
        /// created that it has temporary lanes that will most likely be deleted.
        /// Changes the data of the level, adding the new chunk to it.
        /// </summary>
        /// <param name="temporaryLaneCount">The number of total lanes as long as the chunk is temporary.</param>
        /// <param name="actualLaneCount">The number of lanes once the chunk becomes permanent.</param>
        /// <param name="levelEditorReference">The levelEditor of the scene.</param>
        /// <param name="startBeatIndex">The beatIndex where the new chunk starts at.</param>
        public void CreateNewTemporaryLaneChunk(int temporaryLaneCount, int actualLaneCount, LevelEditor levelEditorReference, int startBeatIndex) {
            if(activeLaneChunks == null) activeLaneChunks = new List<LaneChunk>();
            if(actualLaneCount < 1) actualLaneCount = 1;
            if(temporaryLaneCount < 1) temporaryLaneCount = 1;

            // TODO: check if notes would get deleted by the new chunk
            // as in: if there are notes further along the track on chunks that now get shortened

            // TODO: handle creation of lane chunks at topbeatindex

            levelEditor = levelEditorReference;
            int endBeatIndex = levelEditorReference.GetNextLaneChunkStartBeatIndex(startBeatIndex);
            if(!levelEditorReference.AddTemporaryLaneChunk(startBeatIndex, temporaryLaneCount, actualLaneCount)) return;
            LaneChunk laneChunk = Instantiate(laneChunkPrefab, laneChunkHolder).GetComponent<LaneChunk>();

            // scale to proper width
            int totalLanes = Mathf.Max(temporaryLaneCount, actualLaneCount);
            ResizeChunk(laneChunk, totalLanes);

            laneChunk.Initialize(temporaryLaneCount, actualLaneCount, lanePrefab, levelEditorReference, startBeatIndex, endBeatIndex);
            activeLaneChunks.Add(laneChunk);

            UpdateLaneChunkData();
            RescaleLaneChunks();

            OnLaneChunkWasSpawned?.Invoke(temporaryLaneCount);
        }

        /// <summary>
        /// Spawns a new lane chunk with the values of the given data.
        /// Does not create a new one but rather simply spawns an existing one into the view area.
        /// </summary>
        /// <param name="data">The data of the lane chunk to spawn.</param>
        public void SpawnLaneChunk(LaneChunkData data) {
            LaneChunk laneChunk = Instantiate(laneChunkPrefab, laneChunkHolder).GetComponent<LaneChunk>();
            int lanes = data.LaneCount > data.TemporaryLaneCount ? data.LaneCount : data.TemporaryLaneCount;

            ResizeChunk(laneChunk, lanes);

            if(data.TemporaryLaneCount == 0)
                laneChunk.Initialize(data.LaneCount, lanePrefab, levelEditor, data.StartBeatIndex, data.EndBeatIndex);
            else laneChunk.Initialize(data.TemporaryLaneCount, data.LaneCount, lanePrefab, levelEditor, data.StartBeatIndex, data.EndBeatIndex);
            activeLaneChunks.Add(laneChunk);

            // Force Unity to recalculate the rect transforms in the layout group.
            // Has to be done in case there is a new note spawning with this movement.
            // Then the x-position of the lanes needs to be correct.
            Canvas.ForceUpdateCanvases();
            HorizontalLayoutGroup group = laneChunk.GetComponent<HorizontalLayoutGroup>();
            group.enabled = false;
            group.enabled = true;
        }

        /// <summary>
        /// Updates the endBeatIndex of all currently active lane chunks (in case the dictionary of levelEditor changed).
        /// </summary>
        public void UpdateLaneChunkData() {
            for(int i = 0; i < activeLaneChunks.Count; i++) {
                activeLaneChunks[i].UpdateData();
            }
        }

        /// <summary>
        /// Updates a currently visible lane chunk to display itself as a temporary one with
        /// the passed in values. Also updates all notes placed in this lane chunk to the new correct
        /// position.
        /// Requires the updated lane chunk to be visible!
        /// </summary>
        /// <param name="startBeatIndex">The beatIndex where the chunk starts at.</param>
        /// <param name="temporaryLaneCount">The amount of lanes in its temporary form.</param>
        public void UpdateTemporaryLaneChunk(int startBeatIndex, int temporaryLaneCount) {
            foreach(LaneChunk chunk in activeLaneChunks) {
                if(chunk.StartBeatIndex == startBeatIndex) {
                    chunk.SetToTemporary(temporaryLaneCount, lanePrefab);
                    break;
                }
            }
        }

        /// <summary>
        /// Changes all active lane chunks to remove temporary lanes and become
        /// permanent chunks.
        /// </summary>
        public void ChangeActiveChunksToPermanents() {
            List<LaneChunk> modified = new List<LaneChunk>();
            foreach(LaneChunk chunk in activeLaneChunks) {
                if(chunk.DestroyTemporaryLanes()) modified.Add(chunk);
            }

            foreach(LaneChunk chunk in modified) {
                ResizeChunk(chunk);
            }
        }

        /// <summary>
        /// Resizes the passed in chunk to the correct width.
        /// </summary>
        /// <param name="chunk">The chunk to resize.</param>
        public void ResizeChunk(LaneChunk chunk) {
            if(chunk.LaneCount <= laneWidthLaneCountDependantScaling.Length) {
                chunk.RectTransform.localScale = new Vector3(laneWidthLaneCountDependantScaling[chunk.LaneCount - 1], 1f, 1f);
            } else if(chunk.LaneCount > laneWidthLaneCountDependantScaling.Length + 1) {
                int difference = chunk.LaneCount - (laneWidthLaneCountDependantScaling.Length + 1);
                chunk.RectTransform.localScale = new Vector3(1 + (addedLaneChunkWidthScaling * difference), 1f, 1f);
            }

            chunk.RecalculateNotePositions();
        }

        /// <summary>
        /// Resizes the passed in chunk to the correct width.
        /// </summary>
        /// <param name="chunk">The chunk to resize.</param>
        /// <param name="lanes">The amount of lanes the chunk has.</param>
        public void ResizeChunk(LaneChunk chunk, int lanes) {
            if(lanes <= laneWidthLaneCountDependantScaling.Length) {
                chunk.RectTransform.localScale = new Vector3(laneWidthLaneCountDependantScaling[lanes - 1], 1f, 1f);
            } else if(lanes > laneWidthLaneCountDependantScaling.Length + 1) {
                int difference = lanes - (laneWidthLaneCountDependantScaling.Length + 1);
                chunk.RectTransform.localScale = new Vector3(1 + (addedLaneChunkWidthScaling * difference), 1f, 1f);
            }
        }

        /// <summary>
        /// Returns the correct lane of the correct lane chunk at the passed in beatIndex.
        /// </summary>
        /// <param name="beatIndex">The beatIndex at which to get the lane.</param>
        /// <param name="lane">The lane index of the lane chunk.</param>
        /// <param name="showErrorMessageIfOutOfBounds">Whether to show an error if the lanes index is out of bounds in the found lane chunk.</param>
        public Lane GetLaneAtBeatIndex(int beatIndex, int lane, bool showErrorMessageIfOutOfBounds = false) {
            Canvas.ForceUpdateCanvases();
            int index = 0;
            for(index = 0; index < activeLaneChunks.Count; index++) {
                if(activeLaneChunks[index].StartBeatIndex <= beatIndex && activeLaneChunks[index].EndBeatIndex > beatIndex) break;
            }

            HorizontalLayoutGroup group = activeLaneChunks[index].GetComponent<HorizontalLayoutGroup>();
            group.enabled = false;
            group.enabled = true;
            return activeLaneChunks[index].GetLane(lane, showErrorMessageIfOutOfBounds);
        }

        /// <summary>
        /// Returns the lane at the passed in beatIndex that is closest on the x-axis to the passed
        /// in x-coordinate. Can return null if the beatIndex does not belong to any lane (mainly if
        /// it's the top index when that one belongs to a (not yet spawned) lane chunk).
        /// </summary>
        /// <param name="beatIndex">The beatIndex for which to get the lane.</param>
        /// <param name="xPosition">The x-coordinate from which to get the closest lane.</param>
        public Lane GetLaneAtBeatIndex(int beatIndex, float xPosition) {
            int index = 0;
            bool found = false;
            for(index = 0; index < activeLaneChunks.Count; index++) {
                if(activeLaneChunks[index].StartBeatIndex <= beatIndex && activeLaneChunks[index].EndBeatIndex - 1 >= beatIndex) {
                    found = true;
                    break;
                }
            }

            if(!found) return null;

            return activeLaneChunks[index].GetLane(xPosition);
        }

        /// <summary>
        /// Similar to GetLaneAtBeatIndex but sees the end beatIndex of a lane chunk to belong to
        /// that chunk instead of the next.
        /// Returns null if there was an error and the requested lane does not exist under the active ones.
        /// </summary>
        public Lane GetLaneSwitchAnchorLaneAtBeatIndex(int beatIndex, int lane) {
            int index = 0;
            for(index = 0; index < activeLaneChunks.Count; index++) {
                if(activeLaneChunks[index].StartBeatIndex < beatIndex && activeLaneChunks[index].EndBeatIndex >= beatIndex) break;
            }
            if(index >= activeLaneChunks.Count) return null;
            return activeLaneChunks[index].GetLane(lane, false);
        }

        /// <summary>
        /// Adds a note with the passed in values to the preview.
        /// </summary>
        /// <param name="beatIndex">The beatIndex of the note.</param>
        /// <param name="lane">The lane of the note.</param>
        /// <param name="noteType">The noteType of the note.</param>
        /// <param name="height">The height of the note.</param>
        public void AddNoteToCheckOnMove(int beatIndex, int lane, NoteType noteType) {
            OnMoveNotesToCheck += () => InternalNoteToCheckOnMove(beatIndex, lane, noteType);
        }

        /// <summary>
        /// Moves all currently visible notes of all lanes in the desired direction.
        /// Rescales and repositions lane chunks based on their new percentage of screen space.
        /// </summary>
        /// <param name="moveForward">Whether to move the notes downwards (true) or upwards (false).</param>
        public void MoveAllLanes(bool moveForward) {
            // TODO: maybe show top and bottom lane preview
            LaneChunk chunkToRemove = null;
            for(int i = 0; i < activeLaneChunks.Count; i++) {
                activeLaneChunks[i].MoveLaneNotes(moveForward);
                if(activeLaneChunks[i].CheckIfNeedsToDespawn()) chunkToRemove = activeLaneChunks[i];
            }
            if(chunkToRemove) {
                activeLaneChunks.Remove(chunkToRemove);
                chunkToRemove.DestroyAllNotes();
                Destroy(chunkToRemove.gameObject);
            }

            CheckForNewLaneChunksOnMove(moveForward);
            
            RescaleLaneChunks();

            OnMoveNotesToCheck?.Invoke();
            OnMoveNotesToCheck = null;
        }

        /// <summary>
        /// Moves all lanes during playback mode. Differs from normal movement in that the lanes can move
        /// inbetween beats (given by he passed in offset). Does not move notes, only the lanes.
        /// </summary>
        /// <param name="bottomBeatIndex">The beatIndex at the bottom beat marker.</param>
        public void MoveAllLanesDuringPlayback(int bottomBeatIndex) {
            LaneChunk chunkToRemove = null;
            for(int i = 0; i < activeLaneChunks.Count; i++) {
                if(activeLaneChunks[i].EndBeatIndex <= bottomBeatIndex) chunkToRemove = activeLaneChunks[i];
            }
            if(chunkToRemove) {
                activeLaneChunks.Remove(chunkToRemove);
                Destroy(chunkToRemove.gameObject);
            }

            foreach(LaneChunk chunk in activeLaneChunks) {
                chunk.RescaleWithOffset();
            }
        }

        /// <summary>
        /// Destroys all currently visible notes.
        /// Does NOT remove the note data from the level.
        /// </summary>
        public void DestroyAllNotes() {
            foreach(LaneChunk chunk in activeLaneChunks) {
                chunk.DestroyAllNotes();
            }
        }

        /// <summary>
        /// Destroys all currently visible chunks.
        /// Does NOT remove the chunk data from the level.
        /// Does NOT delete notes, should there be any.
        /// </summary>
        public void DestroyAllLaneChunks() {
            foreach(LaneChunk chunk in activeLaneChunks) {
                Destroy(chunk.gameObject);
            }
            activeLaneChunks.Clear();
        }

        /// <summary>
        /// Decouple all visible notes from their lanes and returns them.
        /// This means they are no longer part of the lanes and can thusly no longer
        /// be controlled by neither the lanes themselves nor the lane manager.
        /// </summary>
        public List<RectTransform> DecoupleNotesFromLanes() {
            List<RectTransform> notes = new List<RectTransform>();
            foreach(LaneChunk chunk in activeLaneChunks) {
                notes.AddRange(chunk.DecoupleAllNotes());
            }
            return notes;
        }

        /// <summary>
        /// Deletes both the visual representation of the lane chunk starting at the passed in startBeatIndex, as
        /// well as the data from the level. Updates other lane chunks accordingly.
        /// Does NOT check whether the removal of the lane chunk would result in there needing to be a new lane chunk
        /// to replace it, so this should only be called on lane chunks inbetween or after already visible lane chunks.
        /// Does NOT remove visual notes.
        /// </summary>
        /// <param name="startBeatIndex">The start beatIndex of the lane chunk to be removed.</param>
        /// <param name="laneNotes">All the notes currently on the deleted lane chunk.</param>
        /// <param name="errorDisplayType">Which type of error message should be displayed, if no chunk exists.</param>
        public void DeleteLaneChunk(int startBeatIndex, out List<LaneNote> laneNotes, ErrorMessageDisplayType errorDisplayType = ErrorMessageDisplayType.Ignore) {
            LaneChunk chunk = null;
            foreach(LaneChunk item in activeLaneChunks) {
                if(item.StartBeatIndex == startBeatIndex) {
                    chunk = item;
                    break;
                }
            }

            if(chunk == null) {
                switch(errorDisplayType) {
                    case ErrorMessageDisplayType.Console:
                        Debug.LogError($"Tried to remove an active lane chunk, starting at {startBeatIndex}, but none were found.");
                        break;
                    case ErrorMessageDisplayType.OnScreen:
                        levelEditor.PopupUtilityInstance.ShowOnScreenMessage($"Tried to remove an active lane chunk, starting at {startBeatIndex}, but none were found.", isErrorMessage: true);
                        break;
                    case ErrorMessageDisplayType.Popup:
                        levelEditor.PopupUtilityInstance.ShowPopup("No chunk found", $"Tried to remove an active lane chunk, starting at {startBeatIndex}, but none were found.", isErrorPopup: true);
                        break;
                }

                laneNotes = null;
                return;
            }

            laneNotes = chunk.GetLaneNotes();
            levelEditor.RemoveLaneChunk(startBeatIndex, errorDisplayType);
            activeLaneChunks.Remove(chunk);
            Destroy(chunk.gameObject);
        }

        /// <summary>
        /// Deletes the passed in lane chunk and spawns the previous one if necessary.
        /// Does NOT check if the passed in lane chunk even exists and can be deleted.
        /// </summary>
        /// <param name="chunk">The lane chunk to delete.</param>
        /// <param name="spawnPrevious">Whether deleting this lane chunk would result in there being empty space, aka needing to spawn the previous lane chunk.</param>
        public void DeleteLaneChunk(LaneChunk chunk, bool spawnPrevious = false) {
            int start = chunk.StartBeatIndex;
            activeLaneChunks.Remove(chunk);
            Destroy(chunk.gameObject);
            if(spawnPrevious) {
                SpawnLaneChunk(levelEditor.GetLaneChunkResponsibleForBeatIndex(start));
            }
        }


        private void RescaleLaneChunks() {
            for(int i = 0; i < activeLaneChunks.Count; i++) {
                activeLaneChunks[i].Rescale();
            }
        }

        /// <summary>
        /// Destroys all chunks and their notes.
        /// </summary>
        private void ResetChunks() {
            for(int i = 0; i < activeLaneChunks.Count; i++) {
                Destroy(activeLaneChunks[i].gameObject);
            }

            for(int i = 0; i < levelEditor.NoteHolder.childCount; i++) {
                Destroy(levelEditor.NoteHolder.GetChild(i).gameObject);
            }
            activeLaneChunks = null;
        }

        private void CheckForNewLaneChunksOnMove(bool checkForward) {
            if(levelEditor.CheckIfNewLaneChunkDataCameIntoEditorView(checkForward, out LaneChunkData data)) {
                SpawnLaneChunk(data);
            }
        }

        private void OnLaneChunksWereModifiedCallbackReceiver(LaneChunkData data) {
            foreach(LaneChunk chunk in activeLaneChunks) {
                if(chunk.StartBeatIndex == data.StartBeatIndex) chunk.SetEndBeatIndex(data.EndBeatIndex);
            }
        }

        private void InternalNoteToCheckOnMove(int beatIndex, int lane, NoteType noteType) {
            if(beatIndex < levelEditor.BeatMarkerManager.BottomMarkerBeatIndex
                || beatIndex > levelEditor.BeatMarkerManager.TopMarkerBeatIndex) return;

            Lane l = GetLaneSwitchAnchorLaneAtBeatIndex(beatIndex, lane);
            float height = levelEditor.BeatMarkerManager.GetBeatMarkerHeightFromBeatIndex(beatIndex);
            EditorNote convertedNote = new EditorNote { BeatIndex = beatIndex, Lane = lane, Type = noteType };
            l.ForceSpawnNote(convertedNote, height, beatIndex);
        }
    }
}
