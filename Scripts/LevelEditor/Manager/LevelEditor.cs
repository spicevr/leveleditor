using NaughtyAttributes;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using System.Linq;
using UnityEngine.EventSystems;

namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Author: Kevin Andersch
    /// Date: 04.04.22
    /// Version: 1.0
    /// </summary>
    public class LevelEditor : MonoBehaviour {
        [Header("References")]
        [SerializeField] private PopupUtility popupUtility;
        [SerializeField] private LevelSelectionManager levelSelectionManager;
        [SerializeField] private LevelSerializationManager levelSerializationManager;
        [SerializeField] private GameObject levelEditorPanel;
        [SerializeField] private LaneManager laneManager;
        [SerializeField] private BeatMarkerManager beatMarkerManager;
        [SerializeField] private Transform noteHolder;
        [SerializeField] private NoteTypeSelector noteTypeSelector;
        [SerializeField] private NotePreviewer notePreviewer;

        public LevelEditorState CurrentEditorState {
            get => currentEditorState;
            private set {
                if(currentEditorState != value) OnEditorStateChanged?.Invoke(value);
                currentEditorState = value;
            }
        }
        public Lane LaneUnderPointer {
            get => laneUnderPointer;
            set {
                if(value != null && value != laneUnderPointer) OnLaneUnderPointerChanged?.Invoke(value);
                laneUnderPointer = value;
            }
        }
        public BeatMarkerManager BeatMarkerManager { get => beatMarkerManager; }
        public float BeatMarkerDistance { get => beatMarkerManager.BeatMarkerDistance; }
        public float MiniBeatMarkerDistance { get => beatMarkerManager.MiniBeatMarkerDistance; }
        public float MinNoteHeight { get => beatMarkerManager.GetBeatMarkerHeightFromMarkerIndex(new MarkerIndex { NormalMarkerIndex = 0 }); }
        public float MaxNoteHeight { get => beatMarkerManager.GetBeatMarkerHeightFromMarkerIndex(new MarkerIndex { NormalMarkerIndex = 999 }); }
        public NotePlacementLocation NotePlacementLocation { get => notePlacementLocation; }
        public LevelEditorSettingsData Settings { get; private set; }
        public int EndOfSongBeatIndex { get => 1000 * (beatMarkerManager.BeatIndicesInbetweenBeatMarker + 1); } // TODO: temporary
        public Transform NoteHolder { get => noteHolder; }
        public PopupUtility PopupUtilityInstance { get => popupUtility; }
        public LevelData Level { get => level; }
        public LaneManager LaneManager { get => laneManager; }
        public NoteTypeSelector NoteTypeSelector { get => noteTypeSelector; }
        public bool IsEditing { get => isEditing; }
        public int MarkerUnderPointerBeatIndex { get => beatMarkerManager.GetBeatMarkerBeatIndexFromMarkerIndex(BeatMarkerUnderMouse); }
        public MarkerIndex BeatMarkerUnderMouse { get => beatMarkerUnderMouse != null ? (MarkerIndex)beatMarkerUnderMouse : default; }
        public int NoteCount { get => notes.Count; }
        public int ScrollingStepDistance { get; private set; }

        private LevelEditorState currentEditorState = LevelEditorState.BasicNoteEditing;
        private LevelData level;
        private bool isEditing;
        private MarkerIndex? beatMarkerUnderMouse;
        private NotePlacementLocation notePlacementLocation;
        private bool unsavedChanges = false;
        private string initialLevelName;
        private LevelDifficulty initialLevelDifficulty;
        private bool allowEditorToOpen = true;
        private Lane laneUnderPointer;
        private float timeOfLastRightClick;

        private SortedDictionary<int, EditorNote> notes;
        private Dictionary<int, LaneChunkData> laneChunks;
        private Dictionary<int, EditorNote> notesWithChildren;

        private Lane savedLaneBeforePopupUI;
        private MarkerIndex savedMarkerBeforePopupUI;

        // Events
        public static System.Action OnSettingsWereLoaded;
        public static System.Action OnBeforeSettingsWereSaved;
        /// <summary>
        /// 0: the lane the note was placed on
        /// </summary>
        public static System.Action<Lane> OnNoteWasPlacedOnLane;
        public static System.Action OnEditorWasOpened;
        public static System.Action OnBeforeEditorWasClosed;
        /// <summary>
        /// 0: beat index of the lowest beat marker
        /// </summary>
        public static System.Action<int> OnTimelineWasMoved;
        /// <summary>
        /// 0: the new lane chunk data (can be identified by startBeatIndex which will never change)
        /// </summary>
        public static System.Action<LaneChunkData> OnLaneChunkWasModified;
        /// <summary>
        /// 0: the new lane under the pointer
        /// </summary>
        public static System.Action<Lane> OnLaneUnderPointerChanged;
        public static System.Action OnBeganPlacingHoldNote;
        /// <summary>
        /// 0: the beatIndex of the hold note that is being edited
        /// </summary>
        public static System.Action<int> OnBeganEditingHoldNote;
        public static System.Action OnStoppedEditingHoldNote;
        /// <summary>
        /// 0: the new state of the editor
        /// </summary>
        public static System.Action<LevelEditorState> OnEditorStateChanged;
        public static System.Action OnScrollStepDistanceChanged;

        public void TempLogic() {
            List<EditorNote> copies = new List<EditorNote>(notes.Values.ToList());
            notes.Clear();
            foreach(EditorNote note in copies) {
                EditorNote n = note;
                n.BeatIndex += 4;
                if(n.BeatIndex >= 0) {
                    notes.Add(n.BeatIndex, n);
                }
            }

            copies.Clear();
            copies = new List<EditorNote>(notesWithChildren.Values.ToList());
            notesWithChildren.Clear();
            foreach(EditorNote note in copies) {
                EditorNote n = note;
                n.BeatIndex += 4;
                for(int i = 0; i < note.ChildNotes.Length; i++) {
                    n.ChildNotes[i].BeatIndex += 4;
                }
                if(n.BeatIndex >= 0) notesWithChildren.Add(n.BeatIndex, n);
            }
        }

        #region Unity Callbacks
        private void Start() {
            LoadEditorSettings();
            LevelSerializationManager.OnTemporaryFileWasResotred += SetUnsavedChanges;
            PlaybackManager.OnPlaybackBegan += StopEditMode;
            PlaybackManager.OnPlaybackEnded += StartEditMode;
            PopupUtility.OnPopupOpened += UnsubscribeMouseHeldEvents;

            if(!levelSerializationManager.HasLevelAssetSelected()) {
                if(!levelSerializationManager.AssignNewLevelAsset(Settings.LastLevelNameWorkedOn, Settings.LastLevelDifficultyWorkedOn)) {
                    popupUtility.ShowPopup("No Level File Detected", "It looks like the serialization manager does not have a" +
                    "\n.level-file selected. This is caused by changing the level name. You have to manually" +
                    "\nassign the new .level-file to the serialization manager to keep working." +
                    "\nDo so while not in play mode. The editor will not work otherwise.", isErrorPopup: true);
                    allowEditorToOpen = false;
                }
            }
        }

        private void OnDisable() {
            LevelSerializationManager.OnLevelDataNeedsToUpdate -= SaveLevelNotes;
            LevelSerializationManager.OnTemporaryFileWasResotred -= SetUnsavedChanges;
            PlaybackManager.OnPlaybackBegan -= StopEditMode;
            PlaybackManager.OnPlaybackEnded -= StartEditMode;
            PopupUtility.OnPopupOpened -= UnsubscribeMouseHeldEvents;
        }

        private void Update() {
            if(!isEditing || PopupUtility.PopupIsActive || ToggleButtonWithVolume.IsCursorOverAnyVolumeToggle) return;

            if(Input.mouseScrollDelta.y > 0f) MoveTimeline(true);
            else if(Input.mouseScrollDelta.y < 0f) MoveTimeline(false);

            switch(currentEditorState) {
                case LevelEditorState.BasicNoteEditing:
                    BasicNoteEditing();
                    break;
                case LevelEditorState.HoldNotePlacement:
                    HoldNotePlacement();
                    break;
            }
        }

        private void OnApplicationQuit() {
            isEditing = false;
            SaveEditorSettings();
        }
        #endregion

        #region Editor States
        private void BasicNoteEditing() {
            if(notePlacementLocation == NotePlacementLocation.UnderCursor) {
                beatMarkerUnderMouse = beatMarkerManager.HighlightMarkerUnderCursor(Input.mousePosition, BeatMarkerUnderMouse);

                if(Input.GetMouseButtonDown(0) && laneUnderPointer != null) BeatMarkerManager.OnLateMarkerUnderCursorChanged += SpawnNoteOnMouseHeldDown;
                if(Input.GetMouseButtonUp(0)) BeatMarkerManager.OnLateMarkerUnderCursorChanged -= SpawnNoteOnMouseHeldDown;

                if(Input.GetMouseButtonDown(1) && laneUnderPointer != null) {
                    DeleteNoteUnderCursor();
                    BeatMarkerManager.OnLateMarkerUnderCursorChanged += DeleteNoteOnMouseHeldDown;
                    OnLaneUnderPointerChanged += DeleteNoteOnLaneUnderPointerChanged;
                }
                if(Input.GetMouseButtonUp(1)) {
                    BeatMarkerManager.OnLateMarkerUnderCursorChanged -= DeleteNoteOnMouseHeldDown;
                    OnLaneUnderPointerChanged -= DeleteNoteOnLaneUnderPointerChanged;
                }
            } else if(Input.GetMouseButtonDown(1)) {
                DeleteNoteUnderCursor();
            }
        }

        private void HoldNotePlacement() {
            beatMarkerUnderMouse = beatMarkerManager.HighlightMarkerUnderCursor(Input.mousePosition, BeatMarkerUnderMouse);

            if(Input.GetKeyDown(KeyCode.Escape)) {
                if(!HoldNoteEditor.Instance.StopEditingNote()) {
                    popupUtility.ShowPopup("Invalid Hold Note", "The hold note you are currently editing is incomplete." +
                        "\nDo you wish to delete it?", "Delete it", "Keep editing", OnIncompleteHoldNoteCallbackReceiver, true);
                } else {
                    StopEditingHoldNote();
                }
            } else if(Input.GetMouseButtonDown(1)) {
                int beatIndex = beatMarkerManager.GetBeatMarkerBeatIndexFromMarkerIndex(BeatMarkerUnderMouse);
                if(laneUnderPointer != null && !HoldNoteEditor.Instance.RemoveAnchorPoint(beatIndex, laneUnderPointer.LaneIndex)) {
                    if(Time.time - timeOfLastRightClick <= 0.3f) {
                        if(!HoldNoteEditor.Instance.StopEditingNote()) {
                            popupUtility.ShowPopup("Invalid Hold Note", "The hold note you are currently editing is incomplete." +
                                "\nDo you wish to delete it?", "Delete it", "Keep editing", OnIncompleteHoldNoteCallbackReceiver, true);
                        } else {
                            StopEditingHoldNote();
                        }
                    } else {
                        timeOfLastRightClick = Time.time;
                    }
                }
            }
        }
        #endregion

        #region General Editor Logic
        /// <summary>
        /// Opens the editor and triggers every part of it to load properly.
        /// </summary>
        public void OpenEditor() {
#if !UNITY_EDITOR
            popupUtility.ShowPopup("WARNING", "The current version of the editor does not support working in a build." +
                "\nDownload and open the unity editor and work from there instead!", isErrorPopup: true);
            return;
#endif
            if(!allowEditorToOpen) return;

            levelEditorPanel.SetActive(true);
            level = levelSerializationManager.Level;
            beatMarkerManager.SpawnBeatMarkers(Settings.BeatMarkerCount, level.MinorPerMajorBeatCount, this);
            LoadLevelNotes();
            isEditing = true;
            initialLevelName = level.Name;
            initialLevelDifficulty = level.Difficulty;

            LevelSerializationManager.OnLevelDataNeedsToUpdate += SaveLevelNotes;

            OnEditorWasOpened?.Invoke();
        }

        /// <summary>
        /// Closes the editor and everything related to it.
        /// </summary>
        public void CloseEditor() {
            OnBeforeEditorWasClosed?.Invoke();

            isEditing = false;
            levelEditorPanel.SetActive(false);
            levelSelectionManager.OpenLevelSelection();
        }

        /// <summary>
        /// Quits the program, either by turning off the unity editor or quitting the running application.
        /// </summary>
        public void Quit() {
            if(unsavedChanges) {
                string[] choices = new string[] { "Save", "Discard", "Cancel" };
                popupUtility.ShowPopup("Unsaved Changes", "There are unsaved changes in the current level.\n" +
                    "Do you wish to save or discard them?", choices, OnQuitCallbackReceiver);
            } else InternalQuit();
        }

        /// <summary>
        /// Closes the editor and opens the level selection screen. Prompts the user if he wants to save if
        /// there are unsaved changes made.
        /// </summary>
        public void ReturnToLevelSelect() {
            if(unsavedChanges) {
                string[] choices = new string[] { "Save", "Discard", "Cancel" };
                popupUtility.ShowPopup("Unsaved Changes", "There are unsaved changes in the current level.\n" +
                    "Do you wish to save or discard them?", choices, OnReturnToLevelSelectCallbackReceiver);
            } else {
                CloseEditor();
            }
        }

        /// <summary>
        /// Saves the level data and serializes it.
        /// </summary>
        public void SaveLevel() {
            if(CheckForOnSaveLevelWarning()) return;

            SaveLevelNotes();
            levelSerializationManager.SaveLevel();
            if(initialLevelName != level.Name || initialLevelDifficulty != level.Difficulty) {
                levelSerializationManager.DeleteLevelFile(initialLevelName, initialLevelDifficulty);
            }
            unsavedChanges = false;
            Settings.LastLevelNameWorkedOn = level.Name;
            Settings.LastLevelDifficultyWorkedOn = level.Difficulty;
            SaveEditorSettings();
        }

        /// <summary>
        /// Loads the view area of the level in the passed in interval, including lanes and notes.
        /// </summary>
        /// <param name="bottomBeatIndex">The beginning of the area.</param>
        /// <param name="topBeatIndex">The end of the area.</param>
        public void LoadViewArea(int bottomBeatIndex, int topBeatIndex) {
            if(ScrollingStepDistance > 1) {
                int indexOffset = bottomBeatIndex % (beatMarkerManager.BeatIndicesInbetweenBeatMarker + 1);
                bottomBeatIndex -= indexOffset;
                topBeatIndex -= indexOffset;
            }

            beatMarkerManager.SetBottomMarkerBeatIndex(bottomBeatIndex);
            List<LaneChunkData> chunks = GetAllLaneChunksInInvetval(bottomBeatIndex, topBeatIndex);
            foreach(LaneChunkData item in chunks) {
                laneManager.SpawnLaneChunk(item);
            }
            foreach(EditorNote note in notes.Values) {
                if(note.BeatIndex >= bottomBeatIndex && note.BeatIndex <= topBeatIndex) {
                    SpawnNote(note, true);
                }
            }
        }

        /// <summary>
        /// Loads the beginning of the level, setting the view area to beatIndex 0.
        /// </summary>
        public void JumpToBottom() {
            laneManager.DestroyAllNotes();
            laneManager.DestroyAllLaneChunks();
            beatMarkerManager.SetBottomMarkerBeatIndex(0);
            List<LaneChunkData> chunks = GetAllLaneChunksInInvetval(0, beatMarkerManager.TopMarkerBeatIndex);
            foreach(LaneChunkData item in chunks) {
                laneManager.SpawnLaneChunk(item);
            }
            for(int i = 0; i <= beatMarkerManager.TopMarkerBeatIndex; i++) {
                EditorNote? note = GetNote(i, false);
                if(note != null) SpawnNote((EditorNote)note, true);
            }
        }

        /// <summary>
        /// Loads the beginning of the level, setting the view area to beatIndex 0.
        /// </summary>
        public void JumpToTop() {
            laneManager.DestroyAllNotes();
            laneManager.DestroyAllLaneChunks();
            beatMarkerManager.SetTopMarkerBeatIndex(EndOfSongBeatIndex);
            List<LaneChunkData> chunks = GetAllLaneChunksInInvetval(0, beatMarkerManager.TopMarkerBeatIndex);
            foreach(LaneChunkData item in chunks) {
                laneManager.SpawnLaneChunk(item);
            }
            for(int i = beatMarkerManager.BottomMarkerBeatIndex; i <= EndOfSongBeatIndex; i++) {
                EditorNote? note = GetNote(i, false);
                if(note != null) SpawnNote((EditorNote)note, true);
            }
        }

        /// <summary>
        /// Changes the editor state to place parts for a hold note, rather than having its normal
        /// functionality.
        /// </summary>
        public void IsPlacingNewHoldNote() {
            CurrentEditorState = LevelEditorState.HoldNotePlacement;
            HoldNoteEditor.Instance.OpenNewNote(laneUnderPointer.LaneIndex,
                beatMarkerManager.GetBeatMarkerBeatIndexFromMarkerIndex(BeatMarkerUnderMouse));

            OnBeganPlacingHoldNote?.Invoke();
        }

        /// <summary>
        /// Prompts the user to input the new number of lanes and then changes the editor state
        /// to place parts for the hold note, rather than having its normal funcitonality.
        /// </summary>
        public void IsPlacingNewLaneSwitchNote() {
            savedLaneBeforePopupUI = laneUnderPointer;
            savedMarkerBeforePopupUI = BeatMarkerUnderMouse;
            popupUtility.ShowInputPopup("New Lane Count", 2, PlaceNewLaneSwitchNoteCallbackReceiver, "Create", "Cancel");
        }

        /// <summary>
        /// Changes the level editor state to the basic editing state and invokes all
        /// relevant events.
        /// </summary>
        public void StopEditingHoldNote() {
            CurrentEditorState = LevelEditorState.BasicNoteEditing;
            ChangeTemporaryLaneChunksToPermanent();

            OnStoppedEditingHoldNote?.Invoke();
        }

        // TODO: currently any change makes it true. maybe check if the level changed
        // (aka i make a change and revert it = still unsaved changes)

        /// <summary>
        /// Notifies the levelEditor that changes to the level were made so that he can
        /// notify the player to save before quitting.
        /// </summary>
        public void SetUnsavedChanges() => unsavedChanges = true;

        /// <summary>
        /// Changes the distance the view area scrolls with one input to either be to the next major beat
        /// or to the next minor beat.
        /// </summary>
        /// <param name="distanceIsMajorBeat">If true, scrolling only occurs on major beats.</param>
        public void SetScrollingStepDistance(bool distanceIsMajorBeat) {
            ScrollingStepDistance = distanceIsMajorBeat ? beatMarkerManager.BeatIndicesInbetweenBeatMarker + 1 : 1;
            if(!distanceIsMajorBeat && !beatMarkerManager.ShowMiniBeatMarkers) beatMarkerManager.ToggleMiniBeatMarkers();
            OnScrollStepDistanceChanged?.Invoke();
        }

        private bool CheckForOnSaveLevelWarning() {
            // TODO: check if there are notes too close to the start
            // TODO: check if there are notes too close to lane switches
            return false;
        }

        private void LoadLevelNotes() {
            notes = new SortedDictionary<int, EditorNote>();
            laneChunks = new Dictionary<int, LaneChunkData>();
            notesWithChildren = new Dictionary<int, EditorNote>();

            laneManager.CreateNewLaneChunk(level.InitialNumberOfLanes, this, 0);

            if(level.Notes == null) return;
            foreach(NoteData note in level.Notes) {
                EditorNote editorNote;

                if(note.Type == NoteType.Hold || note.Type == NoteType.LaneSwitch) {
                    editorNote = ConvertSavedToEditorHoldNote(note);
                    if(editorNote.BeatIndex <= beatMarkerManager.TopMarkerBeatIndex) SpawnNote(editorNote, true);
                    for(int i = 0; i < editorNote.ChildNotes.Length; i++) {
                        if(editorNote.ChildNotes[i].BeatIndex <= beatMarkerManager.TopMarkerBeatIndex) {
                            SpawnNote(editorNote.ChildNotes[i], editorNote.Type, true);
                            if(note.Type == NoteType.LaneSwitch && i == editorNote.ChildNotes.Length - 1) {
                                laneManager.CreateNewLaneChunk(editorNote.NewLaneCountOnCompletion, this, editorNote.ChildNotes[i].BeatIndex);
                            }
                        } else if(note.Type == NoteType.LaneSwitch && i == editorNote.ChildNotes.Length - 1) {
                            AddLaneChunk(editorNote.ChildNotes[i].BeatIndex, editorNote.NewLaneCountOnCompletion, true);
                        }
                    }
                    AddNote(editorNote, true, ErrorMessageDisplayType.OnScreen);
                } else {
                    editorNote = new EditorNote { BeatIndex = note.BeatIndex, Lane = note.Lane, Type = note.Type };
                    if(note.Type == NoteType.Event) {
                        editorNote.EventType = note.SpecialNoteData[0].EventType;
                        if(editorNote.EventType == NoteEvent.Story) editorNote.StoryEventID = note.SpecialNoteData[0].StoryEventID;
                    }
                    if(note.BeatIndex <= beatMarkerManager.TopMarkerBeatIndex) SpawnNote(editorNote, true, true);
                    AddNote(editorNote, true, ErrorMessageDisplayType.OnScreen);
                }
            }
        }

        private void SaveLevelNotes() {
            NoteData[] levelNotes = new NoteData[notes.Count];
            int i = 0;
            foreach(KeyValuePair<int, EditorNote> note in notes) {
                SpecialNoteData[] specialData = null;
                if(note.Value.Type == NoteType.Hold || note.Value.Type == NoteType.LaneSwitch) {
                    specialData = new SpecialNoteData[note.Value.ChildNotes.Length];
                    specialData[0] = new SpecialNoteData() {
                        BeatIndex = note.Value.ChildNotes[0].BeatIndex - note.Value.BeatIndex,
                        Lane = note.Value.ChildNotes[0].Lane,
                        LaneSwitchCount = note.Value.NewLaneCountOnCompletion
                    };
                    for(int c = 1; c < specialData.Length; c++) {
                        EditorChildNote child = note.Value.ChildNotes[c];
                        specialData[c] = new SpecialNoteData() {
                            BeatIndex = child.BeatIndex - note.Value.ChildNotes[c - 1].BeatIndex,
                            Lane = child.Lane,
                            LaneSwitchCount = note.Value.NewLaneCountOnCompletion
                        };
                    }
                }

                if(note.Value.Type == NoteType.Event) {
                    SpecialNoteData specialNote = new SpecialNoteData() {
                        EventType = note.Value.EventType,
                        StoryEventID = note.Value.StoryEventID
                    };
                    specialData = new SpecialNoteData[] { specialNote };
                }

                levelNotes[i++] = new NoteData {
                    BeatIndex = note.Value.BeatIndex,
                    Lane = note.Value.Lane,
                    Type = note.Value.Type,
                    SpecialNoteData = specialData,
                    IsMajorBeatNote = note.Value.BeatIndex % (beatMarkerManager.BeatIndicesInbetweenBeatMarker + 1) == 0
                };
            }

            level.Notes = levelNotes;
        }

        private EditorNote ConvertSavedToEditorHoldNote(NoteData data) {
            EditorNote note = new EditorNote() {
                BeatIndex = data.BeatIndex,
                Lane = data.Lane,
                Type = data.Type,
                NewLaneCountOnCompletion = data.SpecialNoteData[0].LaneSwitchCount
            };
            EditorChildNote[] children = new EditorChildNote[data.SpecialNoteData.Length];
            int beatIndex = data.BeatIndex;
            for(int i = 0; i < children.Length; i++) {
                beatIndex += data.SpecialNoteData[i].BeatIndex;
                children[i] = new EditorChildNote() {
                    BeatIndex = beatIndex,
                    Lane = data.SpecialNoteData[i].Lane
                };
            }
            note.ChildNotes = children;

            return note;
        }

        private void OnQuitCallbackReceiver(int result) {
            if(result == 2) return;

            if(result == 0) SaveLevel();
            InternalQuit();
        }

        private void OnReturnToLevelSelectCallbackReceiver(int result) {
            if(result == 2) return;

            if(result == 0) SaveLevel();
            else levelSerializationManager.LoadLevel();
            CloseEditor();
        }

        private void InternalQuit() {
            levelSerializationManager.DecidedToDiscardChanges = true;
            //levelSerializationManager.LoadLevel();
#if UNITY_EDITOR
            if(Application.isEditor) EditorApplication.ExitPlaymode();
            else Application.Quit();
#else
            Application.Quit();
#endif
        }

        private void StopEditMode() => isEditing = false;

        private void StartEditMode() => isEditing = true;
        #endregion

        #region Notes
        /// <summary>
        /// Moves all notes currently on screen in the correct direction.
        /// Spawns new notes in case there are new notes serialized at the new beat markers.
        /// Updates all beat markers to their correct time values.
        /// </summary>
        /// <param name="moveForward">Wheter to move forwards (true) or backwards (false) in time.</param>
        public void MoveTimeline(bool moveForward) {
            int bottomBeatIndex = beatMarkerManager.BottomMarkerBeatIndex;
            int topBeatIndex = beatMarkerManager.TopMarkerBeatIndex;
            if(moveForward && topBeatIndex >= EndOfSongBeatIndex) return;
            if(!moveForward && bottomBeatIndex <= 0) return;

            beatMarkerManager.ChangeBottomMarkerBeatIndex(moveForward ? ScrollingStepDistance : -ScrollingStepDistance);
            laneManager.MoveAllLanes(moveForward);
            CheckForNotesThatCameIntoViewArea(moveForward);

            OnTimelineWasMoved?.Invoke(beatMarkerManager.BottomMarkerBeatIndex);
        }

        /// <summary>
        /// Returns whether the passed in beatIndex lies within the view area.
        /// </summary>
        /// <param name="beatIndex">The beatIndex to check.</param>
        public bool IsWithinViewArea(int beatIndex) => beatIndex >= beatMarkerManager.BottomMarkerBeatIndex && beatIndex <= beatMarkerManager.TopMarkerBeatIndex;

        #region Spawning & Deleting
        /// <summary>
        /// Places a note at the currently selected placement location in the lane the cursor is currently over.
        /// Does not check whether that lane exists. Should only be called in the OnClick()-Event of said lane.
        /// This places a new note, aka adding it to the notes to be serialized (if possible).
        /// </summary>
        public void PlaceNoteAtLaneUnderCursor() {
            if(beatMarkerUnderMouse == null) return;
            int beatIndex = GetNotePlacementBeatIndex();

            if(currentEditorState == LevelEditorState.BasicNoteEditing) PlaceNoteDuringBasicNoteEditing(beatIndex);
            else if(currentEditorState == LevelEditorState.HoldNotePlacement) PlaceNoteDuringHoldNoteEditing(beatIndex);
        }

        /// <summary>
        /// Spawn the passed in note into the correct lane. This does not add it to the notes
        /// of the level and is thusly not serialized. Should only be called on notes already
        /// in the level (aka on loading).
        /// </summary>
        /// <param name="note">The data of the note to spawn.</param>
        /// <param name="ignoresStateChanges">Whether the spawning of the note will trigger a state change of the level editor (if the note type would do so).</param>
        /// <param name="showErrorMessage">If no lane for the given note is found, should an error be shown?</param>
        public void SpawnNote(EditorNote note, bool ignoresStateChanges = false, bool showErrorMessage = false) {
            Lane lane = laneManager.GetLaneAtBeatIndex(note.BeatIndex, note.Lane);
            float height = beatMarkerManager.GetBeatMarkerHeightFromBeatIndex(note.BeatIndex, true);
            if(lane != null) lane.SpawnNoteAtCustomHeight(note, height, note.BeatIndex, true, ignoresStateChanges: ignoresStateChanges);
            else if(showErrorMessage) Debug.LogError($"Couldn't spawn a note at beatIndex {note.BeatIndex} in lane {note.Lane}!");
        }

        /// <summary>
        /// Similar to SpawnNote() but spawns onto the last beatIndex of a lane chunk instead of the
        /// new lane chunk if the latter starts where the former ends.
        /// Should only be used for lane switch note anchors.
        /// </summary>
        /// <param name="note">The data of the note to spawn.</param>
        public void SpawnLaneSwitchAnchor(EditorNote note) {
            float height = beatMarkerManager.GetBeatMarkerHeightFromBeatIndex(note.BeatIndex, true);
            Lane lane = laneManager.GetLaneSwitchAnchorLaneAtBeatIndex(note.BeatIndex, note.Lane);
            lane.SpawnNoteAtCustomHeight(note, height, note.BeatIndex, true, ignoresStateChanges: true);
        }

        /// <summary>
        /// Spawn the passed in note into the correct lane. This does not add it to the notes
        /// of the level and is thusly not serialized. Should only be called on notes already
        /// in the level (aka on loading).
        /// </summary>
        /// <param name="note">The data of the note to spawn.</param>
        /// <param name="noteType">The type of note the child note belongs to.</param>
        /// <param name="ignoresStateChanges">Whether the spawning of the note will trigger a state change of the level editor (if the note type would do so).</param>
        public void SpawnNote(EditorChildNote note, NoteType noteType, bool ignoresStateChanges = false) {
            Lane lane = laneManager.GetLaneAtBeatIndex(note.BeatIndex, note.Lane);
            float height = beatMarkerManager.GetBeatMarkerHeightFromBeatIndex(note.BeatIndex, true);
            EditorNote convertedNote = new EditorNote { Type = noteType, BeatIndex = note.BeatIndex, Lane = note.Lane };
            lane.SpawnNoteAtCustomHeight(convertedNote, height, note.BeatIndex, true, ignoresStateChanges: ignoresStateChanges);
        }

        /// <summary>
        /// Checks if there is a note in the given lane at the given beat marker where the cursor
        /// is at. If so, deletes that note both visually and from the serialization.
        /// </summary>
        public void DeleteNoteUnderCursor() {
            int beatIndex = beatMarkerManager.GetBeatMarkerBeatIndexFromMarkerIndex(BeatMarkerUnderMouse);
            if(notes.ContainsKey(beatIndex)) {
                if(notes[beatIndex].Type == NoteType.Hold || notes[beatIndex].Type == NoteType.LaneSwitch) {
                    if(laneUnderPointer.HasNote(beatIndex))
                        popupUtility.ShowOnScreenMessage("Can't delete hold note anchors while not editing the hold note.", isErrorMessage: true);
                    return;
                }
                if(laneUnderPointer != null
                    && LaneUnderPointer.DeleteNote(beatIndex, ErrorMessageDisplayType.Ignore)) notes.Remove(beatIndex);
            } else {
                foreach(EditorNote parent in notesWithChildren.Values) {
                    for(int i = 0; i < parent.ChildNotes.Length; i++) {
                        if(parent.Type == NoteType.LaneSwitch && i == parent.ChildNotes.Length - 1) {
                            Lane lane = laneManager.GetLaneSwitchAnchorLaneAtBeatIndex(beatIndex, parent.ChildNotes[i].Lane);
                            if(lane != null && parent.ChildNotes[i].BeatIndex == beatIndex && lane.HasNote(beatIndex)
                                && Mathf.Abs(Input.mousePosition.x - lane.transform.position.x) <= 55f)
                                popupUtility.ShowOnScreenMessage("Can't delete hold note anchors while not editing the hold note.", isErrorMessage: true);
                            break;
                        }

                        if(parent.ChildNotes[i].BeatIndex == beatIndex && LaneUnderPointer.HasNote(beatIndex))
                            popupUtility.ShowOnScreenMessage("Can't delete hold note anchors while not editing the hold note.", isErrorMessage: true);
                    }
                }
            }
        }

        /// <summary>
        /// Adds the passed in note to the notes of the level (which eventually get serialized).
        /// If a note at the given beat index already exists, does not add the note.
        /// Returns true if a note was added, false if not.
        /// </summary>
        /// <param name="note">The note to add.</param>
        /// <param name="calledDuringLoading">Whether this method was called during loading. Prevents some logic to trigger, mainly disabling player feedback.</param>
        /// <param name="errorMessageOnFailure">Whether an error message will be displayed if the note doesn't get added.</param>
        public bool AddNote(EditorNote note, bool calledDuringLoading = false, ErrorMessageDisplayType errorMessageOnFailure = ErrorMessageDisplayType.Ignore) {
            if(notes.ContainsKey(note.BeatIndex)) {
                if(errorMessageOnFailure == ErrorMessageDisplayType.Popup) {
                    popupUtility.ShowPopup("Invalid Note Placement", "Couldn't add a note at the desired position as a note already" +
                        "exists on that beat.", isErrorPopup: true);
                } else if(errorMessageOnFailure == ErrorMessageDisplayType.Console) {
                    Debug.LogError($"Tried to add a note at beat index {note.BeatIndex} in lane {note.Lane} which would result in two notes at the same index!");
                } else if(errorMessageOnFailure == ErrorMessageDisplayType.OnScreen) {
                    popupUtility.ShowOnScreenMessage("Invalid note location. There already is a note on that beat!", 1.5f, true);
                }
                return false;
            } else if(CheckIfBeatIndexIsInbetweenAnyHoldNoteAnchors(note.BeatIndex)) {
                if(errorMessageOnFailure == ErrorMessageDisplayType.Popup) {
                    popupUtility.ShowPopup("Invalid Note Placement", "Couldn't add a note at the desired position it would sit" +
                        "on top of a hold note.", isErrorPopup: true);
                } else if(errorMessageOnFailure == ErrorMessageDisplayType.Console) {
                    Debug.LogError($"Tried to add a note at beat index {note.BeatIndex} in lane {note.Lane} which would result in a note on top of a hold note!");
                } else if(errorMessageOnFailure == ErrorMessageDisplayType.OnScreen) {
                    popupUtility.ShowOnScreenMessage("Invalid note location. There already is a hold note on that beat!", 1.5f, true);
                }
                return false;
            } else {
                notes.Add(note.BeatIndex, note);
                if(note.Type == NoteType.Hold || note.Type == NoteType.LaneSwitch) notesWithChildren.Add(note.BeatIndex, note);
                if(!calledDuringLoading) unsavedChanges = true;
                return true;
            }
        }

        /// <summary>
        /// Updates a note from the notes of the level.
        /// Does NOT update the visual representation of the note.
        /// </summary>
        /// <param name="note">The new data of the note.</param>
        /// <param name="showErrorMessage">If no such note to update is found, should there be an error message?</param>
        public void UpdateNote(EditorNote note, bool showErrorMessage = false) {
            if(!notes.ContainsKey(note.BeatIndex)) {
                if(showErrorMessage) Debug.LogError($"Tried to update a note at beatIndex {note.BeatIndex} but the level does not have a note there.");
                return;
            }

            notes[note.BeatIndex] = note;
            if(note.Type == NoteType.Hold || note.Type == NoteType.LaneSwitch) notesWithChildren[note.BeatIndex] = note;
            unsavedChanges = true;
        }

        /// <summary>
        /// Deletes the passed in note data from the notes of the level.
        /// Does NOT delete the visual representation of the note.
        /// </summary>
        /// <param name="note">The note data to delete.</param>
        public void DeleteNote(EditorNote note, bool showErrorMessage = false) => DeleteNote(note.BeatIndex, showErrorMessage);

        /// <summary>
        /// Deletes the note at the passed in beatIndex from the notes of the level.
        /// Does NOT delete the visual representation of the note.
        /// </summary>
        /// <param name="beatIndex">The beatIndex of the note to delete.</param>
        public void DeleteNote(int beatIndex, bool showErrorMessage = false) {
            if(!notes.ContainsKey(beatIndex)) {
                if(showErrorMessage) Debug.LogError($"Tried to delete a non existant note at beatIndex {beatIndex}.");
                return;
            }

            if(notes[beatIndex].Type == NoteType.Hold || notes[beatIndex].Type == NoteType.LaneSwitch) notesWithChildren.Remove(beatIndex);
            notes.Remove(beatIndex);
            unsavedChanges = true;
        }

        /// <summary>
        /// Deletes all notes from the level, including both their visual representation
        /// in the view area and their data. Then adds the first laneChunk (with the levels
        /// initial number of lanes) to the level again.
        /// </summary>
        public void RemoveAllNotes() {
            laneManager.DestroyAllNotes();
            notes.Clear();
            notesWithChildren.Clear();
            level.Notes = null;

            laneChunks.Clear();
            laneManager.DestroyAllLaneChunks();
            laneManager.CreateNewLaneChunk(level.InitialNumberOfLanes, this, 0);
        }

        private void SpawnNoteOnMouseHeldDown(int beatIndex) {
            if(LaneUnderPointer == null) return;
            EditorNote note = new EditorNote {
                Lane = LaneUnderPointer.LaneIndex,
                Type = noteTypeSelector.ActiveNoteType,
                BeatIndex = beatIndex
            };
            if(!AddNote(note, errorMessageOnFailure: ErrorMessageDisplayType.Ignore)) return;

            LaneUnderPointer.SpawnNoteAtCustomHeight(note,
                    beatMarkerManager.GetBeatMarkerHeightFromBeatIndex(beatIndex, true),
                    beatIndex);

            OnNoteWasPlacedOnLane?.Invoke(LaneUnderPointer);
        }

        private void DeleteNoteOnMouseHeldDown(int beatIndex) {
            if(LaneUnderPointer == null) return;
            if(notes.ContainsKey(beatIndex)) {
                if(notes[beatIndex].Type == NoteType.Hold || notes[beatIndex].Type == NoteType.LaneSwitch) return;
                if(LaneUnderPointer.DeleteNote(beatIndex, ErrorMessageDisplayType.Ignore)) {
                    notes.Remove(beatIndex);
                    unsavedChanges = true;
                }
            }
        }

        private void DeleteNoteOnLaneUnderPointerChanged(Lane lane) {
            int beatIndex = beatMarkerManager.GetBeatMarkerBeatIndexFromMarkerIndex(BeatMarkerUnderMouse);
            if(notes.ContainsKey(beatIndex)) {
                if(notes[beatIndex].Type == NoteType.Hold || notes[beatIndex].Type == NoteType.LaneSwitch) return;
                if(lane.DeleteNote(beatIndex, ErrorMessageDisplayType.Ignore)) {
                    notes.Remove(beatIndex);
                    unsavedChanges = true;
                }
            }
        }

        private void UnsubscribeMouseHeldEvents() {
            BeatMarkerManager.OnLateMarkerUnderCursorChanged -= SpawnNoteOnMouseHeldDown;
            BeatMarkerManager.OnLateMarkerUnderCursorChanged -= DeleteNoteOnMouseHeldDown;
            OnLaneUnderPointerChanged -= DeleteNoteOnLaneUnderPointerChanged;
        }

        private void PlaceNoteDuringBasicNoteEditing(int beatIndex) {
            if(LaneUnderPointer == null) return;

            EditorNote note = new EditorNote {
                Lane = LaneUnderPointer.LaneIndex,
                Type = noteTypeSelector.ActiveNoteType,
                BeatIndex = beatIndex
            };

            // check if the click was meant to open a hold note
            if(ClickedOnHoldNoteAnchor(beatIndex, note.Lane, out int parentBeatIndex)) {
                CurrentEditorState = LevelEditorState.HoldNotePlacement;
                HoldNoteEditor.Instance.OpenExistingNote(parentBeatIndex);

                OnBeganEditingHoldNote?.Invoke(beatIndex);
                return;
            }

            // otherwise check if a note can be placed where the user clicked
            if(!AddNote(note, errorMessageOnFailure: ErrorMessageDisplayType.OnScreen)) return;

            // add the note
            if(notePlacementLocation == NotePlacementLocation.Top) {
                LaneUnderPointer.SpawnNoteAtTop(note, beatIndex);
            } else if(notePlacementLocation == NotePlacementLocation.Bottom) {
                LaneUnderPointer.SpawnNoteAtBottom(note, beatIndex);
            } else {
                LaneUnderPointer.SpawnNoteAtCustomHeight(note,
                    beatMarkerManager.GetBeatMarkerHeightFromMarkerIndex(BeatMarkerUnderMouse),
                    beatIndex);
            }

            // then check if the just placed note needs input (aka if its an event note)
            if(noteTypeSelector.ActiveNoteType == NoteType.Event) {
                List<string> choices = System.Enum.GetNames(typeof(NoteEvent)).ToList();
                popupUtility.ShowDropdownPopup("New Event Note", choices, 0, (choice, index) => {
                    PlaceNewEventNoteCallbackReceiver(note, choice, index);
                }, "Apply", "Cancel");
            }

            OnNoteWasPlacedOnLane?.Invoke(LaneUnderPointer);
        }

        private void PlaceNoteDuringHoldNoteEditing(int beatIndex) {
            bool includeMiniBeatMarkers = beatMarkerManager.TargetingMode == BeatMarkerTargetingMode.AllBeats;
            HoldNoteEditor.Instance.AddAnchorPoint(laneUnderPointer, beatIndex, beatMarkerManager.GetBeatMarkerHeightFromBeatIndex(beatIndex, includeMiniBeatMarkers));
        }
        #endregion

        #region Checks
        /// <summary>
        /// Returns if the passed in beatIndex has a note or
        /// if a new note could potentially be placed there (without overlapping).
        /// </summary>
        /// <param name="beatIndex">The beatIndex to check.</param>
        public bool IsCurrentPointerPositionValidNotePosition(int beatIndex) {
            return !notes.ContainsKey(beatIndex) && !CheckIfBeatIndexIsInbetweenAnyHoldNoteAnchors(beatIndex);
        }

        /// <summary>
        /// Checks if the level has notes placed after the passed in beatIndex.
        /// Returns true if so, with a list of all notes behind the beatIndex.
        /// </summary>
        /// <param name="beatIndex">The beatIndex after which to start.</param>
        /// <param name="foundNotes">All notes placed after the beatIndex.</param>
        public bool AreThereNotesAfterBeatIndex(int beatIndex, out List<EditorNote> foundNotes) {
            foundNotes = new List<EditorNote>();
            foreach(KeyValuePair<int, EditorNote> note in notes) {
                if(note.Key <= beatIndex) continue;

                foundNotes.Add(note.Value);
            }

            return foundNotes.Count > 0;
        }

        private bool ClickedOnHoldNoteAnchor(int beatIndex, int lane, out int parentBeatIndex) {
            parentBeatIndex = -1;

            foreach(EditorNote parent in notesWithChildren.Values) {
                if(parent.BeatIndex == beatIndex && parent.Lane == lane) {
                    parentBeatIndex = parent.BeatIndex;
                    return true;
                }

                for(int i = 0; i < parent.ChildNotes.Length; i++) {
                    if(parent.Type == NoteType.LaneSwitch) {
                        Lane l = laneManager.GetLaneSwitchAnchorLaneAtBeatIndex(beatIndex, parent.ChildNotes[i].Lane);
                        if(l == null) return false;
                        else if(parent.ChildNotes[i].BeatIndex == beatIndex && l.HasNote(beatIndex)
                            && Mathf.Abs(Input.mousePosition.x - l.transform.position.x) <= 55f) {
                            parentBeatIndex = parent.BeatIndex;
                            return true;
                        }
                    }
                    if(parent.ChildNotes[i].BeatIndex == beatIndex && parent.ChildNotes[i].Lane == lane) {
                        parentBeatIndex = parent.BeatIndex;
                        return true;
                    }
                }
            }
            return false;
        }

        private bool CheckIfBeatIndexIsInbetweenAnyHoldNoteAnchors(int beatIndex) {
            foreach(EditorNote parent in notesWithChildren.Values) {
                if(parent.ChildNotes == null) continue;
                if(beatIndex >= parent.BeatIndex
                    && beatIndex <= parent.ChildNotes[parent.ChildNotes.Length - 1].BeatIndex) {
                    return true;
                }
            }

            return false;
        }
        #endregion

        #region Getter
        /// <summary>
        /// Gets the note at the passed in beatIndex.
        /// Returns null if no note exists at that index.
        /// </summary>
        /// <param name="beatIndex">The beatIndex to get the note from.</param>
        /// <param name="checkAnchorsAndReturnParent">Whether to also check child notes (anchors).
        /// If null, does not check child notes, if true does check child notes and returns the parent note,
        /// if false does check child notes and returns the child note.</param>
        public EditorNote? GetNote(int beatIndex, bool? checkAnchorsAndReturnParent = null) {
            if(notes.ContainsKey(beatIndex)) return notes[beatIndex];

            if(checkAnchorsAndReturnParent != null) {
                foreach(EditorNote parent in notesWithChildren.Values) {
                    foreach(EditorChildNote child in parent.ChildNotes) {
                        if(child.BeatIndex == beatIndex) {
                            if((bool)checkAnchorsAndReturnParent) return parent;
                            else return new EditorNote {
                                BeatIndex = beatIndex,
                                Type = parent.Type,
                                Lane = child.Lane
                            };
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the lane chunk at the passed in beatIndex.
        /// Returns null if no chunk exists at that index.
        /// </summary>
        /// <param name="beatIndex">The beatIndex to get the chunk from.</param>
        public LaneChunkData? GetLaneChunk(int beatIndex) {
            if(laneChunks.ContainsKey(beatIndex)) return laneChunks[beatIndex];
            else return null;
        }
        #endregion

        #region Utilities
        /// <summary>
        /// Changes where notes are spawned and highlights the appropriate beat marker.
        /// </summary>
        /// <param name="newLocation">The new location to spawn notes at.</param>
        public void ChangeNoteSpawnLocation(NotePlacementLocation newLocation) {
            notePlacementLocation = newLocation;
            Settings.NotePlacementLocation = newLocation;

            if(isEditing) {
                if(newLocation != NotePlacementLocation.UnderCursor) {
                    beatMarkerUnderMouse = beatMarkerManager.HighlightOuterMarker(newLocation, BeatMarkerUnderMouse);
                } else {
                    beatMarkerManager.CancelOuterMarkerHighlight();
                }
            } else {
                beatMarkerManager.LoadValues(notePlacementLocation);
            }
        }

        private void CheckForNotesThatCameIntoViewArea(bool moveForward) {
            int[] beatsToCheck;
            if(ScrollingStepDistance > 1) beatsToCheck = moveForward ? beatMarkerManager.GetTopBeatIndices() : beatMarkerManager.GetBottomBeatIndices(true);
            else beatsToCheck = moveForward ? new int[] { beatMarkerManager.TopMarkerBeatIndex } : new int[] { beatMarkerManager.BottomMarkerBeatIndex };

            for(int i = 0; i < beatsToCheck.Length; i++) {
                if(notes.ContainsKey(beatsToCheck[i])) SpawnNote(notes[beatsToCheck[i]], true);

                foreach(KeyValuePair<int, EditorNote> item in notesWithChildren) {
                    EditorChildNote[] children = item.Value.ChildNotes;
                    if(children == null) continue;
                    for(int c = 0; c < children.Length; c++) {
                        if(children[c].BeatIndex == beatsToCheck[i]) {
                            if(moveForward && item.Value.Type == NoteType.LaneSwitch) {
                                SpawnLaneSwitchAnchor(new EditorNote() {
                                    BeatIndex = children[c].BeatIndex,
                                    Lane = children[c].Lane,
                                    Type = item.Value.Type
                                });
                            } else {
                                SpawnNote(new EditorNote() {
                                    BeatIndex = children[c].BeatIndex,
                                    Lane = children[c].Lane,
                                    Type = item.Value.Type
                                }, true);
                            }
                        }
                    }
                }
            }
            // check if the previous beat index had a final lane switch anchor
            if(!moveForward) {
                int previousBeatIndex = moveForward ? beatsToCheck[beatsToCheck.Length - 1] - 1 : beatsToCheck[beatsToCheck.Length - 1] + 1;
                foreach(EditorNote item in notesWithChildren.Values) {
                    if(item.ChildNotes == null || item.ChildNotes.Length == 0) continue;
                    EditorChildNote toCheck = item.ChildNotes[item.ChildNotes.Length - 1];
                    if(item.Type == NoteType.LaneSwitch
                    && toCheck.BeatIndex == previousBeatIndex) {
                        SpawnLaneSwitchAnchor(new EditorNote() {
                            BeatIndex = toCheck.BeatIndex,
                            Lane = toCheck.Lane,
                            Type = NoteType.LaneSwitch
                        });
                    }
                }
            }
        }
        #endregion
        #endregion

        #region BeatMarkers & BeatIndices
        /// <summary>
        /// Relays the call to the beatMarkerManager of the scene.
        /// Returns the height of the given beat marker.
        /// </summary>
        /// <param name="index">The beat marker to get the height of.</param>
        public float GetBeatMarkerHeight(MarkerIndex index) => beatMarkerManager.GetBeatMarkerHeightFromMarkerIndex(index);

        private int GetNotePlacementBeatIndex() {
            switch(notePlacementLocation) {
                case NotePlacementLocation.Top:
                    return beatMarkerManager.TopMarkerBeatIndex;
                case NotePlacementLocation.Bottom:
                    return beatMarkerManager.BottomMarkerBeatIndex;
                case NotePlacementLocation.UnderCursor:
                    beatMarkerManager.GetMarkerValues(BeatMarkerUnderMouse, out int beatIndex);
                    return beatIndex;
                default:
                    return beatMarkerManager.BottomMarkerBeatIndex;
            }
        }
        #endregion

        #region Lane Chunks
        /// <summary>
        /// Adds the passed in chunks to the chunks of the level.
        /// Checks if it would overlap with an already existing one or if its even possible to be spawned.
        /// Does not add one if so. Returns whether a chunk was added or not.
        /// If a chunk was added, updates corresponding values for surrounding chunks (start / end).
        /// </summary>
        /// <param name="startBeatIndex">The beat index the new chunk starts at.</param>
        /// <param name="laneCount">The amount of lanes the chunk has.</param>
        /// <param name="showErrorMessageIfUnsuccessful">Should an error message be displayed if the chunk would overlap?</param>
        public bool AddLaneChunk(int startBeatIndex, int laneCount, bool showErrorMessageIfUnsuccessful = false) {
            int endBeatIndex = GetNextLaneChunkStartBeatIndex(startBeatIndex);
            if(laneChunks.ContainsKey(startBeatIndex)) {
                if(showErrorMessageIfUnsuccessful) Debug.LogError($"Tried to add a lane chunk from {startBeatIndex} to {endBeatIndex} which would overlap with an already existing one!");
                return false;
            }

            SetPreviousLaneChunksEnd(startBeatIndex);
            laneChunks.Add(startBeatIndex, new LaneChunkData { StartBeatIndex = startBeatIndex, EndBeatIndex = endBeatIndex, LaneCount = laneCount });
            return true;
        }

        /// <summary>
        /// Adds the passed in chunks (as a temporary one) to the chunks of the level.
        /// It becomes part of the level and can still be accessed / deleted like every other chunk.
        /// Checks if it would overlap with an already existing one or if it's even possible to be spawned.
        /// Does not add one if so. Returns whether a chunk was added or not.
        /// If a chunk was added, updates correspoonding values of surrounding chunks (start / end).
        /// </summary>
        /// <param name="startBeatIndex">The beatIndex the new chunk starts at.</param>
        /// <param name="temporaryLaneCount">The temporary amount of lanes, this chunk has.</param>
        /// <param name="actualLaneCount">The amount of lanes this chunk has after becoming permanent.</param>
        /// <param name="showErrorMessageIfUnsuccessful">Whether an error should be displayed if the chunk would overlap.</param>
        public bool AddTemporaryLaneChunk(int startBeatIndex, int temporaryLaneCount, int actualLaneCount, bool showErrorMessageIfUnsuccessful = false) {
            int endBeatIndex = GetNextLaneChunkStartBeatIndex(startBeatIndex);
            if(laneChunks.ContainsKey(startBeatIndex)) {
                if(showErrorMessageIfUnsuccessful) Debug.LogError($"Tried to add a lane chunk from {startBeatIndex} to {endBeatIndex} which would overlap with an already existing one!");
                return false;
            }

            SetPreviousLaneChunksEnd(startBeatIndex);
            laneChunks.Add(startBeatIndex, new LaneChunkData { StartBeatIndex = startBeatIndex, EndBeatIndex = endBeatIndex, LaneCount = actualLaneCount, TemporaryLaneCount = temporaryLaneCount });
            return true;
        }

        /// <summary>
        /// Makes the lane chunk at the passed in startBeatIndex temporary.
        /// </summary>
        /// <param name="startBeatIndex">The beatIndex the lane chunk starts at.</param>
        /// <param name="temporaryLaneCount">The amount of temporary lanes.</param>
        /// <param name="actualLaneCount">The amount of lanes when permanent.</param>
        /// <param name="showErrorMessageIfUnsuccesful">Whether to give error feedback if no such lane chunk is found.</param>
        public void MakeLaneChunkTemporary(int startBeatIndex, int temporaryLaneCount, int actualLaneCount, bool showErrorMessageIfUnsuccesful = false) {
            if(!laneChunks.ContainsKey(startBeatIndex)) {
                if(showErrorMessageIfUnsuccesful) Debug.LogError($"Tried to make a permanent lane chunk temporary, but there is no chunk starting at {startBeatIndex}.");
                return;
            }

            LaneChunkData data = laneChunks[startBeatIndex];
            data.TemporaryLaneCount = temporaryLaneCount;
            laneChunks[startBeatIndex] = data;
        }

        /// <summary>
        /// Changes all temporary lane chunks of the level into permanent ones, deleting all their
        /// temporary lanes.
        /// </summary>
        public void ChangeTemporaryLaneChunksToPermanent() {
            Dictionary<int, LaneChunkData> permanentChunks = new Dictionary<int, LaneChunkData>();
            foreach(KeyValuePair<int, LaneChunkData> item in laneChunks) {
                permanentChunks.Add(item.Key, new LaneChunkData() {
                    StartBeatIndex = item.Value.StartBeatIndex,
                    EndBeatIndex = item.Value.EndBeatIndex,
                    LaneCount = item.Value.LaneCount,
                    TemporaryLaneCount = 0
                });
            }
            laneChunks = permanentChunks;
            laneManager.ChangeActiveChunksToPermanents();
        }

        /// <summary>
        /// Removes a lane chunk from the data of the level, updating the previous lane chunk
        /// to now end where the removed lane chunk ended.
        /// </summary>
        /// <param name="startBeatIndex">The beatIndex where the lane chunk to be removed starts.</param>
        /// <param name="errorDisplayType">Which type of error message should be displayed, if no such chunk is found.</param>
        public void RemoveLaneChunk(int startBeatIndex, ErrorMessageDisplayType errorDisplayType = ErrorMessageDisplayType.Ignore) {
            if(!laneChunks.ContainsKey(startBeatIndex)) {
                switch(errorDisplayType) {
                    case ErrorMessageDisplayType.Console:
                        Debug.LogError($"Tried to remove lane chunk starting at {startBeatIndex} form the level, but no such chunk exists!");
                        break;
                    case ErrorMessageDisplayType.OnScreen:
                        popupUtility.ShowOnScreenMessage($"Tried to remove lane chunk starting at {startBeatIndex} form the level, but no such chunk exists!", isErrorMessage: true);
                        break;
                    case ErrorMessageDisplayType.Popup:
                        popupUtility.ShowPopup("No chunk found", $"Tried to remove lane chunk starting at {startBeatIndex} form the level, but no such chunk exists!", isErrorPopup: true);
                        break;
                }

                return;
            }

            SetPreviousLaneChunksEnd(startBeatIndex, laneChunks[startBeatIndex].EndBeatIndex);
            laneChunks.Remove(startBeatIndex);
        }

        /// <summary>
        /// Gets the endBeatIndex for the given chunk from the dictionary.
        /// </summary>
        /// <param name="startBeatIndex">The beatIndex the chunk starts at (key of the dictionary).</param>
        public int? GetLaneChunkEndBeatIndex(int startBeatIndex) {
            if(laneChunks.ContainsKey(startBeatIndex)) return laneChunks[startBeatIndex].EndBeatIndex;
            else return null;

            throw new System.Exception($"Tried to receive data for the chunk at {startBeatIndex} which doesn't exist!");
        }

        /// <summary>
        /// Gets the closest chunk (moving forward in the timeline) to the passed in start beat index.
        /// </summary>
        /// <param name="startBeatIndex">The start from which to search forward.</param>
        public int GetNextLaneChunkStartBeatIndex(int startBeatIndex) {
            int closestStart = EndOfSongBeatIndex;
            foreach(KeyValuePair<int, LaneChunkData> chunk in laneChunks) {
                if(chunk.Key > startBeatIndex && chunk.Key < closestStart) closestStart = chunk.Key;
            }

            return closestStart;
        }

        /// <summary>
        /// Returns the lane chunk data that is responsbile for the passed in beatIndex.
        /// (aka the one where the beatIndex lies between its start and end).
        /// </summary>
        /// <param name="beatIndex">The beatIndex to get the lane chunk for.</param>
        public LaneChunkData GetLaneChunkResponsibleForBeatIndex(int beatIndex) {
            LaneChunkData result = default;
            foreach(KeyValuePair<int, LaneChunkData> chunk in laneChunks) {
                if(chunk.Value.StartBeatIndex <= beatIndex && chunk.Value.EndBeatIndex > beatIndex) {
                    result = chunk.Value;
                    break;
                }
            }
            return result;
        }

        /// <summary>
        /// Checks if any of the levels lane chunks came into the view area of the editor.
        /// Returns true if so, false if none came into view.
        /// </summary>
        /// <param name="checkForward">Whether the view area was moved downwards (true) or upwards (false).</param>
        /// <param name="data">The chunk that came into view.</param>
        public bool CheckIfNewLaneChunkDataCameIntoEditorView(bool checkForward, out LaneChunkData data) {
            int[] beatsToCheck = checkForward ? beatMarkerManager.GetTopBeatIndices() : beatMarkerManager.GetBottomBeatIndices(false);

            if(checkForward) {
                for(int i = 0; i < beatsToCheck.Length; i++) {
                    if(laneChunks.ContainsKey(beatsToCheck[i] - 1)) {
                        data = laneChunks[beatsToCheck[i] - 1];
                        return true;
                    }
                }
            } else {
                for(int i = 0; i < beatsToCheck.Length; i++) {
                    if(CheckIfAnyLaneChunkEndsAtBeatIndex(beatsToCheck[i], out int foundStartIndex)) {
                        data = laneChunks[foundStartIndex];
                        return true;
                    }
                }
            }

            data = default;
            return false;
        }

        private bool CheckIfAnyLaneChunkEndsAtBeatIndex(int beatIndex, out int foundDataStartBeatIndex) {
            foreach(KeyValuePair<int, LaneChunkData> chunk in laneChunks) {
                if(chunk.Value.EndBeatIndex == beatIndex) {
                    foundDataStartBeatIndex = chunk.Key;
                    return true;
                }
            }

            foundDataStartBeatIndex = -1;
            return false;
        }

        private List<LaneChunkData> GetAllLaneChunksInInvetval(int bottomBeatIndex, int topBeatIndex) {
            List<LaneChunkData> result = new List<LaneChunkData>();
            int closestStartBeatIndex = 0;
            foreach(KeyValuePair<int, LaneChunkData> chunk in laneChunks) {
                if(chunk.Value.StartBeatIndex <= bottomBeatIndex && chunk.Value.StartBeatIndex > closestStartBeatIndex) {
                    closestStartBeatIndex = chunk.Key;
                } else if(chunk.Value.StartBeatIndex > bottomBeatIndex && chunk.Value.StartBeatIndex < topBeatIndex) {
                    result.Add(chunk.Value);
                }
            }

            result.Add(laneChunks[closestStartBeatIndex]);
            return result;
        }

        /// <summary>
        /// The newEndBeatIndex is the startBeatIndex of the chunk beaing removed.
        /// </summary>
        private void SetPreviousLaneChunksEnd(int newEndBeatIndex) {
            if(laneChunks.Count == 0) return;

            int closestStart = 0;
            foreach(KeyValuePair<int, LaneChunkData> chunk in laneChunks) {
                if(chunk.Key < newEndBeatIndex && chunk.Key > closestStart) closestStart = chunk.Key;
            }

            LaneChunkData data = laneChunks[closestStart];
            data.EndBeatIndex = newEndBeatIndex;
            laneChunks[closestStart] = data;

            OnLaneChunkWasModified?.Invoke(data);
        }

        private void SetPreviousLaneChunksEnd(int startBeatIndex, int newEndBeatIndex) {
            if(laneChunks.Count == 0) return;

            int closestStart = 0;
            foreach(KeyValuePair<int, LaneChunkData> chunk in laneChunks) {
                if(chunk.Key < startBeatIndex && chunk.Key > closestStart) closestStart = chunk.Key;
            }

            LaneChunkData data = laneChunks[closestStart];
            data.EndBeatIndex = newEndBeatIndex;
            laneChunks[closestStart] = data;

            OnLaneChunkWasModified?.Invoke(data);
        }
        #endregion

        #region Setting Serialization
        /// <summary>
        /// Returns the file path to the settings file.
        /// </summary>
        public string GetSettingsFilePath() => Application.persistentDataPath + "/levelEditor.settings";

        public string GetSettingsFolderPath() => Application.persistentDataPath;

        /// <summary>
        /// Serializes the currently selected settings into the settings file.
        /// </summary>
        public void SaveEditorSettings() {
            OnBeforeSettingsWereSaved?.Invoke();

            string filePath = GetSettingsFilePath();
            string jsonContent = JsonUtility.ToJson(Settings, true);
            File.WriteAllText(filePath, jsonContent);
        }

        /// <summary>
        /// Loads the settings file and sets all settings accordingly.
        /// </summary>
        public void LoadEditorSettings() {
            string filePath = GetSettingsFilePath();
            if(File.Exists(filePath)) {
                string jsonContent = File.ReadAllText(filePath);
                Settings = JsonUtility.FromJson<LevelEditorSettingsData>(jsonContent);
            } else {
                Settings = new LevelEditorSettingsData() {
                    NotePlacementLocation = NotePlacementLocation.Bottom,
                    BeatMarkerCount = 10,
                    BPMAdjustmentIndicatorVolume = 0.3f,
                    BPMAdjustmentBackgroundAudioVolume = 0f,
                    PlaybackMusicVolume = 0.3f,
                    PlaybackNoteVolume = 0.3f
                };
            }

            ChangeNoteSpawnLocation(Settings.NotePlacementLocation);
            beatMarkerManager.SetBeatMarkerInfoType(Settings.BeatMarkerInfoType, true);

            OnSettingsWereLoaded?.Invoke();
        }

        [Button("Open Settings Folder")]
        public void OpenSettingsFolder() {
#if UNITY_EDITOR
            string path = File.Exists(GetSettingsFilePath()) ? GetSettingsFilePath() : GetSettingsFolderPath();
            EditorUtility.RevealInFinder(path);
#endif
        }
        #endregion

        #region Popup Callbacks
        // These methods are callbacks to the popupUtility and should not be directly called by this class.

        [System.Obsolete]
        private void OnNoLevelFileDetectedCallbackReceiver(bool choice) {
            if(choice) {
                //Settings.ShowNoLevelFileDetectedMessage = false;
                SaveEditorSettings();
            }
        }

        /// <summary>
        /// Callback receiver. Should not be invoked by this class.
        /// </summary>
        private void OnIncompleteHoldNoteCallbackReceiver(bool result) {
            if(result) {
                HoldNoteEditor.Instance.DeleteNote();
                StopEditingHoldNote();
            }
        }

        private void PlaceNewLaneSwitchNoteCallbackReceiver(int choice, int? input) {
            if(choice == 0 && input != null) {
                if((int)input <= 1) {
                    popupUtility.ShowPopup("Invalid Lane Count", "A lane switch note has to have at least 2 Lanes!", isErrorPopup: true);
                    Lane.DestroyLastPlacedNote();
                    return;
                }
                CurrentEditorState = LevelEditorState.HoldNotePlacement;
                HoldNoteEditor.Instance.OpenNewNote(savedLaneBeforePopupUI.LaneIndex,
                    beatMarkerManager.GetBeatMarkerBeatIndexFromMarkerIndex(savedMarkerBeforePopupUI), (int)input);
            } else {
                Lane.DestroyLastPlacedNote();
            }
        }

        private void PlaceNewEventNoteCallbackReceiver(EditorNote note, int choice, int selectedIndex) {
            if(choice == 1) {
                Lane.DestroyLastPlacedNote();
                return;
            }

            NoteEvent e = (NoteEvent)selectedIndex;
            LaneNote? lastNote = Lane.GetLastPlacedNote();
            if(lastNote == null) return;

            if(e == NoteEvent.Story) {
                popupUtility.ShowInputPopup("Story ID", 0, (choice, result) => PlaceNewStoryNoteCallbackReceiver(note, choice, result),
                    "Apply", "Cancel");
            } else {
                note.EventType = e;
                UpdateNote(note, true);
                ((LaneNote)lastNote).Rect.GetComponent<EventNoteVisuals>().EventType = e;
            }
        }

        private void PlaceNewStoryNoteCallbackReceiver(EditorNote note, int choice, int? result) {
            if(choice == 1 || result == null) {
                Lane.DestroyLastPlacedNote();
                return;
            }

            if(result < 0 || result > byte.MaxValue) {
                popupUtility.ShowOnScreenMessage("Story IDs should be 0 >= x <= 255", isErrorMessage: true);
                Lane.DestroyLastPlacedNote();
                return;
            }

            note.EventType = NoteEvent.Story;
            note.StoryEventID = (byte)result;
            UpdateNote(note, true);
            EventNoteVisuals visuals = ((LaneNote)Lane.GetLastPlacedNote()).Rect.GetComponent<EventNoteVisuals>();
            visuals.EventType = NoteEvent.Story;
            visuals.StoryID = note.StoryEventID;
        }
        #endregion
    }
}
