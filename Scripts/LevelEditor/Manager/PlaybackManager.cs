using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Manages playback of the song without disrupting the data of the level.
    /// 
    /// Author: Kevin Andersch
    /// Date: 28.04.22
    /// Version: 1.0
    /// </summary>
    public class PlaybackManager : MonoBehaviour {
        [Header("References")]
        [SerializeField] private LevelEditor levelEditor;
        [SerializeField] private CanvasScaler canvasScaler;
        [SerializeField] private GameObject settingsPanel;
        [SerializeField] private ToggleButtonWithVolume noteVolumeToggle;
        [SerializeField] private ToggleButtonWithVolume musicVolumeToggle;
        [SerializeField] private AudioSource noteAudioSource;
        [SerializeField] private Slider playbackSpeedSlider;

        public bool PlaybackIsRunning { get => playbackRunning; }

        private bool playbackRunning;
        private List<RectTransform> notes;
        private float hitBeatHeight;
        private float spawnNoteHeight;
        private float verticalMovementPerSecond;
        private bool keyListenerEnabled;
        private float nextSpawnTime;
        private int topBeatIndex;
        private int bottomBeatIndex;
        private float musicVolume;
        private float noteVolume;
        private float timePerMajorBeat;
        private float timeSinceLastMajorBeat;
        private bool canBeCanceled;

        private LaneManager laneManager;
        private BeatMarkerManager beatMarkerManager;

        private bool playSingleBeat = false;
        private int previousBeatIndex;

        // Events
        public static System.Action OnPlaybackBegan;
        public static System.Action OnPlaybackEnded;

        private void Start() {
            laneManager = levelEditor.LaneManager;
            beatMarkerManager = levelEditor.BeatMarkerManager;

            LevelEditor.OnEditorWasOpened += EnableKeyListener;
            LevelEditor.OnBeforeEditorWasClosed += DisableKeyListener;
        }

        private void OnDisable() {
            LevelEditor.OnEditorWasOpened -= EnableKeyListener;
            LevelEditor.OnBeforeEditorWasClosed -= DisableKeyListener;
        }

        private void Update() {
            if(!keyListenerEnabled || PopupUtility.PopupIsActive
                || levelEditor.CurrentEditorState == LevelEditorState.HoldNotePlacement) return;

            if(Input.GetKeyDown(KeyCode.Space)) {
                if(!playbackRunning) BeginPlayback();
                else if(canBeCanceled) EndPlayback();
            }

            if(Input.GetKeyDown(KeyCode.S)) {
                if(!playbackRunning) PlaySingleBeat();
            }

            if(playbackRunning) RunPlayback();
        }

        /// <summary>
        /// Plays a single (mini) beat and then stops the playback and sets the
        /// view area back to where it was.
        /// </summary>
        public void PlaySingleBeat() {
            if(playbackRunning) return;
            playSingleBeat = true;
            previousBeatIndex = beatMarkerManager.BottomMarkerBeatIndex;
            BeginPlayback();
        }

        public void UpdatePlaybackHeights(float spawnHeight, float despawnHeight) {
            spawnNoteHeight = spawnHeight;
            hitBeatHeight = despawnHeight;
        }

        private void BeginPlayback() {
            levelEditor.LaneUnderPointer?.ResetColor();
            notes = laneManager.DecoupleNotesFromLanes();
            hitBeatHeight = beatMarkerManager.GetBeatMarkerHeightFromMarkerIndex(new MarkerIndex() { NormalMarkerIndex = -1 });
            spawnNoteHeight = beatMarkerManager.GetBeatMarkerHeightFromMarkerIndex(new MarkerIndex() { NormalMarkerIndex = 999 });
            timePerMajorBeat = LevelDataUtilities.GetBPMFrequency(levelEditor.Level.Bpm);
            verticalMovementPerSecond = beatMarkerManager.BeatMarkerDistance
                * (1 / timePerMajorBeat);

            bottomBeatIndex = beatMarkerManager.BottomMarkerBeatIndex;
            float startTime = LevelDataUtilities.ConvertBeatIndexToTime(bottomBeatIndex, levelEditor.Level);
            topBeatIndex = beatMarkerManager.TopMarkerBeatIndex + 1;
            nextSpawnTime = LevelDataUtilities.ConvertBeatIndexToTime(bottomBeatIndex + 1, levelEditor.Level);
            LevelEditorMusicPlayer.Instance.SetBackgroundMusicVolume(musicVolume);
            LevelEditorMusicPlayer.Instance.PlayBackgroundMusic(startTime);
            noteAudioSource.volume = noteVolume;

            playbackRunning = true;
            OnPlaybackBegan?.Invoke();
            canBeCanceled = true;
        }

        private void RunPlayback() {
            float speed = -verticalMovementPerSecond * Time.deltaTime * playbackSpeedSlider.value;

            RectTransform noteToRemove = null;
            foreach(RectTransform note in notes) {
                note.anchoredPosition = new Vector2(note.anchoredPosition.x, note.anchoredPosition.y + speed);
                if(note.transform.position.y <= hitBeatHeight) {
                    PlayNoteAudio(note.tag);
                    noteToRemove = note;
                }
            }
            if(noteToRemove != null) {
                notes.Remove(noteToRemove);
                Destroy(noteToRemove.gameObject);
            }

            if(LevelEditorMusicPlayer.Instance.GetCurrentSongTime() >= nextSpawnTime) {
                if(playSingleBeat) {
                    EndPlayback();
                    return;
                }
                EditorNote? note = levelEditor.GetNote(topBeatIndex, false);
                LaneChunkData? chunk = levelEditor.GetLaneChunk(topBeatIndex);
                topBeatIndex++;
                bottomBeatIndex++;
                beatMarkerManager.SetBottomMarkerBeatIndexWithoutUpdate(bottomBeatIndex);
                beatMarkerManager.SwapPlaybackMarkers(spawnNoteHeight, true);
                nextSpawnTime = LevelDataUtilities.ConvertBeatIndexToTime(bottomBeatIndex + 1, levelEditor.Level);
                timeSinceLastMajorBeat = 0f;

                if(chunk != null) {
                    laneManager.SpawnLaneChunk((LaneChunkData)chunk);
                }

                if(note != null) {
                    EditorNote noteToSpawn = (EditorNote)note;
                    Lane lane = noteToSpawn.Type == NoteType.LaneSwitch ?
                        laneManager.GetLaneSwitchAnchorLaneAtBeatIndex(noteToSpawn.BeatIndex, noteToSpawn.Lane)
                        : laneManager.GetLaneAtBeatIndex(noteToSpawn.BeatIndex, noteToSpawn.Lane);
                    notes.Add(NoteSpawner.Instance.SpawnNote(noteToSpawn, new Vector2(lane.transform.position.x, spawnNoteHeight), levelEditor.NoteHolder, true).GetComponent<RectTransform>());
                }
            }

            beatMarkerManager.MovePlaybackMarkers(speed, hitBeatHeight, spawnNoteHeight);
            laneManager.MoveAllLanesDuringPlayback(bottomBeatIndex);

            // TODO: handle end of song
        }

        private void EndPlayback() {
            playbackRunning = false;
            DestroyAllNotes();
            laneManager.DestroyAllLaneChunks();
            levelEditor.LoadViewArea(bottomBeatIndex, topBeatIndex - 1);
            LevelEditorMusicPlayer.Instance.StopBackgroundMusic();
            levelEditor.LaneUnderPointer?.Highlight();

            canBeCanceled = false;
            playSingleBeat = false;
            SaveSettingsData();
            OnPlaybackEnded?.Invoke();
        }

        private void PlayNoteAudio(string tag) {
            AudioClip clip = null;
            switch(tag) {
                case "NoteNormal":
                    clip = LevelEditorMusicPlayer.Instance.PlaybackNormalNoteSound;
                    break;
                case "NoteGaze":
                    clip = LevelEditorMusicPlayer.Instance.PlaybackGazeNoteSound;
                    break;
                case "NoteHold":
                    clip = LevelEditorMusicPlayer.Instance.PlaybackHoldNoteSound;
                    break;
                case "NoteLaneSwitch":
                    clip = LevelEditorMusicPlayer.Instance.PlaybackHoldNoteSound;
                    break;
                case "NoteEvent":
                    clip = LevelEditorMusicPlayer.Instance.PlaybackNormalNoteSound;
                    break;
            }

            noteAudioSource.PlayOneShot(clip);
        }

        private void DestroyAllNotes() {
            foreach(RectTransform note in notes) {
                Destroy(note.gameObject);
            }
            notes = null;
        }

        #region Callback Receiver
        public void OnPlaybackSpeedChanged() {
            LevelEditorMusicPlayer.Instance.SetPitch(playbackSpeedSlider.value);
        }

        private void EnableKeyListener() {
            keyListenerEnabled = true;
            musicVolume = levelEditor.Settings.PlaybackMusicVolume;
            noteVolume = levelEditor.Settings.PlaybackNoteVolume;
            noteVolumeToggle.Initialize(noteVolume);
            musicVolumeToggle.Initialize(musicVolume);
            settingsPanel.SetActive(true);

            noteVolumeToggle.OnVolumeChanged += OnNoteVolumeChangedCallbackReceiver;
            musicVolumeToggle.OnVolumeChanged += OnMusicVolumeChangedCallbackReceiver;
        }

        private void DisableKeyListener() {
            noteVolumeToggle.OnVolumeChanged -= OnNoteVolumeChangedCallbackReceiver;
            musicVolumeToggle.OnVolumeChanged -= OnMusicVolumeChangedCallbackReceiver;

            keyListenerEnabled = false;
            settingsPanel.SetActive(false);
            SaveSettingsData();
        }

        private void SaveSettingsData() {
            LevelEditorSettingsData data = levelEditor.Settings;
            data.PlaybackMusicVolume = musicVolume;
            data.PlaybackNoteVolume = noteVolume;
        }

        private void OnMusicVolumeChangedCallbackReceiver(float value) {
            musicVolume = value;
            if(playbackRunning) LevelEditorMusicPlayer.Instance.SetBackgroundMusicVolume(value);
        }

        private void OnNoteVolumeChangedCallbackReceiver(float value) {
            noteVolume = value;
            if(playbackRunning) noteAudioSource.volume = value;
        }
        #endregion
    }
}
