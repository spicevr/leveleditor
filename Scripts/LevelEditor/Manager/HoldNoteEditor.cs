using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Aids in editing (and creating) hold notes.
    /// 
    /// Author: Kevin Andersch
    /// Date: 09.05.22
    /// Version: 1.0
    /// </summary>
    public class HoldNoteEditor : MonoBehaviour {
        public static HoldNoteEditor Instance { get; private set; }

        [SerializeField] private GameObject[] disablingPanels;
        [SerializeField] private LevelEditor levelEditor;
        [SerializeField] private RectTransform editingDisclaimer;
        [SerializeField] private PopupUtility popupUtility;

        private SortedList<int, EditorNote> noteData;
        private int laneSwitchNewLaneCount;
        private int previousChunkLaneCount;

        private List<EditorNote> notesToDelete;
        private System.Action savedAction;

        // Events
        public static System.Action<EditorNote> OnAnchorWasAdded;
        public static System.Action<EditorNote> OnAnchorWasRemoved;

        private void Awake() {
            if(!Instance) Instance = this;
            else Destroy(gameObject);
        }

        /// <summary>
        /// Opens a completely new hold note to be edited.
        /// </summary>
        /// <param name="lane">The lane of the first (and only) anchor of the note.</param>
        /// <param name="beatIndex">The beatIndex of the first (and only) anchor of the note.</param>
        public void OpenNewNote(int lane, int beatIndex, int laneSwitch = -1) {
            noteData = new SortedList<int, EditorNote>();
            laneSwitchNewLaneCount = laneSwitch;
            previousChunkLaneCount = levelEditor.GetLaneChunkResponsibleForBeatIndex(beatIndex).LaneCount;

            EditorNote note = new EditorNote() { BeatIndex = beatIndex, Lane = lane };
            noteData.Add(beatIndex, note);
            levelEditor.AddNote(note);
            UpdateDisablingPanels(true);
        }

        /// <summary>
        /// Reads the data of an existing hold note and sets it to be the currently edited one.
        /// </summary>
        /// <param name="beatIndex">The beatIndex of the first anchor of the hold note to open.</param>
        public void OpenExistingNote(int beatIndex) {
            previousChunkLaneCount = levelEditor.GetLaneChunkResponsibleForBeatIndex(beatIndex).LaneCount;
            noteData = LoadHoldNoteData(beatIndex);
            if(laneSwitchNewLaneCount > 0) {
                int finalBeatIndex = noteData.Values[noteData.Values.Count - 1].BeatIndex;
                levelEditor.MakeLaneChunkTemporary(finalBeatIndex, previousChunkLaneCount, laneSwitchNewLaneCount, true);
                if(finalBeatIndex >= levelEditor.BeatMarkerManager.BottomMarkerBeatIndex
                    && finalBeatIndex < levelEditor.BeatMarkerManager.TopMarkerBeatIndex) {
                    levelEditor.LaneManager.UpdateTemporaryLaneChunk(finalBeatIndex, previousChunkLaneCount);
                }
            }
            UpdateDisablingPanels(true);
        }

        /// <summary>
        /// Adds an anchor point to the currently being edited hold note.
        /// </summary>
        /// <param name="lane">The lane of the new anchor point.</param>
        /// <param name="beatIndex">The beatIndex of the new anchor point.</param>
        /// <param name="height">The height at which the note should spawn.</param>
        public void AddAnchorPoint(Lane lane, int beatIndex, float height) {
            if(!IsValidPlacement(beatIndex)) return;

            if(CheckIfThereAreNotesInbetween(beatIndex, out List<EditorNote> notes)) {
                notesToDelete = notes;
                savedAction = () => AddAnchorPoint(lane, beatIndex, height);
                popupUtility.ShowPopup("Overlapping Notes", "There are notes between this anchor and the next one." +
                    "\nDo you wish to delete those notes?", "Delete Notes", "Cancel", OnInbetweenNotesWereFoundCallbackReceiver, true); ;
                return;
            }

            NoteType noteType = laneSwitchNewLaneCount <= 0 ? NoteType.Hold : NoteType.LaneSwitch;

            noteData.Add(beatIndex, new EditorNote() {
                BeatIndex = beatIndex,
                Lane = lane.LaneIndex
            });

            // TODO: update visual connections
            if(noteData.IndexOfKey(beatIndex) == 0) {
                levelEditor.DeleteNote(noteData.Values[1].BeatIndex);
                levelEditor.AddNote(CreateFinishedHoldNoteData(), errorMessageOnFailure: ErrorMessageDisplayType.Console);

                // if this is the second anchor, create the (first) lane switch...
                if(laneSwitchNewLaneCount > 0 && noteData.Count == 2) {
                    int beatIndexToUse = noteData.Values[noteData.Count - 1].BeatIndex;
                    if(levelEditor.AreThereNotesAfterBeatIndex(beatIndexToUse, out List<EditorNote> foundNotes)) {
                        popupUtility.ShowPopup("Later Notes Found", $"There are notes behind this lane switch!" +
                            $"\nChanging the amount of lanes here would result in {foundNotes.Count} notes to be removed." +
                            $"\nWould you like to proceed?", $"Delete {foundNotes.Count} Notes", "Cancel", (choice) => {
                                if(choice) {
                                    DeleteNotes(foundNotes);
                                    CreateNewLaneSwitchAnchor(beatIndexToUse, lane, out lane);
                                } else {
                                    //Lane.DeleteLastPlacedNote();
                                    //noteData.Remove(beatIndexToUse);
                                    RemoveAnchorPoint(beatIndex);
                                }
                            }, true);
                    } else {
                        CreateNewLaneSwitchAnchor(beatIndexToUse, lane, out lane);
                    }
                }
            } else {
                if(laneSwitchNewLaneCount > 0) {
                    // if this is the new final anchor of a laneSwitch note...
                    if(noteData.IndexOfKey(beatIndex) == noteData.Count - 1) {
                        int beatIndexToUse = noteData.Values[noteData.Count - 1].BeatIndex;
                        if(levelEditor.AreThereNotesAfterBeatIndex(beatIndexToUse, out List<EditorNote> foundNotes)) {
                            popupUtility.ShowPopup("Later Notes Found", $"There are notes behind this lane switch!" +
                                $"\nChanging the amount of lanes here would result in {foundNotes.Count} notes to be removed." +
                                $"\nWould you like to proceed?", $"Delete {foundNotes.Count} Notes", "Cancel", (choice) => {
                                    if(choice) {
                                        DeleteNotes(foundNotes);
                                        CreateNewLaneSwitchAnchor(beatIndexToUse, lane, out lane);
                                    } else {
                                        //Lane.DeleteLastPlacedNote();
                                        //noteData.Remove(beatIndexToUse);
                                        RemoveAnchorPoint(beatIndex);
                                    }
                                }, true);
                        } else {
                            CreateNewLaneSwitchAnchor(beatIndexToUse, lane, out lane);
                        }
                    }
                }
                levelEditor.UpdateNote(CreateFinishedHoldNoteData(), true);
            }
            EditorNote convertedNoted = new EditorNote { Lane = lane.LaneIndex, Type = noteType, BeatIndex = beatIndex };
            lane.SpawnNoteAtCustomHeight(convertedNoted, height, beatIndex, true);

            OnAnchorWasAdded?.Invoke(new EditorNote() {
                BeatIndex = beatIndex,
                Lane = lane.LaneIndex
            });
        }

        /// <summary>
        /// Removes the anchor at the passed in beatIndex from this hold note (if the cursor
        /// is over the same lane as the anchor) and restructures
        /// the hold note accordingly. This removes both the visual representation as well as
        /// the data of the level.
        /// </summary>
        /// <param name="beatIndex">The beatIndex of the anchor to remove.</param>
        /// <param name="lane">The lane the cursor is currently over.</param>
        public bool RemoveAnchorPoint(int beatIndex, int lane) {
            bool result = noteData.ContainsKey(beatIndex);
            if(!result) return false;

            Lane l = laneSwitchNewLaneCount > 0 ? levelEditor.LaneManager.GetLaneSwitchAnchorLaneAtBeatIndex(beatIndex, lane)
                : levelEditor.LaneManager.GetLaneAtBeatIndex(beatIndex, lane);
            if(!l.DeleteNote(beatIndex)) return false;

            int i = noteData.IndexOfKey(beatIndex);
            int beatIndexOfFinalAnchor = noteData.Values[noteData.Count - 1].BeatIndex;

            if(noteData.Count == 1) {
                levelEditor.DeleteNote(noteData[beatIndex]);
                UpdateDisablingPanels(false);
                levelEditor.StopEditingHoldNote();
                return true;
            }

            if(i == 0) {
                levelEditor.DeleteNote(beatIndex);
                noteData.Remove(beatIndex);
                levelEditor.AddNote(CreateFinishedHoldNoteData());
                if(laneSwitchNewLaneCount > 0 && noteData.Count == 1) {
                    List<LaneNote> notesToReassign = null;
                    if(beatIndexOfFinalAnchor >= levelEditor.BeatMarkerManager.BottomMarkerBeatIndex
                        && beatIndexOfFinalAnchor < levelEditor.BeatMarkerManager.TopMarkerBeatIndex) {
                        levelEditor.LaneManager.DeleteLaneChunk(beatIndexOfFinalAnchor, out notesToReassign, ErrorMessageDisplayType.Ignore);
                    } else levelEditor.RemoveLaneChunk(beatIndexOfFinalAnchor, ErrorMessageDisplayType.Ignore);

                    if(notesToReassign != null) ReassignNotes(notesToReassign);
                }
            } else {
                noteData.Remove(beatIndex);
                levelEditor.UpdateNote(CreateFinishedHoldNoteData(), true);
                if(laneSwitchNewLaneCount > 0 && i == noteData.Count) {
                    List<LaneNote> notesToReassign = null;
                    if(beatIndexOfFinalAnchor >= levelEditor.BeatMarkerManager.BottomMarkerBeatIndex
                        && beatIndexOfFinalAnchor < levelEditor.BeatMarkerManager.TopMarkerBeatIndex) {
                        levelEditor.LaneManager.DeleteLaneChunk(beatIndexOfFinalAnchor, out notesToReassign, ErrorMessageDisplayType.Ignore);
                    } else levelEditor.RemoveLaneChunk(beatIndexOfFinalAnchor, ErrorMessageDisplayType.Ignore);

                    if(noteData.Count != 1) {
                        beatIndexOfFinalAnchor = noteData.Values[noteData.Count - 1].BeatIndex;
                        if(beatIndexOfFinalAnchor < levelEditor.BeatMarkerManager.TopMarkerBeatIndex) {
                            levelEditor.LaneManager.CreateNewTemporaryLaneChunk(previousChunkLaneCount, laneSwitchNewLaneCount, levelEditor, beatIndexOfFinalAnchor);
                        } else levelEditor.AddTemporaryLaneChunk(beatIndexOfFinalAnchor, previousChunkLaneCount, laneSwitchNewLaneCount, true);
                    }

                    if(notesToReassign != null) ReassignNotes(notesToReassign);
                }
            }

            return true;
        }

        /// <summary>
        /// Removes the anchor at the passed in beatIndex from this hold note and restructures
        /// the hold note accordingly. This removes both the visual representation as well as
        /// the data of the level.
        /// </summary>
        /// <param name="beatIndex">The beatIndex of the anchor to remove.</param>
        public bool RemoveAnchorPoint(int beatIndex) {
            bool result = noteData.ContainsKey(beatIndex);
            if(!result) return false;
            return RemoveAnchorPoint(beatIndex, noteData[beatIndex].Lane);
        }

        /// <summary>
        /// Finishes editing the hold note and finalizes its data representation in the level.
        /// Returns whether the current data of the note is transformable to a hold note or not.
        /// </summary>
        public bool StopEditingNote() {
            if(noteData.Count <= 1) return false;

            UpdateDisablingPanels(false);
            return true;
        }

        /// <summary>
        /// Deletes the visual representation of the currently being edited hold note.
        /// Also deletes its data representation in the notes of the level if there is one.
        /// </summary>
        public void DeleteNote() {
            EditorNote note = CreateFinishedHoldNoteData();
            levelEditor.DeleteNote(note);
            // TODO: check if lanes have to change
            for(int i = 0; i < noteData.Count; i++) {
                note = noteData.Values[i];
                if(note.BeatIndex >= levelEditor.BeatMarkerManager.BottomMarkerBeatIndex
                    && note.BeatIndex <= levelEditor.BeatMarkerManager.TopMarkerBeatIndex) {
                    Lane lane = levelEditor.LaneManager.GetLaneAtBeatIndex(note.BeatIndex, note.Lane);
                    lane.DeleteNote(note.BeatIndex);
                }
            }

            UpdateDisablingPanels(false);
        }

        /// <summary>
        /// Returns whether the passed in beatIndex is applicable to become a new anchor for the
        /// currently being edited hold note.
        /// </summary>
        /// <param name="beatIndex">The passed in beatIndex.</param>
        public bool IsValidPlacement(int beatIndex) {
            foreach(int key in noteData.Keys) {
                if(key == beatIndex) return false;
            }

            return true;
        }

        private void DeleteNotes(List<EditorNote> notes) {
            for(int i = 0; i < notes.Count; i++) {
                levelEditor.DeleteNote(notes[i]);
                if(notes[i].BeatIndex >= levelEditor.BeatMarkerManager.BottomMarkerBeatIndex
                    && notes[i].BeatIndex <= levelEditor.BeatMarkerManager.TopMarkerBeatIndex) {
                    Lane lane = levelEditor.LaneManager.GetLaneAtBeatIndex(notes[i].BeatIndex, notes[i].Lane);
                    lane.DeleteNote(notes[i].BeatIndex);
                }
            }
        }

        private void CreateNewLaneSwitchAnchor(int beatIndex, Lane lane, out Lane newLane) {
            // ... remove the previous last anchor if it isnt the first
            List<LaneNote> notesToReassign = null;
            if(noteData.Count > 2) {
                int previousBeatIndex = noteData.Values[noteData.Count - 2].BeatIndex;
                if(previousBeatIndex >= levelEditor.BeatMarkerManager.BottomMarkerBeatIndex
                    && previousBeatIndex < levelEditor.BeatMarkerManager.TopMarkerBeatIndex) {
                    levelEditor.LaneManager.DeleteLaneChunk(previousBeatIndex, out notesToReassign, ErrorMessageDisplayType.Console);
                } else {
                    levelEditor.RemoveLaneChunk(previousBeatIndex, ErrorMessageDisplayType.Console);
                }
            }

            // ... then make this anchor a new lane switch
            if(beatIndex == levelEditor.BeatMarkerManager.TopMarkerBeatIndex)
                levelEditor.AddTemporaryLaneChunk(beatIndex, previousChunkLaneCount, laneSwitchNewLaneCount, true);
            else levelEditor.LaneManager.CreateNewTemporaryLaneChunk(previousChunkLaneCount, laneSwitchNewLaneCount, levelEditor, beatIndex);

            newLane = levelEditor.LaneManager.GetLaneSwitchAnchorLaneAtBeatIndex(beatIndex, lane.LaneIndex);
            if(notesToReassign != null) ReassignNotes(notesToReassign);
        }

        private void UpdateDisablingPanels(bool setActive) {
            foreach(GameObject item in disablingPanels) {
                item.SetActive(setActive);
            }

            float targetY = setActive ? editingDisclaimer.sizeDelta.y : 0f;
            editingDisclaimer.DOAnchorPosY(targetY, 0.5f);
        }

        private EditorNote CreateFinishedHoldNoteData() {
            EditorNote note = noteData.Values[0];
            note.Type = laneSwitchNewLaneCount <= 0 ? NoteType.Hold : NoteType.LaneSwitch;
            if(laneSwitchNewLaneCount > 0) note.NewLaneCountOnCompletion = laneSwitchNewLaneCount;
            note.ChildNotes = new EditorChildNote[noteData.Count - 1];

            for(int i = 1; i < noteData.Count; i++) {
                EditorNote child = noteData.Values[i];
                note.ChildNotes[i - 1] = new EditorChildNote(child);
            }

            return note;
        }

        private SortedList<int, EditorNote> LoadHoldNoteData(int beatIndex) {
            SortedList<int, EditorNote> data = new SortedList<int, EditorNote>();
            EditorNote? note = levelEditor.GetNote(beatIndex);
            if(note != null) {
                EditorNote initialNote = (EditorNote)note;
                laneSwitchNewLaneCount = initialNote.NewLaneCountOnCompletion;
                data.Add(beatIndex, initialNote);
                for(int i = 0; i < initialNote.ChildNotes.Length; i++) {
                    EditorChildNote child = initialNote.ChildNotes[i];
                    data.Add(child.BeatIndex, new EditorNote() { BeatIndex = child.BeatIndex, Lane = child.Lane });
                }
            }
            if(data.Count <= 1) Debug.LogError($"Tried to load a hold note at beatIndex {beatIndex} which doesnt exist or isnt complete.");

            return data;
        }

        private bool CheckIfThereAreNotesInbetween(int beatIndex, out List<EditorNote> foundNotes) {
            int firstAnchorBeatIndex = noteData.Values[0].BeatIndex;

            int startBeatIndex = firstAnchorBeatIndex > beatIndex ? beatIndex : firstAnchorBeatIndex;
            int endBeatIndex = firstAnchorBeatIndex > beatIndex ? firstAnchorBeatIndex : beatIndex;

            foundNotes = new List<EditorNote>();
            for(int i = 0; i <= endBeatIndex - startBeatIndex; i++) {
                EditorNote? note = levelEditor.GetNote(startBeatIndex + i);
                if(note != null && ((EditorNote)note).BeatIndex != firstAnchorBeatIndex) {
                    foundNotes.Add((EditorNote)note);
                }
            }

            return foundNotes.Count > 0;
        }

        private void OnInbetweenNotesWereFoundCallbackReceiver(bool choice) {
            if(choice) {
                for(int i = 0; i < notesToDelete.Count; i++) {
                    levelEditor.DeleteNote(notesToDelete[i].BeatIndex);
                    if(notesToDelete[i].BeatIndex >= levelEditor.BeatMarkerManager.BottomMarkerBeatIndex
                        && notesToDelete[i].BeatIndex <= levelEditor.BeatMarkerManager.TopMarkerBeatIndex) {
                        Lane lane = levelEditor.LaneManager.GetLaneAtBeatIndex(notesToDelete[i].BeatIndex, notesToDelete[i].Lane);
                        lane.DeleteNote(notesToDelete[i].BeatIndex);
                    }
                    if(notesToDelete[i].Type == NoteType.Hold || notesToDelete[i].Type == NoteType.LaneSwitch) {
                        foreach(EditorChildNote child in notesToDelete[i].ChildNotes) {
                            if(child.BeatIndex >= levelEditor.BeatMarkerManager.BottomMarkerBeatIndex
                                && child.BeatIndex <= levelEditor.BeatMarkerManager.TopMarkerBeatIndex) {
                                Lane lane = levelEditor.LaneManager.GetLaneAtBeatIndex(child.BeatIndex, child.Lane);
                                lane.DeleteNote(child.BeatIndex);
                            }
                        }
                    }
                }

                notesToDelete = null;
                savedAction?.Invoke();
                savedAction = null;
            }
        }

        private void ReassignNotes(List<LaneNote> notes) {
            foreach(LaneNote note in notes) {
                levelEditor.LaneManager.GetLaneSwitchAnchorLaneAtBeatIndex(note.BeatIndex, note.Lane).AssignNote(note);
            }
        }
    }
}
