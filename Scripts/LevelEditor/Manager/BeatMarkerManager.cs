using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Manages positions and scale of beat markers.
    /// 
    /// Author: Kevin Andersch
    /// Date: 05.04.22
    /// Version: 1.0
    /// </summary>
    public class BeatMarkerManager : MonoBehaviour {
        [Header("References")]
        [SerializeField] private GameObject beatMarkerPrefab;
        [SerializeField] private GameObject miniBeatMarkerPrefab;
        [SerializeField] private RectTransform laneHolder;
        [SerializeField] private Toggle miniMarkerUIToggle;
        [SerializeField] private RectTransform anchoredPositionHelper;
        [SerializeField] private PlaybackManager playbackManager;

        [Header("Beat Marker Setup")]
        [SerializeField] private float offBeatMarkerHeight;
        [SerializeField] private float mainBeatMarkerHeight;
        [SerializeField] private float bottomMargin;
        [SerializeField] private float topMargin;
        [SerializeField] private Color normalMarkerColor;
        [SerializeField] private Color miniMarkerColor;
        [SerializeField] private Color highlightedMarkerColor;
        [SerializeField] private float outOfScreenMarkerYAnchor = -500f;

        public float BeatMarkerDistance { get; private set; }
        public float MiniBeatMarkerDistance { get; private set; }
        public int BottomMarkerBeatIndex { get => bottomMarkerBeatIndex; }
        public int TopMarkerBeatIndex { get => bottomMarkerBeatIndex + ((beatMarkers.Length - 1) * (BeatIndicesInbetweenBeatMarker + 1)); }
        public float BottomMargin { get => bottomMargin; }
        public float TopMargin { get => topMargin; }
        public int BeatIndicesInbetweenBeatMarker { get; private set; }
        public bool ShowMiniBeatMarkers { get; private set; }
        public BeatMarkerTargetingMode TargetingMode { get; set; }
        public BeatMarkerInfoType BeatMarkerInfoType { get; private set; }

        private BeatMarkerData[] beatMarkers;
        private BeatMarkerData[][] miniBeatMarkers;
        private TextMeshProUGUI[] beatMarkerTexts;
        private int bottomMarkerBeatIndex;
        private bool highlightMainBeat;
        private int mainBeatTime;
        private NotePlacementLocation markerToBeHighlightedOnEditorOpen;
        private LevelEditor levelEditor;
        private float[] beatMarkerAnchoredPositions;
        private float[] beatMarkerScreenPositions;
        private float currentBeatMarkerPositionsHeight;

        // playback
        private LinkedList<BeatMarkerData> playbackMarkers;
        private bool miniBeatMarkersWereEnabled;

        // Events
        /// <summary>
        /// 0: the highlighted marker
        /// </summary>
        public static System.Action<MarkerIndex> OnMarkerWasHighlighted;
        /// <summary>
        /// 0: number of markers
        /// </summary>
        public static System.Action<int> OnMarkersWereSpawned;
        /// <summary>
        /// 0: bottom beat marker index
        /// </summary>
        public static System.Action<int> OnBottomBeatMarkerIndexWasChanged;
        /// <summary>
        /// 0: the beatIndex of the marker under the cursor
        /// </summary>
        public static System.Action<int> OnMarkerUnderCursorChanged;
        /// <summary>
        /// Triggers after OnMarkerUnderCursorChanged.
        /// 0: the beatIndex of the marker under the cursor
        /// </summary>
        public static System.Action<int> OnLateMarkerUnderCursorChanged;

        private void Update() {
            if(currentBeatMarkerPositionsHeight > 0 && currentBeatMarkerPositionsHeight != Screen.height) {
                UpdateBeatMarkerPositions();
            }
        }

        private void OnEnable() {
            LevelEditor.OnEditorWasOpened += HighlightCorrectMarkerOnEditorOpened;
            LevelEditor.OnBeforeEditorWasClosed += DestroyAllMarkers;
            PlaybackManager.OnPlaybackBegan += OnBeginPlayback;
            PlaybackManager.OnPlaybackEnded += OnEndPlayback;
            LevelEditor.OnBeforeSettingsWereSaved += SaveSettings;
        }

        private void OnDisable() {
            LevelEditor.OnEditorWasOpened -= HighlightCorrectMarkerOnEditorOpened;
            LevelEditor.OnBeforeEditorWasClosed -= DestroyAllMarkers;
            PlaybackManager.OnPlaybackBegan -= OnBeginPlayback;
            PlaybackManager.OnPlaybackEnded -= OnEndPlayback;
            LevelEditor.OnBeforeSettingsWereSaved -= SaveSettings;
        }

        /// <summary>
        /// Saves which marker to highlight when the editor is opened.
        /// </summary>
        /// <param name="highlightedMarker">The marker to highlight. true: top / false: bottom</param>
        public void LoadValues(NotePlacementLocation highlightedMarker) {
            markerToBeHighlightedOnEditorOpen = highlightedMarker;
        }

        /// <summary>
        /// Highlights the correct marker when the editor is opened.
        /// </summary>
        public void HighlightCorrectMarkerOnEditorOpened() {
            if(markerToBeHighlightedOnEditorOpen != NotePlacementLocation.UnderCursor) HighlightOuterMarker(markerToBeHighlightedOnEditorOpen, new MarkerIndex { NormalMarkerIndex = 1 });
        }

        /// <summary>
        /// Spawns the desired amount of beat markers at their corresponding locations.
        /// Highlights main beat markers. If there already were markers in the scene, destroys them.
        /// </summary>
        /// <param name="amount">The amount of beat markers to spawn.</param>
        /// <param name="beatIndicesIndebtweenMarkers">The amount of mini beats inbetween each major beat. Clamped to a minimum of 1.</param>
        /// <param name="bottomBeatIndex">The beat index that the bottom most beat marker starts at.</param>
        /// <param name="highlightMainBeat">Whether to highlight main beats (making the marker bigger).</param>
        /// <param name="mainBeatTime">Beats within quarter time (e.g. four quarters time = 4).</param>
        public void SpawnBeatMarkers(int amount, int beatIndicesIndebtweenMarkers, LevelEditor levelEditor, int bottomBeatIndex = 0, bool highlightMainBeat = true, int mainBeatTime = 4) {
            this.levelEditor = levelEditor;
            BeatIndicesInbetweenBeatMarker = beatIndicesIndebtweenMarkers < 1 ? 1 : beatIndicesIndebtweenMarkers;
            bottomMarkerBeatIndex = bottomBeatIndex;
            this.highlightMainBeat = highlightMainBeat;
            this.mainBeatTime = mainBeatTime;
            if(beatMarkers != null) DestroyAllMarkers();
            beatMarkers = new BeatMarkerData[amount];
            beatMarkerTexts = new TextMeshProUGUI[amount];
            miniBeatMarkers = new BeatMarkerData[amount][];
            for(int i = 0; i < miniBeatMarkers.Length; i++) {
                miniBeatMarkers[i] = new BeatMarkerData[BeatIndicesInbetweenBeatMarker];
            }
            float totalHeight = laneHolder.sizeDelta.y - bottomMargin - topMargin;
            BeatMarkerDistance = totalHeight / (amount - 1);
            MiniBeatMarkerDistance = (BeatMarkerDistance / (BeatIndicesInbetweenBeatMarker + 1));

            currentBeatMarkerPositionsHeight = Screen.height;
            beatMarkerAnchoredPositions = new float[(amount * (BeatIndicesInbetweenBeatMarker + 1)) - BeatIndicesInbetweenBeatMarker];
            beatMarkerScreenPositions = new float[(amount * (BeatIndicesInbetweenBeatMarker + 1)) - BeatIndicesInbetweenBeatMarker];
            for(int i = 0; i < amount; i++) {
                RectTransform rect = Instantiate(beatMarkerPrefab, transform).GetComponent<RectTransform>();
                Image renderer = rect.GetComponent<Image>();

                // normal beat marker
                BeatMarkerData data = new BeatMarkerData { Rect = rect, Model = renderer };
                rect.anchoredPosition = new Vector2(0f, bottomMargin + BeatMarkerDistance * i);
                bool isMainBeat = IsMainBeat(bottomBeatIndex + i * (BeatIndicesInbetweenBeatMarker + 1));
                SetProperBeatMarkerSize(ref data, isMainBeat);
                // TODO: scale to width of current lane chunk (AFTER THEY ARE INITIALIZED)
                renderer.color = normalMarkerColor;
                beatMarkers[i] = data;
                beatMarkerTexts[i] = rect.GetComponentInChildren<TextMeshProUGUI>();
                if(BeatMarkerInfoType == BeatMarkerInfoType.None) beatMarkerTexts[i].gameObject.SetActive(false);
                else if(BeatMarkerInfoType == BeatMarkerInfoType.BeatIndex)
                    beatMarkerTexts[i].text = (bottomBeatIndex + i * (BeatIndicesInbetweenBeatMarker + 1)).ToString();
                else if(BeatMarkerInfoType == BeatMarkerInfoType.Time)
                    beatMarkerTexts[i].text = LevelDataUtilities.ConvertBeatIndexToTime((bottomBeatIndex + i * (BeatIndicesInbetweenBeatMarker + 1)), levelEditor.Level).ToString("F2");

                beatMarkerAnchoredPositions[i * (beatIndicesIndebtweenMarkers + 1)] = rect.anchoredPosition.y;
                beatMarkerScreenPositions[i * (beatIndicesIndebtweenMarkers + 1)] = rect.position.y;
                // its mini beat markers
                for(int j = 0; j < BeatIndicesInbetweenBeatMarker; j++) {
                    RectTransform miniRect = Instantiate(miniBeatMarkerPrefab, transform).GetComponent<RectTransform>();
                    Image miniRenderer = miniRect.GetComponent<Image>();
                    if(!ShowMiniBeatMarkers) miniRect.gameObject.SetActive(false);

                    BeatMarkerData miniData = new BeatMarkerData { Rect = miniRect, Model = miniRenderer };
                    float yPos = i < amount - 1 ? rect.anchoredPosition.y + MiniBeatMarkerDistance * (j + 1) : outOfScreenMarkerYAnchor;
                    miniRect.anchoredPosition = new Vector2(0f, yPos);
                    // TODO: scale to width, or do so only in the toggle
                    miniRenderer.color = miniMarkerColor;
                    miniBeatMarkers[i][j] = miniData;
                    if(i < amount - 1) beatMarkerAnchoredPositions[(i * (beatIndicesIndebtweenMarkers + 1)) + j + 1] = miniRect.anchoredPosition.y;
                    if(i < amount - 1) beatMarkerScreenPositions[(i * (beatIndicesIndebtweenMarkers + 1)) + j + 1] = miniRect.position.y;
                }
            }

            OnMarkersWereSpawned?.Invoke(amount);
        }

        /// <summary>
        /// Toggles all miniBeatMarkers on / off.
        /// </summary>
        public void ToggleMiniBeatMarkers(bool calledFromUIElement = false) {
            if(!calledFromUIElement) {
                miniMarkerUIToggle.isOn = ShowMiniBeatMarkers;
                return;
            }

            ShowMiniBeatMarkers = !ShowMiniBeatMarkers;
            DisplayMiniBeatMarkers(ShowMiniBeatMarkers);
        }

        /// <summary>
        /// Either shows or hides the miniBeatMarkers, depending on the passed in value.
        /// </summary>
        /// <param name="show">Whether to show (true) or hide (false) the markers.</param>
        public void DisplayMiniBeatMarkers(bool show) {
            ShowMiniBeatMarkers = show;
            for(int i = 0; i < miniBeatMarkers.Length; i++) {
                for(int j = 0; j < miniBeatMarkers[i].Length; j++) {
                    miniBeatMarkers[i][j].Rect.gameObject.SetActive(show);
                }
            }
        }

        /// <summary>
        /// Either shows or hides the majorBeatMarkers, depending on the passed in value.
        /// </summary>
        /// <param name="show">Whether to show (true) or hide (false) the markers.</param>
        public void DisplayBeatMarkers(bool show) {
            for(int i = 0; i < beatMarkers.Length; i++) {
                beatMarkers[i].Rect.gameObject.SetActive(show);
            }
        }

        /// <summary>
        /// Returns whether the passed in beatIndex is a main beat or not.
        /// </summary>
        /// <param name="beatIndex">The beatIndex to check.</param>
        public bool IsMainBeat(int beatIndex) {
            return beatIndex % (mainBeatTime * (BeatIndicesInbetweenBeatMarker + 1)) == 0;
        }

        /// <summary>
        /// Returns whether the passed in beatIndex is a major beat or not.
        /// </summary>
        /// <param name="beatIndex">The beatIndex to check.</param>
        public bool IsMajorBeat(int beatIndex) {
            return beatIndex % (BeatIndicesInbetweenBeatMarker + 1) == 0;
        }

        /// <summary>
        /// Checks which beat marker is currently under (closest to) the mouse cursor and highlits it.
        /// </summary>
        /// <param name="mousePosition">The current mouse position.</param>
        /// <param name="previouslyHighlightedMarkerIndex">The previously highlighted marker.</param>
        public MarkerIndex HighlightMarkerUnderCursor(Vector2 mousePosition, MarkerIndex previouslyHighlightedMarkerIndex) {
            // maybe check mouse x if highlight should occur

            // set last marker to normal marker at index 0 if we no longer show mini markers
            if(!ShowMiniBeatMarkers && previouslyHighlightedMarkerIndex.MiniMarkerIndex != null) previouslyHighlightedMarkerIndex.NormalMarkerIndex = 0;

            float mouseHeight = mousePosition.y;
            int bottomOffset = BottomMarkerBeatIndex % (BeatIndicesInbetweenBeatMarker + 1);

            // if mouse is under bottom marker -> highlight bottom marker and return
            if(mouseHeight <= beatMarkerScreenPositions[0]) {
                MarkerIndex bottomIndex = new MarkerIndex() {
                    NormalMarkerIndex = 0,
                    MiniMarkerIndex = bottomOffset == 0 ? null : bottomOffset - 1
                };
                if(!AreMarkerIndicesEqual(bottomIndex, previouslyHighlightedMarkerIndex)) {
                    ChangeMarkerColor(previouslyHighlightedMarkerIndex, false);
                    ChangeMarkerColor(bottomIndex, true);
                }
                return bottomIndex;
            }
            // if mouse is over top marker -> highlight top marker and return
            else if(mouseHeight >= beatMarkerScreenPositions[beatMarkerScreenPositions.Length - 1]) {
                MarkerIndex topMarkerIndex = new MarkerIndex() {
                    NormalMarkerIndex = beatMarkers.Length - 1,
                    MiniMarkerIndex = bottomOffset == 0 ? null : bottomOffset - 1
                };
                if(!AreMarkerIndicesEqual(topMarkerIndex, previouslyHighlightedMarkerIndex)) {
                    ChangeMarkerColor(previouslyHighlightedMarkerIndex, false);
                    ChangeMarkerColor(topMarkerIndex, true);
                }
                return topMarkerIndex;
            }

            int screenIndex = 0;
            int stepDistance = ShowMiniBeatMarkers ? 1 : (BeatIndicesInbetweenBeatMarker + 1);
            for(int i = 0; i < beatMarkerScreenPositions.Length; i += stepDistance) {
                if(beatMarkerScreenPositions[i] > mousePosition.y) {
                    screenIndex = i;
                    break;
                }
            }

            float dstToAbove = beatMarkerScreenPositions[screenIndex] - mousePosition.y;
            float dstToBelow = mousePosition.y - beatMarkerScreenPositions[screenIndex - stepDistance];
            if(dstToBelow < dstToAbove) screenIndex = screenIndex - stepDistance;

            MarkerIndex index = ConvertScreenIndexToMarkerIndex(screenIndex);

            if(!AreMarkerIndicesEqual(previouslyHighlightedMarkerIndex, index)) {
                ChangeMarkerColor(previouslyHighlightedMarkerIndex, false);
            }

            ChangeMarkerColor(index, true);
            OnMarkerWasHighlighted?.Invoke(index);
            if(!AreMarkerIndicesEqual(previouslyHighlightedMarkerIndex, index)) OnLateMarkerUnderCursorChanged?.Invoke(GetBeatMarkerBeatIndexFromMarkerIndex(index));

            return index;
        }

        /// <summary>
        /// Highlights either the top or bottom marker.
        /// </summary>
        /// <param name="location">The marker to highlight.</param>
        /// <param name="previouslyHighlightedMarkerIndex">The previously highlighted marker.</param>
        public MarkerIndex HighlightOuterMarker(NotePlacementLocation location, MarkerIndex previouslyHighlightedMarkerIndex) {
            if(previouslyHighlightedMarkerIndex.MiniMarkerIndex == null) {
                beatMarkers[previouslyHighlightedMarkerIndex.NormalMarkerIndex].Model.color = normalMarkerColor;
            } else {
                miniBeatMarkers[previouslyHighlightedMarkerIndex.NormalMarkerIndex][(int)previouslyHighlightedMarkerIndex.MiniMarkerIndex].Model.color = miniMarkerColor;
            }

            int bottomOffset = bottomMarkerBeatIndex % (BeatIndicesInbetweenBeatMarker + 1);

            if(location == NotePlacementLocation.Top) {
                MarkerIndex topMarkerIndex = new MarkerIndex() {
                    NormalMarkerIndex = beatMarkers.Length - 1,
                    MiniMarkerIndex = bottomOffset == 0 ? null : bottomOffset - 1
                };
                beatMarkers[beatMarkers.Length - 1].Model.color = highlightedMarkerColor;
                OnMarkerWasHighlighted?.Invoke(topMarkerIndex);
                return topMarkerIndex;
            } else {
                MarkerIndex bottomMarkerIndex = new MarkerIndex() {
                    NormalMarkerIndex = 0,
                    MiniMarkerIndex = bottomOffset == 0 ? null : bottomOffset - 1
                };
                beatMarkers[0].Model.color = highlightedMarkerColor;
                OnMarkerWasHighlighted?.Invoke(bottomMarkerIndex);
                return bottomMarkerIndex;
            }
        }

        /// <summary>
        /// Recolours the outer markers to the normal color.
        /// </summary>
        public void CancelOuterMarkerHighlight() {
            beatMarkers[0].Model.color = normalMarkerColor;
            beatMarkers[beatMarkers.Length - 1].Model.color = normalMarkerColor;
        }

        /// <summary>
        /// Determines both the height of the marker at the passed in index and its beatIndex.
        /// </summary>
        /// <param name="markerIndex">Index of the beat marker for which to determine the values.</param>
        /// <param name="markerBeatIndex">The beat index of the marker.</param>
        public float GetMarkerValues(MarkerIndex markerIndex, out int markerBeatIndex) {
            int beatOffset = bottomMarkerBeatIndex % (BeatIndicesInbetweenBeatMarker + 1);
            if(markerIndex.MiniMarkerIndex == null) {
                markerBeatIndex = bottomMarkerBeatIndex - beatOffset + (markerIndex.NormalMarkerIndex * (BeatIndicesInbetweenBeatMarker + 1));
            } else {
                markerBeatIndex = bottomMarkerBeatIndex - beatOffset + (markerIndex.NormalMarkerIndex * (BeatIndicesInbetweenBeatMarker + 1))
                    + ((int)markerIndex.MiniMarkerIndex + 1);
            }
            return markerIndex.MiniMarkerIndex == null
                ? beatMarkers[markerIndex.NormalMarkerIndex].Rect.position.y
                : miniBeatMarkers[markerIndex.NormalMarkerIndex][(int)markerIndex.MiniMarkerIndex].Rect.position.y;
        }

        /// <summary>
        /// Adds the passed in amount to the bottom marker beatIndex and updates main beat markers.
        /// This method does NOT check whether the resulting beatIndex is possible (e.g. < 0).
        /// WARNING: The result is not multiplied by indices per marker. This calculation should be passed in.
        /// </summary>
        /// <param name="amount">The amount to change the beatIndex by (can be negative).</param>
        public void ChangeBottomMarkerBeatIndex(int amount) {
            bottomMarkerBeatIndex += amount;
            if(Mathf.Abs(amount) <= 1) {
                ShuffleBeatMarkersToCorrectPositions();
            } else if(highlightMainBeat) {
                for(int i = 0; i < beatMarkers.Length; i++) {
                    bool isMainBeat = IsMainBeat(bottomMarkerBeatIndex + i * (BeatIndicesInbetweenBeatMarker + 1));
                    SetProperBeatMarkerSize(ref beatMarkers[i], isMainBeat);
                    UpdateMarkerInfoText(i);
                }
            }

            OnBottomBeatMarkerIndexWasChanged?.Invoke(bottomMarkerBeatIndex);
        }

        /// <summary>
        /// Sets the bottom marker beatIndex to the passed in value and updates main beat markers.
        /// This method does NOT check whether the resulting beatIndex is possible (e.g. < 0).
        /// WARNING: The result is not multiplied by indices per marker. This calculation should be passed in.
        /// </summary>
        /// <param name="value">The amount to set the beatIndex to.</param>
        public void SetBottomMarkerBeatIndex(int value) {
            bottomMarkerBeatIndex = value;
            if(highlightMainBeat) {
                for(int i = 0; i < beatMarkers.Length; i++) {
                    SetProperBeatMarkerSize(ref beatMarkers[i], (bottomMarkerBeatIndex + i) % mainBeatTime == 0);
                    UpdateMarkerInfoText(i);
                }
            }

            OnBottomBeatMarkerIndexWasChanged?.Invoke(bottomMarkerBeatIndex);
        }

        /// <summary>
        /// Sets the top marker beatIndex to the passed in value and updates main beat markers.
        /// This method does NOT check whether the resulting beatIndex is possible (e.g. < 0).
        /// WARNING: The result is not multiplied by indices per marker. This calculation should be passed in.
        /// </summary>
        /// <param name="value">The amount to set the beatIndex to.</param>
        public void SetTopMarkerBeatIndex(int value) {
            SetBottomMarkerBeatIndex(value - (beatMarkers.Length - 1) * (BeatIndicesInbetweenBeatMarker + 1));
        }

        /// <summary>
        /// Similar to SetBottomMarkerBeatIndex but does not invoke events nor does it rescale the
        /// beat markers.
        /// Unsafe, has no out of bounds checks and does not check if the passed in value is usable.
        /// </summary>
        /// <param name="value">The new index for the bottom marker.</param>
        public void SetBottomMarkerBeatIndexWithoutUpdate(int value) => bottomMarkerBeatIndex = value;

        /// <summary>
        /// Returns the y-position of the beat marker at the given beat index.
        /// Safe to use, has out of bounds checks.
        /// </summary>
        /// <param name="beatIndex">The index of the beat marker to get the height for.</param>
        /// <param name="includeMiniBeatMarkers">If false, rounds down to the nearest actual beatMarker and returns its y-position.</param>
        public float GetBeatMarkerHeightFromBeatIndex(int beatIndex, bool includeMiniBeatMarkers = false) {
            int actualBeatIndex = beatIndex;
            if(beatIndex < bottomMarkerBeatIndex) actualBeatIndex = bottomMarkerBeatIndex;
            if(beatIndex > TopMarkerBeatIndex) actualBeatIndex = TopMarkerBeatIndex;

            int index = actualBeatIndex - bottomMarkerBeatIndex;
            if(IsMainBeat(beatIndex)) return GetScreenPositionForBeatIndex(beatIndex, true);
            else {
                int offset = includeMiniBeatMarkers ? 0 : beatIndex % (BeatIndicesInbetweenBeatMarker + 1);
                return GetScreenPositionForBeatIndex(beatIndex - offset, true);
            }
        }

        /// <summary>
        /// Returns the anchored y-position of the beat marker at the given beat index.
        /// Has one more imaginary marker at the top to allow height bigger than the final marker.
        /// Safe to use, has out of bounds checks. Is only applicable if the requesting object has the same anchor.
        /// </summary>
        /// <param name="beatIndex">The index of the beat marker to get the height for.</param>
        /// <param name="includeMiniBeatMarkers">If false, rounds down to the nearest actual beatMarker and returns its y-position.</param>
        public float GetBeatMarkerAnchoredHeightFromBeatIndex(int beatIndex, bool includeMiniBeatMarkers = false) {
            if(beatIndex < bottomMarkerBeatIndex) beatIndex = bottomMarkerBeatIndex;
            if(beatIndex > TopMarkerBeatIndex) beatIndex = TopMarkerBeatIndex;

            if(IsMainBeat(beatIndex)) return GetAnchoredPositionForBeatIndex(beatIndex, true);
            else {
                int offset = includeMiniBeatMarkers ? 0 : beatIndex % (BeatIndicesInbetweenBeatMarker + 1);
                return GetAnchoredPositionForBeatIndex(beatIndex - offset, true);
            }
        }

        /// <summary>
        /// Returns the anchored y-position of the (main) beat marker at the given index.
        /// Has one more imaginary marker at the top to allow height bigger than the final marker.
        /// Safe to use, has out of bounds checks. Is only applicable if the requesting object has the same anchor.
        /// </summary>
        /// <param name="index">The index of the beat marker.</param>
        /// <param name="includeImaginaryMarker">Whether the imaginary marker at the top should be included.</param>
        public float GetBeatMarkerAnchoredHeightFromIndex(int index, bool includeImaginaryMarker = true) {
            index = Mathf.Clamp(index, 0, beatMarkers.Length - (BeatIndicesInbetweenBeatMarker + 1));
            float imaginaryOffset = index > beatMarkers.Length - (BeatIndicesInbetweenBeatMarker + 1) && includeImaginaryMarker ? BeatMarkerDistance : 0f;
            return beatMarkers[index].Rect.anchoredPosition.y + imaginaryOffset;
        }

        /// <summary>
        /// Returns the y-position of the beat marker at the given index (of it in the marker array).
        /// Safe to use, has out of bounds checks.
        /// </summary>
        /// <param name="index">The index of the beat marker to get the height for.</param>
        public float GetBeatMarkerHeightFromMarkerIndex(MarkerIndex index) {
            if(index.NormalMarkerIndex < 0) return beatMarkerScreenPositions[0];
            if(index.MiniMarkerIndex != null && index.MiniMarkerIndex < 0) index.MiniMarkerIndex = 0;
            if(index.NormalMarkerIndex >= beatMarkers.Length) return beatMarkerScreenPositions[beatMarkerScreenPositions.Length - 1];
            if(index.MiniMarkerIndex >= BeatIndicesInbetweenBeatMarker) index.MiniMarkerIndex = BeatIndicesInbetweenBeatMarker - 1;
            return index.MiniMarkerIndex == null ? beatMarkers[index.NormalMarkerIndex].Rect.position.y : miniBeatMarkers[index.NormalMarkerIndex][(int)index.MiniMarkerIndex].Rect.position.y;
        }

        /// <summary>
        /// Returns the beatIndex value of the passed in markerIndex.
        /// Safe to use, has out of bounds checks.
        /// </summary>
        /// <param name="index">The markerIndex from which to get the beatIndex.</param>
        public int GetBeatMarkerBeatIndexFromMarkerIndex(MarkerIndex index) {
            int beatOffset = bottomMarkerBeatIndex % (BeatIndicesInbetweenBeatMarker + 1);
            if(index.NormalMarkerIndex < 0) index.NormalMarkerIndex = 0;
            if(index.MiniMarkerIndex != null && index.MiniMarkerIndex < 0) index.MiniMarkerIndex = 0;
            if(index.NormalMarkerIndex >= beatMarkers.Length) index.NormalMarkerIndex = beatMarkers.Length - 1;
            if(index.MiniMarkerIndex >= BeatIndicesInbetweenBeatMarker) index.MiniMarkerIndex = BeatIndicesInbetweenBeatMarker - 1;

            int result = index.MiniMarkerIndex == null
                ? (index.NormalMarkerIndex * (BeatIndicesInbetweenBeatMarker + 1))
                : (index.NormalMarkerIndex * (BeatIndicesInbetweenBeatMarker + 1)) + (int)index.MiniMarkerIndex + 1;
            return result + bottomMarkerBeatIndex - beatOffset;
        }

        /// <summary>
        /// Gets both the beatIndex of the top marker and all beatIndices of the miniBeatMarkers of the previous beatMarker.
        /// </summary>
        public int[] GetTopBeatIndices() {
            int[] result = new int[BeatIndicesInbetweenBeatMarker + 1];
            result[0] = TopMarkerBeatIndex;
            for(int i = 1; i <= BeatIndicesInbetweenBeatMarker; i++) {
                result[i] = TopMarkerBeatIndex - i;
            }
            return result;
        }

        /// <summary>
        /// Gets both the beatIndex of the bottom marker and all beatIndices of its miniBeatMarkers.
        /// </summary>
        /// <param name="includeBottomBeatmarker">Whether to include the lowest beatMarker and its children, or its children and the next beatMarker.</param>
        public int[] GetBottomBeatIndices(bool includeBottomBeatmarker) {
            int[] result = new int[BeatIndicesInbetweenBeatMarker + 1];
            int offset = includeBottomBeatmarker ? 0 : 1;
            result[0] = BottomMarkerBeatIndex + offset;
            for(int i = 1; i <= BeatIndicesInbetweenBeatMarker; i++) {
                result[i] = BottomMarkerBeatIndex + i + offset;
            }
            return result;
        }

        private MarkerIndex ConvertScreenIndexToMarkerIndex(int screenIndex) {
            int bottomOffset = bottomMarkerBeatIndex % (BeatIndicesInbetweenBeatMarker + 1);
            int adjustedIndex = bottomOffset == 0 ? screenIndex : (screenIndex + bottomOffset);

            int? miniIndex = adjustedIndex % (BeatIndicesInbetweenBeatMarker + 1) == 0
                    ? null : adjustedIndex % (BeatIndicesInbetweenBeatMarker + 1) - 1;
            return new MarkerIndex() {
                NormalMarkerIndex = (int)(adjustedIndex / (BeatIndicesInbetweenBeatMarker + 1)),
                MiniMarkerIndex = miniIndex
            };
        }

        private void ChangeMarkerColor(MarkerIndex index, bool applyHighlightedColor) {
            if(index.MiniMarkerIndex == null) {
                Color c = applyHighlightedColor ? highlightedMarkerColor : normalMarkerColor;
                beatMarkers[index.NormalMarkerIndex].Model.color = c;
            } else {
                Color c = applyHighlightedColor ? highlightedMarkerColor : miniMarkerColor;
                miniBeatMarkers[index.NormalMarkerIndex][(int)index.MiniMarkerIndex].Model.color = c;
            }
        }

        private bool AreMarkerIndicesEqual(MarkerIndex first, MarkerIndex second) {
            return first.NormalMarkerIndex == second.NormalMarkerIndex && first.MiniMarkerIndex == second.MiniMarkerIndex;
        }

        private void DestroyAllMarkers() {
            for(int i = 0; i < beatMarkers.Length; i++) {
                Destroy(beatMarkers[i].Rect.gameObject);
            }
            for(int i = 0; i < miniBeatMarkers.Length; i++) {
                for(int j = 0; j < miniBeatMarkers[i].Length; j++) {
                    Destroy(miniBeatMarkers[i][j].Rect.gameObject);
                }
            }

            beatMarkers = null;
        }

        private void SetProperBeatMarkerSize(ref BeatMarkerData beatMarker, bool isMainBeat = false) {
            float height = isMainBeat ? mainBeatMarkerHeight : offBeatMarkerHeight;
            beatMarker.Rect.sizeDelta = new Vector2(beatMarker.Rect.sizeDelta.x, height);
        }

        private void SetProperBeatMarkerSize(BeatMarkerData beatMarker, int beatIndex) {
            float height = IsMainBeat(beatIndex) ? mainBeatMarkerHeight : offBeatMarkerHeight;
            beatMarker.Rect.sizeDelta = new Vector2(beatMarker.Rect.sizeDelta.x, height);
            beatMarker.Model.color = IsMajorBeat(beatIndex) ? normalMarkerColor : miniMarkerColor;
        }

        private void UpdateMarkerInfoText(int index) {
            if(BeatMarkerInfoType == BeatMarkerInfoType.None) beatMarkerTexts[index].gameObject.SetActive(false);
            else if(BeatMarkerInfoType == BeatMarkerInfoType.BeatIndex) {
                beatMarkerTexts[index].text = (bottomMarkerBeatIndex + index * (BeatIndicesInbetweenBeatMarker + 1)).ToString();
                beatMarkerTexts[index].gameObject.SetActive(true);
            } else if(BeatMarkerInfoType == BeatMarkerInfoType.Time) {
                beatMarkerTexts[index].text = LevelDataUtilities.ConvertBeatIndexToTime((bottomMarkerBeatIndex + index * (BeatIndicesInbetweenBeatMarker + 1)), levelEditor.Level).ToString("F2");
                beatMarkerTexts[index].gameObject.SetActive(true);
            }
        }

        private void UpdateMarkerInfoText(int index, int beatIndex) {
            if(BeatMarkerInfoType == BeatMarkerInfoType.None) beatMarkerTexts[index].gameObject.SetActive(false);
            else if(BeatMarkerInfoType == BeatMarkerInfoType.BeatIndex) {
                beatMarkerTexts[index].text = beatIndex.ToString();
                beatMarkerTexts[index].gameObject.SetActive(true);
            } else if(BeatMarkerInfoType == BeatMarkerInfoType.Time) {
                beatMarkerTexts[index].text = LevelDataUtilities.ConvertBeatIndexToTime(beatIndex, levelEditor.Level).ToString("F2");
                beatMarkerTexts[index].gameObject.SetActive(true);
            }
        }

        private void ShuffleBeatMarkersToCorrectPositions() {
            RectTransform r;
            int beatIndex;

            int bottomMiniBeat = bottomMarkerBeatIndex % (BeatIndicesInbetweenBeatMarker + 1);
            int firstMajorBeatBeatIndex = bottomMarkerBeatIndex;
            int startingNormalBeatMarker = 0;
            int normalIndexOffset = bottomMiniBeat != 0 ? 1 : 0;

            // set everything for the first major beat that is offscreen (index 0)
            if(!IsMajorBeat(bottomMarkerBeatIndex)) {
                startingNormalBeatMarker = 1;
                firstMajorBeatBeatIndex = BottomMarkerBeatIndex + BeatIndicesInbetweenBeatMarker + 1 - bottomMiniBeat;
                r = beatMarkers[0].Rect;
                r.anchoredPosition = new Vector2(r.anchoredPosition.x, outOfScreenMarkerYAnchor);
                for(int j = 0; j < BeatIndicesInbetweenBeatMarker; j++) {
                    beatIndex = firstMajorBeatBeatIndex - (BeatIndicesInbetweenBeatMarker - j);
                    r = miniBeatMarkers[0][j].Rect;
                    r.anchoredPosition = new Vector2(r.anchoredPosition.x, GetAnchoredPositionForBeatIndex(beatIndex, false));
                }
            }

            // set all but the last one
            for(int i = startingNormalBeatMarker; i < beatMarkers.Length; i++) {
                beatIndex = firstMajorBeatBeatIndex + (i - normalIndexOffset) * (BeatIndicesInbetweenBeatMarker + 1);
                r = beatMarkers[i].Rect;
                r.anchoredPosition = new Vector2(r.anchoredPosition.x, GetAnchoredPositionForBeatIndex(beatIndex, false));
                SetProperBeatMarkerSize(ref beatMarkers[i], IsMainBeat(beatIndex));
                UpdateMarkerInfoText(i, beatIndex);

                for(int j = 0; j < BeatIndicesInbetweenBeatMarker; j++) {
                    r = miniBeatMarkers[i][j].Rect;
                    beatIndex++;
                    r.anchoredPosition = new Vector2(r.anchoredPosition.x, GetAnchoredPositionForBeatIndex(beatIndex, false));
                }
            }
        }

        /// <summary>
        /// Returns the correct anchored y position for the passed in beatIndex.
        /// If clampValues, the return will be clamped between the starting (lowest) anchored y and the highest one.
        /// Otherwise, if the beatIndex is e.g. smaller than the bottomBeatIndex, the resulting possition is outside of the screen.
        /// </summary>
        private float GetAnchoredPositionForBeatIndex(int beatIndex, bool clampValues) {
            if(beatIndex < BottomMarkerBeatIndex) {
                if(!clampValues) return outOfScreenMarkerYAnchor;
                else return beatMarkerAnchoredPositions[0];
            }
            if(beatIndex > TopMarkerBeatIndex) {
                if(!clampValues) return outOfScreenMarkerYAnchor;
                else return beatMarkerAnchoredPositions[beatMarkerAnchoredPositions.Length - 1];
            }
            int i = beatIndex - bottomMarkerBeatIndex;
            return beatMarkerAnchoredPositions[i];
        }

        private float GetScreenPositionForBeatIndex(int beatIndex, bool clampValues) {
            if(beatIndex < BottomMarkerBeatIndex) {
                if(!clampValues) return outOfScreenMarkerYAnchor;
                else return beatMarkerScreenPositions[0];
            }
            if(beatIndex > TopMarkerBeatIndex) {
                if(!clampValues) return outOfScreenMarkerYAnchor;
                else return beatMarkerScreenPositions[beatMarkerScreenPositions.Length - 1];
            }
            int i = beatIndex - bottomMarkerBeatIndex;
            return beatMarkerScreenPositions[i];
        }

        private void SaveSettings() {
            levelEditor.Settings.BeatMarkerInfoType = BeatMarkerInfoType;
        }

        private void UpdateBeatMarkerPositions() {
            currentBeatMarkerPositionsHeight = Screen.height;
            for(int i = 0; i < beatMarkerAnchoredPositions.Length; i++) {
                anchoredPositionHelper.anchoredPosition
                    = new Vector2(anchoredPositionHelper.anchoredPosition.x, beatMarkerAnchoredPositions[i]);
                beatMarkerScreenPositions[i] = anchoredPositionHelper.position.y;
            }
            ShuffleBeatMarkersToCorrectPositions();
            if(!playbackManager.PlaybackIsRunning) return;
            playbackManager.UpdatePlaybackHeights(beatMarkerScreenPositions[beatMarkerScreenPositions.Length - 1], beatMarkerScreenPositions[0]);
            BeatMarkerData[] pbm = playbackMarkers.ToArray();
            RectTransform r;
            for(int i = 0; i < pbm.Length; i++) {
                r = pbm[i].Rect;
                r.anchoredPosition = new Vector2(r.anchoredPosition.x, beatMarkerAnchoredPositions[i]);
            }
        }

        #region Playback
        /// <summary>
        /// Moves the playbackMarkers by the passed in values.
        /// They respawn at the top if they pass too far down.
        /// </summary>
        /// <param name="speed">The speed at which the markers should move.</param>
        /// <param name="despawnHeight">The height at which the markers despawn.</param>
        /// <param name="spawnHeight">The height at which the markers reappear.</param>
        public void MovePlaybackMarkers(float speed, float despawnHeight, float spawnHeight) {
            foreach(BeatMarkerData marker in playbackMarkers) {
                marker.Rect.anchoredPosition += new Vector2(0f, speed);
            }
        }

        /// <summary>
        /// Swaps the first (lowest) playback marker to be the last (highest) one and updates
        /// its position.
        /// </summary>
        /// <param name="spawnHeight">The height of the heighest marker (this markers new y-position (not anchored)).</param>
        /// <param name="recalculateMarkerHighlight">Whether to rescale the marker moved to the top to be a main beat or not.
        /// Requires setting the bottomBeatIndex beforehand.</param>
        public void SwapPlaybackMarkers(float spawnHeight, bool recalculateMarkerHighlight = false) {
            LinkedListNode<BeatMarkerData> r = playbackMarkers.First;
            r.Value.Rect.position = new Vector2(r.Value.Rect.position.x, spawnHeight);
            if(recalculateMarkerHighlight) {
                SetProperBeatMarkerSize(r.Value, TopMarkerBeatIndex);
            }
            playbackMarkers.RemoveFirst();
            playbackMarkers.AddLast(r.Value);
        }

        /// <summary>
        /// Returns the anchored height of the playback marker at the given beatIndex.
        /// Only applicable if the requester has the same anchor.
        /// Safe to use, has out of bounds checks.
        /// </summary>
        /// <param name="beatIndex">The beatIndex to get the height for.</param>
        public float GetPlaybackMarkerAnchoredPositionFromBeatIndex(int beatIndex) {
            if(beatIndex <= bottomMarkerBeatIndex) return beatMarkerAnchoredPositions[0];
            else if(beatIndex >= TopMarkerBeatIndex) return beatMarkerAnchoredPositions[beatMarkerAnchoredPositions.Length - 1];

            LinkedListNode<BeatMarkerData> result;

            int index = beatIndex - bottomMarkerBeatIndex - 1;
            index = Mathf.Clamp(index, 0, playbackMarkers.Count - 1);

            result = playbackMarkers.First;
            for(int i = 1; i <= index; i++) {
                result = result.Next;
            }

            return result.Value.Rect.anchoredPosition.y;
        }

        /// <summary>
        /// Changes the info displayed next to the beatMarkers.
        /// </summary>
        /// <param name="beatMarkerInfoType">The info type to be displayed.</param>
        /// <param name="calledDuringLoading">Whether this was called during loading.</param>
        public void SetBeatMarkerInfoType(BeatMarkerInfoType beatMarkerInfoType, bool calledDuringLoading = false) {
            this.BeatMarkerInfoType = beatMarkerInfoType;
            if(calledDuringLoading) return;

            for(int i = 0; i < beatMarkers.Length; i++) {
                bool isMainBeat = IsMainBeat(bottomMarkerBeatIndex + i * (BeatIndicesInbetweenBeatMarker + 1));
                if(!isMainBeat || beatMarkerInfoType == BeatMarkerInfoType.None) beatMarkerTexts[i].gameObject.SetActive(false);
                else if(isMainBeat && beatMarkerInfoType == BeatMarkerInfoType.BeatIndex) {
                    beatMarkerTexts[i].text = (bottomMarkerBeatIndex + i * (BeatIndicesInbetweenBeatMarker + 1)).ToString();
                    beatMarkerTexts[i].gameObject.SetActive(true);
                } else if(isMainBeat && beatMarkerInfoType == BeatMarkerInfoType.Time) {
                    beatMarkerTexts[i].text = LevelDataUtilities.ConvertBeatIndexToTime((bottomMarkerBeatIndex + i * (BeatIndicesInbetweenBeatMarker + 1)), levelEditor.Level).ToString("F2");
                    beatMarkerTexts[i].gameObject.SetActive(true);
                }
            }
        }

        private void OnBeginPlayback() {
            miniBeatMarkersWereEnabled = ShowMiniBeatMarkers;
            DisplayMiniBeatMarkers(false);
            DisplayBeatMarkers(false);
            playbackMarkers = new LinkedList<BeatMarkerData>();

            int bottomOffset = bottomMarkerBeatIndex % (BeatIndicesInbetweenBeatMarker + 1);
            int firstMiniMarker = bottomOffset;

            for(int i = 0; i < beatMarkers.Length; i++) {
                if(i >= 1) {
                    RectTransform m = Instantiate(beatMarkerPrefab, transform).GetComponent<RectTransform>();
                    Image img = m.GetComponent<Image>();
                    img.color = normalMarkerColor;
                    m.anchoredPosition = beatMarkers[i].Rect.anchoredPosition;
                    m.sizeDelta = beatMarkers[i].Rect.sizeDelta;
                    m.GetChild(0).gameObject.SetActive(false);
                    playbackMarkers.AddLast(new BeatMarkerData() { Rect = m, Model = img });
                }

                for(int j = 0; j < miniBeatMarkers[i].Length; j++) {
                    if(i == 0 && j < firstMiniMarker) continue;
                    if(i == beatMarkers.Length - 1 && j >= firstMiniMarker) continue;

                    RectTransform t = Instantiate(miniBeatMarkerPrefab, transform).GetComponent<RectTransform>();
                    Image img = t.GetComponent<Image>();
                    img.color = miniMarkerColor;
                    t.anchoredPosition = miniBeatMarkers[i][j].Rect.anchoredPosition;
                    t.sizeDelta = miniBeatMarkers[i][j].Rect.sizeDelta;
                    playbackMarkers.AddLast(new BeatMarkerData() { Rect = t, Model = img });
                    t.gameObject.SetActive(miniBeatMarkersWereEnabled);
                }
            }
        }

        private void OnEndPlayback() {
            DisplayBeatMarkers(true);
            DisplayMiniBeatMarkers(miniBeatMarkersWereEnabled);
            foreach(BeatMarkerData rect in playbackMarkers) {
                Destroy(rect.Rect.gameObject);
            }
            playbackMarkers = null;
            ChangeBottomMarkerBeatIndex(0);
        }
        #endregion

        //private void OnDrawGizmos() {
        //    if(currentBeatMarkerPositionsHeight == 0) return;
        //    Gizmos.matrix = GetComponent<RectTransform>().localToWorldMatrix;
        //    Vector3 size = new Vector3(5f, 5f, 5f);
        //    for(int i = 0; i < beatMarkerScreenPositions.Length; i++) {
        //        Gizmos.color = Color.blue;
        //        Vector3 centre = new Vector2(0f, beatMarkerScreenPositions[i]);
        //        Gizmos.DrawCube(centre, size);
        //    }
        //}
    }

    public struct BeatMarkerData {
        public RectTransform Rect;
        public Image Model;
    }
}
