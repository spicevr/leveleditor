using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;

namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Author: Kevin Andersch
    /// Date: 22.04.22
    /// Version: 1.0
    /// </summary>
    public class LevelDataDisplay : MonoBehaviour {
        [Header("References")]
        [SerializeField] private PopupUtility popupUtility;
        [SerializeField] private LevelEditor levelEditor;
        [SerializeField] private TextMeshProUGUI titleText;
        [SerializeField] private TextMeshProUGUI difficultyText;
        [SerializeField] private TextMeshProUGUI bpmText;
        [SerializeField] private BPMAdjuster bpmAdjuster;
        [SerializeField] private TextMeshProUGUI initLanesText;
        [SerializeField] private TextMeshProUGUI audioIDText;
        [SerializeField] private TextMeshProUGUI timeText;

        private void OnEnable() {
            LevelEditor.OnEditorWasOpened += UpdateLevelInfo;
        }

        private void OnDisable() {
            LevelEditor.OnEditorWasOpened -= UpdateLevelInfo;
        }

        /// <summary>
        /// Updates the level info displayed to the correct values.
        /// </summary>
        public void UpdateLevelInfo() {
            LevelData level = levelEditor.Level;
            titleText.text = level.Name;
            difficultyText.text = level.Difficulty.ToString();
            bpmText.text = level.Bpm.ToString();
            initLanesText.text = level.InitialNumberOfLanes.ToString();
            if(LevelEditorMusicPlayer.Instance.SetTrack((uint)level.AudioTrackID)) {
                audioIDText.text = level.AudioTrackID.ToString();
            } else {
                audioIDText.text = "none";
            }
        }

        /// <summary>
        /// Opens a prompt for the user to set a new name for the level being currently edited.
        /// </summary>
        public void ChangeLevelTitle() {
            popupUtility.ShowInputPopup("New Title", titleText.text, OnTitleChangeCallbackReceiver);
        }

        /// <summary>
        /// Opens a prompt for the user to set a new difficulty for the level being currently edited.
        /// </summary>
        public void ChangeLevelDifficulty() {
            List<string> choices = System.Enum.GetNames(typeof(LevelDifficulty)).ToList();
            int initialIndex = System.Array.IndexOf(System.Enum.GetValues(typeof(LevelDifficulty)), levelEditor.Level.Difficulty);
            popupUtility.ShowDropdownPopup("New Difficulty", choices, initialIndex, OnDifficultyChangeCallbackReceiver);
        }

        /// <summary>
        /// Opens a prompt for the user to set a new bpm for the level being currently edited.
        /// If the level already has notes placed, prompts the user to either move existing notes
        /// or delete them.
        /// </summary>
        public void ChangeLevelBPM() {
            bpmAdjuster.ShowAdjustmentPanel(levelEditor.Level.Bpm, OnBPMChangeCallbackReceiver);
        }

        /// <summary>
        /// Opens a prompt for the user to set a new initial number of lanes for the level being
        /// currently edited. If the level already has notes placed, prompts the user to either remove
        /// them or cancel the process.
        /// </summary>
        public void ChangeInitialNumberOfLanes() {
            popupUtility.ShowInputPopup("New Initial Lanes", levelEditor.Level.InitialNumberOfLanes, OnInitLanesChangeCallbackReceiver);
        }

        /// <summary>
        /// Opens a prompt for the user to set a new audio track ID for the level being
        /// currently edited. If changed, also changes the music the level will play during playback.
        /// </summary>
        public void ChangeAudioTrackID() {
            popupUtility.ShowInputPopup("New Audio Track ID", 0, OnAudioTrackIDChangedCallbackReceiver);
        }

        private void OnTitleChangeCallbackReceiver(int buttonResult, string value) {
            if(buttonResult == 1) return;

            titleText.text = value;
            levelEditor.Level.Name = value;
            levelEditor.SetUnsavedChanges();
        }

        private void OnDifficultyChangeCallbackReceiver(int buttonResult, int value) {
            if(buttonResult == 1) return;

            // TODO: check if that difficulty already exists maybe
            LevelDifficulty difficulty = (LevelDifficulty)(System.Enum.GetValues(typeof(LevelDifficulty))).GetValue(value);
            difficultyText.text = difficulty.ToString();
            levelEditor.Level.Difficulty = difficulty;
            levelEditor.SetUnsavedChanges();
        }

        private void OnBPMChangeCallbackReceiver(bool buttonResult, int value) {
            if(!buttonResult) return;
            if(value == levelEditor.Level.Bpm) return;

            if(levelEditor.NoteCount > 0) {
                string[] choices = new string[] { "Move Notes", "Delete Notes", "Cancel" };
                popupUtility.ShowPopup("Changing BPM", "You are about to change the BPM value of a" +
                    "\nlevel that already has placed notes." +
                    "\nDo you wish to move all already existing notes of the level to their new" +
                    "\ntime value (according to the new BPM value) or should all existing notes" +
                    "\nbe deleted?", choices, (n) => OnBPMChangeCausedInvalidNotesCallbackReceiver(n, value), true);
            } else {
                levelEditor.Level.Bpm = value;
                bpmText.text = value.ToString();
                levelEditor.SetUnsavedChanges();
            }
        }

        private void OnBPMChangeCausedInvalidNotesCallbackReceiver(int buttonResult, int value) {
            if(buttonResult == 2) return;

            if(buttonResult == 1) {
                levelEditor.RemoveAllNotes();
            }

            levelEditor.Level.Bpm = value;
            bpmText.text = value.ToString();
            levelEditor.SetUnsavedChanges();
        }

        private void OnInitLanesChangeCallbackReceiver(int choice, int? value) {
            if(choice == 1 || value == null || (int)value <= 0) return;

            if(levelEditor.NoteCount > 0) {
                popupUtility.ShowPopup("Notes Detected", "The level already has notes placed in it." +
                    "\nChanging the initial number of lanes of the level will result in all notes" +
                    "\nbeing deleted. Do you wish to continue?", "Delete all notes", "Cancel", (b) => {
                        if(!b) return;
                        OnInitLanesChangeWithLevelNotesPresentCallbackReceiver((int)value);
                    });
                return;
            }

            levelEditor.Level.InitialNumberOfLanes = (int)value;
            initLanesText.text = value.ToString();

            levelEditor.SetUnsavedChanges();
        }

        private void OnInitLanesChangeWithLevelNotesPresentCallbackReceiver(int newInitLanes) {
            levelEditor.Level.InitialNumberOfLanes = newInitLanes;
            initLanesText.text = newInitLanes.ToString();
            levelEditor.RemoveAllNotes();
            levelEditor.SetUnsavedChanges();
        }

        private void OnAudioTrackIDChangedCallbackReceiver(int choice, int? value) {
            if(choice == 1 || value == null || (int)value < 0) return;

            if(!LevelEditorMusicPlayer.Instance.SetTrack((uint)value, ErrorMessageDisplayType.Popup)) return;

            audioIDText.text = value.ToString();
            levelEditor.Level.AudioTrackID = (int)value;
            levelEditor.SetUnsavedChanges();
        }
    }
}
