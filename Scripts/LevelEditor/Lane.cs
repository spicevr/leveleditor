using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Author: Kevin Andersch
    /// Date: 04.04.22
    /// Version: 1.0
    /// </summary>
    public class Lane : MonoBehaviour, IPointerExitHandler, IPointerEnterHandler, IPointerDownHandler {
        [Header("Reference Setup")]
        [SerializeField] private Image background;
        [SerializeField] private RectTransform upperNoteSpawnPoint;
        [SerializeField] private RectTransform lowerNoteSpawnPoint;
        [SerializeField] private Transform noteHolder;

        [Header("Lane Setup")]
        [SerializeField] private Color highlightColor;
        [SerializeField] private Color temporaryColor;
        [SerializeField] private Color permanentColor;
        [SerializeField] private float overlappingNoteDetectionMagnitude;
        [SerializeField] private float despawnLocationOffset;

        public int LaneIndex { get; private set; }
        public bool GetsDeletedOnBecomingPermanent { get => IsTemporaryLane && canHaveNotes; }
        public bool IsTemporaryLane { get; private set; }

        private Color initialColor;
        private LevelEditor levelEditor;
        private List<LaneNote> activeNotes;
        private LaneChunk laneChunk;
        private bool shouldRecheckForCorrectLane;
        private bool canHaveNotes;

        private static Lane LaneWithLastPlacedNote;

        // Events
        /// <summary>
        /// 0: the lane the note was destroyed in
        /// 1: the destroyed notes transform
        /// </summary>
        public static System.Action<Lane, RectTransform> OnAnyNoteWasDestroyedByMovement;
        /// <summary>
        /// 0: the destroyed notes transform
        /// </summary>
        public System.Action<RectTransform> OnNoteWasDestroyedByMovement;
        /// <summary>
        /// 0: the lane the note was destroyed in
        /// 1: the overlapping note that already exists
        /// </summary>
        public System.Action<Lane, RectTransform> OnAnyFoundOverlappingNote;
        /// <summary>
        /// 0: the overlapping note that already exists
        /// </summary>
        public System.Action<RectTransform> OnFoundOverlappingNote;

        private void OnDisable() {
            BeatMarkerManager.OnMarkerUnderCursorChanged -= OnMarkerUnderPointerChangedCallbackReceiver;
        }

        private void Update() {
            if(shouldRecheckForCorrectLane) RecheckIfOtherLaneIsStillCorrect(levelEditor.MarkerUnderPointerBeatIndex);
        }

        /// <summary>
        /// Initializes this lane with the required values and references.
        /// Should be called right after instantiating it.
        /// </summary>
        /// <param name="levelEditor">The level editor of the scene.</param>
        /// <param name="laneChunk">The lane chunk this lane belongs to.</param>
        /// <param name="laneIndex">The index for this lane and all its notes.</param>
        /// <param name="isTemporaryLane">Whether this is a temporary lane and should be visually displayed as such.</param>
        public void Initialize(LevelEditor levelEditor, LaneChunk laneChunk, int laneIndex, bool isTemporaryLane, bool canHaveNotes = true) {
            this.levelEditor = levelEditor;
            this.laneChunk = laneChunk;
            LaneIndex = laneIndex;
            activeNotes = new List<LaneNote>();
            if(isTemporaryLane) background.color = temporaryColor;
            else background.color = permanentColor;
            this.IsTemporaryLane = isTemporaryLane;
            initialColor = background.color;
            this.canHaveNotes = canHaveNotes;
        }

        /// <summary>
        /// Moves all currently active notes to the next beat marker in the
        /// corresponding direction.
        /// </summary>
        /// <param name="moveForward">Whether to move downwards (true) or upwards (false).</param>
        public void MoveNotes(bool moveForward) {
            float direction = moveForward ? -levelEditor.BeatMarkerDistance : levelEditor.BeatMarkerDistance;
            List<LaneNote> notesToRemove = new List<LaneNote>();
            RectTransform r;
            foreach(LaneNote note in activeNotes) {
                if(!levelEditor.IsWithinViewArea(note.BeatIndex)) {
                    notesToRemove.Add(note);
                    continue;
                }
                r = note.Rect;
                r.position = new Vector2(r.position.x,
                    levelEditor.BeatMarkerManager.GetBeatMarkerHeightFromBeatIndex(note.BeatIndex, true));
            }
            foreach(LaneNote note in notesToRemove) {
                activeNotes.Remove(note);
                Destroy(note.Rect.gameObject);
                OnAnyNoteWasDestroyedByMovement?.Invoke(this, note.Rect);
                OnNoteWasDestroyedByMovement?.Invoke(note.Rect);
            }
        }

        /// <summary>
        /// Recalculates the horizontal positions of all visible notes in this lane and replaces them.
        /// Usefull for horizontal adjustments when the layout group of the parent changed.
        /// </summary>
        public void RecalculateNotePositions() {
            foreach(LaneNote note in activeNotes) {
                note.Rect.position = new Vector2(transform.position.x, note.Rect.position.y);
            }
        }

        /// <summary>
        /// Returns all notes belonging to this lane.
        /// </summary>
        public List<LaneNote> GetNotes() => activeNotes;

        public void AssignNote(LaneNote note) {
            activeNotes.Add(note);
        }

        /// <summary>
        /// Spawn a note at the top of the editor screen, assigning it to this lane.
        /// Does not spawn a note if one already exists at the spawn position.
        /// </summary>
        /// <param name="giveErrorFeedbackOnNotSpawning">Whether to throw an exception if a note can't be spawned.</param>
        public void SpawnNoteAtTop(EditorNote note, int beatIndex, bool giveErrorFeedbackOnNotSpawning = false, bool ignoresStateChanges = false) {
            Vector2 spawnPosition = new Vector2(upperNoteSpawnPoint.position.x, levelEditor.GetBeatMarkerHeight(new MarkerIndex { NormalMarkerIndex = 999 }));
            
            if(!canHaveNotes) {
                if(giveErrorFeedbackOnNotSpawning) Debug.LogError($"Tried to spawn a note on a temporary lane that can't hold notes once permanent!");
                return;
            }

            if(CheckIfNoteAlreadyExistsAtPosition(spawnPosition)) {
                if(giveErrorFeedbackOnNotSpawning) Debug.LogError($"Tried to spawn a note at position x:{spawnPosition.x} / y:{spawnPosition.y} that would overlap with an already existing one!");
                return;
            }

            SpawnNote(note, spawnPosition, beatIndex, ignoresStateChanges);
        }

        /// <summary>
        /// Spawn a note at the bottom of the editor screen, assigning it to this lane.
        /// Does not spawn a note if one already exists at the spawn position.
        /// </summary>
        /// <param name="giveErrorFeedbackOnNotSpawning">Whether to throw an exception if a note can't be spawned.</param>
        public void SpawnNoteAtBottom(EditorNote note, int beatIndex, bool giveErrorFeedbackOnNotSpawning = false, bool ignoresStateChanges = false) {
            Vector2 spawnPosition = new Vector2(lowerNoteSpawnPoint.position.x, levelEditor.GetBeatMarkerHeight(new MarkerIndex { NormalMarkerIndex = 0 }));

            if(!canHaveNotes) {
                if(giveErrorFeedbackOnNotSpawning) Debug.LogError($"Tried to spawn a note on a temporary lane that can't hold notes once permanent!");
                return;
            }

            if(CheckIfNoteAlreadyExistsAtPosition(spawnPosition)) {
                if(giveErrorFeedbackOnNotSpawning) Debug.LogError($"Tried to spawn a note at position x:{spawnPosition.x} / y:{spawnPosition.y} that would overlap with an already existing one!");
                return;
            }

            SpawnNote(note, spawnPosition, beatIndex, ignoresStateChanges);
        }

        /// <summary>
        /// Spawn a note at the passed in height, assigning it to this lane.
        /// Does not spawn a note if one already exists at the spawn position.
        /// Does not add the note to the notes of the level (only creates its visual).
        /// </summary>
        /// <param name="noteType">The type of the note to spawn.</param>
        /// <param name="height">The height at which to spawn the note.</param>
        /// <param name="beatIndex">The beatIndex of the note (for debug purposes).</param>
        /// <param name="giveErrorFeedbackOnNotSpawning">Whether an error should be thrown if the new note would overlap with an already existing one.</param>
        /// <param name="ignoresStateChanges">Whether this method will change the editing state of the level editor if notes would do so.</param>
        public void SpawnNoteAtCustomHeight(EditorNote note, float height, int beatIndex, bool giveErrorFeedbackOnNotSpawning = false, bool ignoresStateChanges = false) {
            Vector2 spawnPosition = new Vector2(lowerNoteSpawnPoint.position.x, height);

            if(!canHaveNotes) {
                if(giveErrorFeedbackOnNotSpawning) Debug.LogError($"Tried to spawn a note on a temporary lane that can't hold notes once permanent!");
                return;
            }

            if(CheckIfNoteAlreadyExistsAtPosition(spawnPosition)) {
                if(giveErrorFeedbackOnNotSpawning) Debug.LogError($"Tried to spawn a note at beatIntex {beatIndex} with position x:{spawnPosition.x} / y:{spawnPosition.y} that would overlap with an already existing one!");
                return;
            }

            if(ignoresStateChanges && note.Type == NoteType.LaneSwitch && beatIndex == laneChunk.StartBeatIndex) {
                //levelEditor.LaneManager.AddNoteToCheckOnMove(beatIndex, LaneIndex, noteType);
                return;
            }

            SpawnNote(note, spawnPosition, beatIndex, ignoresStateChanges);
        }

        /// <summary>
        /// Forces this lane to spawn a note at the passed in values, ignoring all
        /// safety checks and any event triggers.
        /// </summary>
        /// <param name="noteType">The type of the note to spawn.</param>
        /// <param name="height">The height to spawn the note at.</param>
        /// <param name="beatIndex">The beatIndex of the note.</param>
        public void ForceSpawnNote(EditorNote note, float height, int beatIndex) {
            Vector2 spawnPosition = new Vector2(lowerNoteSpawnPoint.position.x, height);
            SpawnNote(note, spawnPosition, beatIndex, true);
        }

        /// <summary>
        /// Deletes the visual representation of the note at the passed in height.
        /// Does not change the serialized notes of the level.
        /// </summary>
        /// <param name="height">The height of the note to be deleted.</param>
        /// <param name="displayErrorMessage">Which type of error message will be displayed should no note be found.</param>
        public bool DeleteNote(float height, ErrorMessageDisplayType displayErrorMessage = ErrorMessageDisplayType.Ignore) {
            foreach(LaneNote item in activeNotes) {
                float h = item.Rect.position.y;
                if(height >= h - overlappingNoteDetectionMagnitude && height <= h + overlappingNoteDetectionMagnitude) {
                    activeNotes.Remove(item);
                    Destroy(item.Rect.gameObject);
                    return true;
                }
            }

            switch(displayErrorMessage) {
                case ErrorMessageDisplayType.Ignore:
                    break;
                case ErrorMessageDisplayType.Console:
                    Debug.LogError($"Tried to delete a note at height {height} but didn't find any!");
                    break;
                case ErrorMessageDisplayType.OnScreen:
                    levelEditor.PopupUtilityInstance.ShowOnScreenMessage($"Tried to delete a note at height {height} but didn't find any!");
                    break;
                case ErrorMessageDisplayType.Popup:
                    levelEditor.PopupUtilityInstance.ShowPopup("Note Deletion Error", $"Tried to delete a note at height {height} but didn't find any!", isErrorPopup: true);
                    break;
            }

            return false;
        }

        /// <summary>
        /// Deletes the visual representation of the note at the passed in beatIndex.
        /// Does not change the serialized notes of the level.
        /// Returns whether a note was deleted or not.
        /// </summary>
        /// <param name="beatIndex">The beatIndex of the note to be deleted.</param>
        /// <param name="displayErrorMessage">Which type of error message will be displayed should no note be found.</param>
        public bool DeleteNote(int beatIndex, ErrorMessageDisplayType displayErrorMessage = ErrorMessageDisplayType.Ignore) {
            foreach(LaneNote item in activeNotes) {
                if(item.BeatIndex == beatIndex) {
                    activeNotes.Remove(item);
                    Destroy(item.Rect.gameObject);
                    return true;
                }
            }

            switch(displayErrorMessage) {
                case ErrorMessageDisplayType.Ignore:
                    break;
                case ErrorMessageDisplayType.Console:
                    Debug.LogError($"Tried to delete a note at beatIndex {beatIndex} but didn't find any!");
                    break;
                case ErrorMessageDisplayType.OnScreen:
                    levelEditor.PopupUtilityInstance.ShowOnScreenMessage($"Tried to delete a note at beatIndex {beatIndex} but didn't find any!");
                    break;
                case ErrorMessageDisplayType.Popup:
                    levelEditor.PopupUtilityInstance.ShowPopup("Note Deletion Error", $"Tried to delete a note at beatIndex {beatIndex} but didn't find any!", isErrorPopup: true);
                    break;
            }

            return false;
        }

        /// <summary>
        /// Deletes the last placed note from the corresponding lane.
        /// Removes only the visual representation, NOT the data of the level.
        /// </summary>
        public static void DeleteLastPlacedNote() {
            LaneWithLastPlacedNote.RemoveLastNote();
        }

        /// <summary>
        /// Deletes the last placed note from the corresponding lane.
        /// Also removes the serialized data of the level, removing both the notes
        /// visuals and its data.
        /// </summary>
        public static void DestroyLastPlacedNote() {
            LaneWithLastPlacedNote.DestroyLastNote();
        }

        public static LaneNote? GetLastPlacedNote() {
            if(LaneWithLastPlacedNote != null) return LaneWithLastPlacedNote.GetLastNote();
            else return null;
        }

        /// <summary>
        /// Destroys all visible notes (only visuals, not data).
        /// </summary>
        public void DestroyAllNotes() {
            foreach(LaneNote note in activeNotes) {
                Destroy(note.Rect.gameObject);
            }
            activeNotes.Clear();
        }

        /// <summary>
        /// Decouples all currently visible notes of this lane.
        /// </summary>
        public List<RectTransform> DecoupleAllNotes() {
            List<RectTransform> allNotes = new List<RectTransform>(activeNotes.Count);
            for(int i = 0; i < activeNotes.Count; i++) {
                allNotes.Add(activeNotes[i].Rect);
            }
            activeNotes.Clear();
            return allNotes;
        }

        /// <summary>
        /// Highlights this lane (visually) and sets it to be the note receiver for
        /// on click note creation.
        /// </summary>
        public void Highlight() {
            if(!canHaveNotes) return;
            if(levelEditor.IsEditing) background.color = highlightColor;
            levelEditor.LaneUnderPointer = this;
        }

        /// <summary>
        /// Cancels the visual highlight of this lane.
        /// </summary>
        public void CancelHighlight() {
            if(background != null) background.color = initialColor;
            levelEditor.LaneUnderPointer = null;
        }

        /// <summary>
        /// Resets the color of this lane back to its original color;
        /// </summary>
        public void ResetColor() => background.color = initialColor;

        /// <summary>
        /// Changes this lane to be a permanent one.
        /// </summary>
        public void ChangeToPermanent() {
            background.color = permanentColor;
            initialColor = permanentColor;
            canHaveNotes = true;
            IsTemporaryLane = false;
        }

        /// <summary>
        /// Changes this lane to be a temporary one.
        /// This makes it so this lane cannot have notes.
        /// </summary>
        public void ChangeToTemporary() {
            background.color = temporaryColor;
            initialColor = temporaryColor;
            canHaveNotes = false;
            IsTemporaryLane = true;
        }

        /// <summary>
        /// Returns whether this lane contains a note with the passed in beatIndex.
        /// </summary>
        /// <param name="beatIndex">The beatIndex to check for.</param>
        public bool HasNote(int beatIndex) {
            foreach(LaneNote note in activeNotes) {
                if(note.BeatIndex == beatIndex) return true;
            }
            return false;
        }

        private void RemoveLastNote() {
            Destroy(activeNotes[activeNotes.Count - 1].Rect.gameObject);
            activeNotes.RemoveAt(activeNotes.Count - 1);
        }

        private void DestroyLastNote() {
            LaneNote note = activeNotes[activeNotes.Count - 1];
            levelEditor.DeleteNote(note.BeatIndex, true);
            Destroy(note.Rect.gameObject);
            activeNotes.RemoveAt(activeNotes.Count - 1);
        }

        private LaneNote GetLastNote() {
            return activeNotes[activeNotes.Count - 1];
        }

        private void SpawnNote(EditorNote note, Vector2 location, int beatIndex, bool ignoresStateChanges = false) {
            GameObject n = NoteSpawner.Instance.SpawnNote(note, location, levelEditor.NoteHolder, ignoresStateChanges);
            activeNotes.Add(new LaneNote() { BeatIndex = beatIndex, Lane = LaneIndex, Rect = n.GetComponent<RectTransform>(), Type = note.Type });
            LaneWithLastPlacedNote = this;
        }

        private bool CheckIfNoteAlreadyExistsAtPosition(Vector2 position) {
            for(int i = 0; i < activeNotes.Count; i++) {
                Vector2 vector = position - (Vector2)activeNotes[i].Rect.position;
                if(vector.magnitude <= overlappingNoteDetectionMagnitude) {
                    OnAnyFoundOverlappingNote?.Invoke(this, activeNotes[i].Rect);
                    OnFoundOverlappingNote?.Invoke(activeNotes[i].Rect);
                    return true;
                }
            }

            return false;
        }

        private bool CheckIfCanPlaceNoteWithCurrentNotePlacementLocation() {
            switch(levelEditor.NotePlacementLocation) {
                case NotePlacementLocation.Top:
                    return laneChunk.EndBeatIndex >= levelEditor.BeatMarkerManager.TopMarkerBeatIndex;
                case NotePlacementLocation.Bottom:
                    return laneChunk.StartBeatIndex <= levelEditor.BeatMarkerManager.BottomMarkerBeatIndex;
                case NotePlacementLocation.UnderCursor:
                    bool result = MarkerUnderPointerBelongsToMe(levelEditor.MarkerUnderPointerBeatIndex);
                    if(!result) {
                        RecheckIfOtherLaneIsStillCorrect(levelEditor.MarkerUnderPointerBeatIndex);
                    }
                    return result;
                default:
                    return false;
            }
        }

        private void RecheckIfOtherLaneIsStillCorrect(int beatIndex) {
            Lane properLane = levelEditor.LaneManager.GetLaneAtBeatIndex(beatIndex, Input.mousePosition.x);
            if(properLane != null && properLane != levelEditor.LaneUnderPointer) {
                levelEditor.LaneUnderPointer?.CancelHighlight();
                properLane.Highlight();
            }
        }

        private bool MarkerUnderPointerBelongsToMe(int beatIndex) {
            LaneChunkData? chunk = levelEditor.GetLaneChunk(beatIndex);
            if(chunk == null) return true;
            else if(((LaneChunkData)chunk).StartBeatIndex == laneChunk.StartBeatIndex) return true;
            else return false;
        }

        private void OnMarkerUnderPointerChangedCallbackReceiver(int beatIndex) {
            if(!MarkerUnderPointerBelongsToMe(beatIndex)) {
                CancelHighlight();
                levelEditor.LaneManager.GetLaneAtBeatIndex(beatIndex, transform.position.x)?.Highlight();
            } else if(levelEditor.LaneUnderPointer != this) {
                levelEditor.LaneUnderPointer?.CancelHighlight();
                Highlight();
            }
        }

        #region Interface Implementation
        public void OnPointerDown(PointerEventData eventData) {
            if(eventData.button != PointerEventData.InputButton.Left
                || !levelEditor.IsEditing || !canHaveNotes) return;
            levelEditor.PlaceNoteAtLaneUnderCursor();
        }

        public void OnPointerEnter(PointerEventData eventData) {
            BeatMarkerManager.OnMarkerUnderCursorChanged += OnMarkerUnderPointerChangedCallbackReceiver;
            shouldRecheckForCorrectLane = true;
            if(!canHaveNotes || !CheckIfCanPlaceNoteWithCurrentNotePlacementLocation()) return;
            levelEditor.LaneUnderPointer?.CancelHighlight();
            Highlight();
        }

        public void OnPointerExit(PointerEventData eventData) {
            BeatMarkerManager.OnMarkerUnderCursorChanged -= OnMarkerUnderPointerChangedCallbackReceiver;
            levelEditor.LaneUnderPointer?.CancelHighlight();
            shouldRecheckForCorrectLane = false;
        }
        #endregion
    }

    [System.Serializable]
    public struct LaneNote {
        public int BeatIndex;
        public int Lane;
        public RectTransform Rect;
        public NoteType Type;
    }
}
