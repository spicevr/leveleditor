using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Author: Kevin Andersch
    /// Date: 11.04.22
    /// Version: 1.0
    /// </summary>
    public class NoteTypeSelector : MonoBehaviour {
        [Header("References")]
        [SerializeField] private GameObject setterPanel;
        [SerializeField] private LevelEditor levelEditor;
        [SerializeField] private NotePreviewer notePreviewer;

        [Header("UI Setup")]
        [SerializeField] private Color currentlyActiveColor;

        public NoteType ActiveNoteType {
            get => activeNoteType;
            private set {
                notePreviewer.ShowPreview(value);
                activeNoteType = value;
            }
        }

        private List<NoteTypeOnClickData> noteTypeUIElements;
        private Color initialColor;
        private NoteType activeNoteType;

        // Events
        /// <summary>
        /// 0: The newly set active note type
        /// </summary>
        public static System.Action<NoteType> OnActiveNoteTypeWasChanged;

        private void OnEnable() {
            LevelEditor.OnEditorWasOpened += OpenSetterPanel;
            LevelEditor.OnBeforeEditorWasClosed += CloseSetterPanel;
            LevelEditor.OnBeganEditingHoldNote += OnBeginHoldNoteEdit;
            LevelEditor.OnStoppedEditingHoldNote += OnEndHoldNoteEdit;
        }

        private void OnDisable() {
            LevelEditor.OnEditorWasOpened -= OpenSetterPanel;
            LevelEditor.OnBeforeEditorWasClosed -= CloseSetterPanel;
            LevelEditor.OnBeganEditingHoldNote -= OnBeginHoldNoteEdit;
            LevelEditor.OnStoppedEditingHoldNote -= OnEndHoldNoteEdit;
        }

        public void OpenSetterPanel() {
            setterPanel.SetActive(true);
        }

        public void CloseSetterPanel() {
            setterPanel.SetActive(false);
            notePreviewer.StopPreview();
        }

        /// <summary>
        /// Changes the active note type (to be placed on click) of the editor.
        /// </summary>
        /// <param name="noteTypeData">The new active note type.</param>
        public void ChangeActiveNoteType(NoteTypeOnClickData noteTypeData) {
            // TODO: check if allowed to switch (aka if only single hold note placed, then not)

            ActiveNoteType = noteTypeData.NoteType;
            foreach(NoteTypeOnClickData item in noteTypeUIElements) {
                if(item == noteTypeData) item.BackgroundImage.color = currentlyActiveColor;
                else item.BackgroundImage.color = initialColor;
            }

            switch(ActiveNoteType) {
                case NoteType.Normal:
                    levelEditor.BeatMarkerManager.TargetingMode = BeatMarkerTargetingMode.AllBeats;
                    break;
                case NoteType.Hold:
                    levelEditor.BeatMarkerManager.TargetingMode = BeatMarkerTargetingMode.AllBeats;
                    break;
                case NoteType.Gaze:
                    levelEditor.BeatMarkerManager.TargetingMode = BeatMarkerTargetingMode.AllBeats;
                    if(!levelEditor.BeatMarkerManager.ShowMiniBeatMarkers) levelEditor.BeatMarkerManager.ToggleMiniBeatMarkers();
                    break;
                case NoteType.Event:
                    break;
                case NoteType.LaneSwitch:
                    levelEditor.BeatMarkerManager.TargetingMode = BeatMarkerTargetingMode.AllBeats;
                    break;
            }

            OnActiveNoteTypeWasChanged?.Invoke(ActiveNoteType);
        }

        /// <summary>
        /// Registers note type data so that this script has a reference to its image component.
        /// </summary>
        /// <param name="data">The data that is registering.</param>
        public void RegisterNoteTypeData(NoteTypeOnClickData data) {
            if(noteTypeUIElements == null) noteTypeUIElements = new List<NoteTypeOnClickData>();
            noteTypeUIElements.Add(data);
            initialColor = data.BackgroundImage.color;
        }

        private void OnBeginHoldNoteEdit(int ingored) {
            levelEditor.BeatMarkerManager.TargetingMode = BeatMarkerTargetingMode.AllBeats;
        }

        private void OnEndHoldNoteEdit() {
            if(ActiveNoteType == NoteType.Normal) {
                levelEditor.BeatMarkerManager.TargetingMode = BeatMarkerTargetingMode.NormalBeatsOnly;
            }
        }
    }
}
