using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Author: Kevin Andersch
    /// Date: 07.04.22
    /// Version: 1.0
    /// </summary>
    public class NoteSpawnLocationSetter : MonoBehaviour {
        [Header("References")]
        [SerializeField] private LevelEditor levelEditor;
        [SerializeField] private GameObject locationSetterPanel;
        [SerializeField] private Image[] backgroundImages;

        [Header("UI Setup")]
        [SerializeField] private Color currentlyActiveColor;

        private Color initialColor;

        private void OnEnable() {
            LevelEditor.OnEditorWasOpened += OpenSetterPanel;
            LevelEditor.OnBeforeEditorWasClosed += CloseSetterPanel;
            LevelEditor.OnBeganPlacingHoldNote += OnBeganPlacingHoldNoteCallbackReceiver;
        }

        private void OnDisable() {
            LevelEditor.OnEditorWasOpened -= OpenSetterPanel;
            LevelEditor.OnBeforeEditorWasClosed -= CloseSetterPanel;
            LevelEditor.OnBeganPlacingHoldNote -= OnBeganPlacingHoldNoteCallbackReceiver;
        }

        public void OpenSetterPanel() {
            locationSetterPanel.SetActive(true);
            initialColor = backgroundImages[0].color;
            switch(levelEditor.NotePlacementLocation) {
                case NotePlacementLocation.Top:
                    backgroundImages[2].color = currentlyActiveColor;
                    break;
                case NotePlacementLocation.Bottom:
                    backgroundImages[0].color = currentlyActiveColor;
                    break;
                case NotePlacementLocation.UnderCursor:
                    backgroundImages[1].color = currentlyActiveColor;
                    break;
            }
        }

        public void CloseSetterPanel() => locationSetterPanel.SetActive(false);

        public void SetToTopSpawn() {
            if(levelEditor.CurrentEditorState == LevelEditorState.HoldNotePlacement) return;

            levelEditor.ChangeNoteSpawnLocation(NotePlacementLocation.Top);
            backgroundImages[0].color = initialColor;
            backgroundImages[1].color = initialColor;
            backgroundImages[2].color = currentlyActiveColor;
        }

        public void SetToBottomSpawn() {
            if(levelEditor.CurrentEditorState == LevelEditorState.HoldNotePlacement) return;

            levelEditor.ChangeNoteSpawnLocation(NotePlacementLocation.Bottom);
            backgroundImages[0].color = currentlyActiveColor;
            backgroundImages[1].color = initialColor;
            backgroundImages[2].color = initialColor;
        }

        public void SetToCursorSpawn() {
            levelEditor.ChangeNoteSpawnLocation(NotePlacementLocation.UnderCursor);
            backgroundImages[0].color = initialColor;
            backgroundImages[1].color = currentlyActiveColor;
            backgroundImages[2].color = initialColor;
        }

        private void OnBeganPlacingHoldNoteCallbackReceiver() {
            if(levelEditor.NotePlacementLocation != NotePlacementLocation.UnderCursor) SetToCursorSpawn();
        }
    }
}
