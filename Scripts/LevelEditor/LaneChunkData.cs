using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Author: Kevin Andersch
    /// Date: 07.04.22
    /// Version: 1.0
    /// </summary>
    [System.Serializable]
    public struct LaneChunkData {
        /// <summary>
        /// Inclusive
        /// </summary>
        public int StartBeatIndex;
        /// <summary>
        /// Exclusive
        /// </summary>
        public int EndBeatIndex;
        public int LaneCount;
        public int TemporaryLaneCount;
    }
}
