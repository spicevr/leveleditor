using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Author: Kevin Andersch
    /// Date: 11.04.22
    /// Version: 1.0
    /// </summary>
    public class NoteTypeOnClickData : MonoBehaviour {
        [SerializeField] private NoteTypeSelector noteTypeSelector;

        public Image BackgroundImage;
        public NoteType NoteType;

        private void Start() {
            noteTypeSelector.RegisterNoteTypeData(this);
            if(NoteType == NoteType.Normal) noteTypeSelector.ChangeActiveNoteType(this);
        }
    }
}
