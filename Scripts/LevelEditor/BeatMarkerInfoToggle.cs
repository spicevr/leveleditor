using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Author: Kevin Andersch
    /// Date: 13.05.22
    /// Version: 1.0
    /// </summary>
    public class BeatMarkerInfoToggle : MonoBehaviour, IPointerClickHandler {
        [SerializeField] private LevelEditor levelEditor;
        [SerializeField] private GameObject noneBackground;
        [SerializeField] private GameObject beatIndexBackground;
        [SerializeField] private GameObject timeBackground;

        private void OnEnable() {
            BeatMarkerInfoType infoType = levelEditor.Settings.BeatMarkerInfoType;
            switch(infoType) {
                case BeatMarkerInfoType.None:
                    noneBackground.SetActive(true);
                    break;
                case BeatMarkerInfoType.Time:
                    timeBackground.SetActive(true);
                    break;
                case BeatMarkerInfoType.BeatIndex:
                    beatIndexBackground.SetActive(true);
                    break;
            }
        }

        public void OnPointerClick(PointerEventData eventData) {
            BeatMarkerInfoType infoType = levelEditor.BeatMarkerManager.BeatMarkerInfoType;
            noneBackground.SetActive(false);
            beatIndexBackground.SetActive(false);
            timeBackground.SetActive(false);
            int t = (int)infoType;
            t++;
            BeatMarkerInfoType newType = (BeatMarkerInfoType)(t % System.Enum.GetValues(typeof(BeatMarkerInfoType)).Length);
            switch(newType) {
                case BeatMarkerInfoType.None:
                    noneBackground.SetActive(true);
                    break;
                case BeatMarkerInfoType.Time:
                    timeBackground.SetActive(true);
                    break;
                case BeatMarkerInfoType.BeatIndex:
                    beatIndexBackground.SetActive(true);
                    break;
            }
            levelEditor.BeatMarkerManager.SetBeatMarkerInfoType(newType);
        }
    }
}
