using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Manages playback of the song without disrupting the data of the level.
    /// 
    /// Author: Kevin Andersch
    /// Date: 28.04.22
    /// Version: 1.0
    /// </summary>
    public class NoteSpawner : MonoBehaviour {
        public static NoteSpawner Instance;

        [Header("References")]
        [SerializeField] private LevelEditor levelEditor;

        [Header("Prefabs")]
        [SerializeField] private GameObject normalNotePrefab;
        [SerializeField] private GameObject gazeNotePrefab;
        [SerializeField] private GameObject holdNotePrefab;
        [SerializeField] private GameObject laneSwitchNotePrefab;
        [SerializeField] private GameObject eventNotePrefab;

        private void Awake() {
            if(!Instance) Instance = this;
            else Destroy(gameObject);
        }

        /// <summary>
        /// Spawns a note of the correct passed in type at the desired location into the scene.
        /// Returns the created note.
        /// </summary>
        /// <param name="note">The data of the note to spawn.</param>
        /// <param name="location">The position to spawn the note at.</param>
        /// <param name="parent">The parent transform to spawn the note under. This is relevant for rect transform anchors.</param>
        /// <param name="ignoresStateChanges">Whether spawning this note triggers state changes in the editor or not
        /// (aka whether this was spawned by player input or not).</param>
        public GameObject SpawnNote(EditorNote note, Vector2 location, Transform parent, bool ignoresStateChanges = false) {
            switch(note.Type) {
                case NoteType.Normal:
                    return SpawnSimpleNote(normalNotePrefab, location, parent);
                case NoteType.Hold:
                    if(!ignoresStateChanges && levelEditor.CurrentEditorState != LevelEditorState.HoldNotePlacement) levelEditor.IsPlacingNewHoldNote();
                    return SpawnSimpleNote(holdNotePrefab, location, parent);
                case NoteType.Gaze:
                    return SpawnSimpleNote(gazeNotePrefab, location, parent);
                case NoteType.Event:
                    GameObject noteGO = SpawnSimpleNote(eventNotePrefab, location, parent);
                    EventNoteVisuals visuals = noteGO.GetComponent<EventNoteVisuals>();
                    if(ignoresStateChanges) {
                        visuals.EventType = note.EventType;
                        if(note.EventType == NoteEvent.Story) visuals.StoryID = note.StoryEventID;
                    }
                    return noteGO;
                case NoteType.LaneSwitch:
                    if(!ignoresStateChanges && levelEditor.CurrentEditorState != LevelEditorState.HoldNotePlacement) levelEditor.IsPlacingNewLaneSwitchNote();
                    return SpawnSimpleNote(laneSwitchNotePrefab, location, parent);
            }

            return null;
        }

        private GameObject SpawnSimpleNote(GameObject prefab, Vector2 location, Transform parent) {
            GameObject note = Instantiate(prefab, parent);
            note.transform.position = location;
            return note;
        }
    }
}
