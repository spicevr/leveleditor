using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Author: Kevin Andersch
    /// Date: 11.04.22
    /// Version: 1.0
    /// </summary>
    public struct MarkerIndex {
        public int NormalMarkerIndex;
        public int? MiniMarkerIndex;

        public static bool operator ==(MarkerIndex left, MarkerIndex right) {
            return left.NormalMarkerIndex == right.NormalMarkerIndex && left.MiniMarkerIndex == right.MiniMarkerIndex;
        }

        public static bool operator !=(MarkerIndex left, MarkerIndex right) {
            return !(left == right);
        }
    }
}
