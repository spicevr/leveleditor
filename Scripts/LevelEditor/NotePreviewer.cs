using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Author: Kevin Andersch
    /// Date: 09.05.22
    /// Version: 1.0
    /// </summary>
    public class NotePreviewer : MonoBehaviour {
        [SerializeField] private LevelEditor levelEditor;
        [SerializeField] [Range(0f, 1f)] private float previewModelAlpha;
        [SerializeField] private Color invalidPlacementColor;
        [SerializeField] private Toggle previewToggle;

        private RectTransform previewNote;
        private Image previewNoteIcon;
        private Color initialColor;
        private bool canShowPreview;
        private NoteType? savedPreviewType = null;

        private void OnEnable() {
            LevelEditor.OnSettingsWereLoaded += LoadSettings;
            LevelEditor.OnBeforeSettingsWereSaved += SaveSettings;
            LevelEditor.OnBeganEditingHoldNote += OnBeginHoldNoteEdit;
            LevelEditor.OnStoppedEditingHoldNote += OnEndHoldNoteEdit;
            PlaybackManager.OnPlaybackBegan += OnPlaybackBegan;
            PlaybackManager.OnPlaybackEnded += OnPlaybackEnded;
        }

        private void OnDisable() {
            LevelEditor.OnLaneUnderPointerChanged -= UpdatePreviewPositionOnLaneUnderPointerChanged;
            BeatMarkerManager.OnLateMarkerUnderCursorChanged -= UpdatePreviewPositionOnMarkerUnderCursorChanged;
            LevelEditor.OnSettingsWereLoaded -= LoadSettings;
            LevelEditor.OnBeforeSettingsWereSaved -= SaveSettings;
            LevelEditor.OnBeganEditingHoldNote -= OnBeginHoldNoteEdit;
            LevelEditor.OnStoppedEditingHoldNote -= OnEndHoldNoteEdit;
            PlaybackManager.OnPlaybackBegan -= OnPlaybackBegan;
            PlaybackManager.OnPlaybackEnded -= OnPlaybackEnded;
        }

        /// <summary>
        /// Starts showing a preview under the pointed at beatIndex of the passed in noteType.
        /// </summary>
        /// <param name="noteType">The type of note of the preview.</param>
        /// <param name="calledByToggle">Whether this method was called by the UI toggle.</param>
        public void ShowPreview(NoteType noteType, bool calledByToggle = false) {
            savedPreviewType = noteType;
            if(!calledByToggle && !canShowPreview) return;
            if(previewNote != null) Destroy(previewNote.gameObject);

            float y = levelEditor.BeatMarkerManager.GetBeatMarkerHeightFromMarkerIndex(levelEditor.BeatMarkerUnderMouse);
            Vector2 location = levelEditor.LaneUnderPointer != null
                ? new Vector2(levelEditor.LaneUnderPointer.transform.position.x, y)
                : new Vector2(-1000, -1000);
            EditorNote convertedNote = new EditorNote { Type = noteType};
            GameObject note = NoteSpawner.Instance.SpawnNote(convertedNote, location, levelEditor.NoteHolder, true);
            if(noteType == NoteType.Hold || noteType == NoteType.LaneSwitch) note.GetComponent<HoldNoteVisuals>().enabled = false;
            Image i = note.GetComponentInChildren<Image>();
            Color c = i.color;
            c.a = previewModelAlpha;
            i.color = c;
            invalidPlacementColor.a = previewModelAlpha;
            previewNote = note.GetComponent<RectTransform>();
            previewNoteIcon = i;
            initialColor = i.color;

            LevelEditor.OnLaneUnderPointerChanged += UpdatePreviewPositionOnLaneUnderPointerChanged;
            BeatMarkerManager.OnLateMarkerUnderCursorChanged += UpdatePreviewPositionOnMarkerUnderCursorChanged;
        }

        /// <summary>
        /// Stops showing the preview.
        /// </summary>
        /// <param name="isInteralCall">Whether this method is called by this class.</param>
        public void StopPreview(bool isInteralCall = false) {
            if(previewNote) Destroy(previewNote.gameObject);
            previewNote = null;
            if(!isInteralCall) savedPreviewType = null;

            LevelEditor.OnLaneUnderPointerChanged -= UpdatePreviewPositionOnLaneUnderPointerChanged;
            BeatMarkerManager.OnLateMarkerUnderCursorChanged -= UpdatePreviewPositionOnMarkerUnderCursorChanged;
        }

        /// <summary>
        /// Toggles whether the preview note will be shown or not.
        /// </summary>
        public void ToggleCanShowPreview() {
            if(!canShowPreview && savedPreviewType != null) ShowPreview((NoteType)savedPreviewType, true);
            else if(canShowPreview) StopPreview(true);

            canShowPreview = !canShowPreview;
        }

        private void UpdatePreviewPositionOnMarkerUnderCursorChanged(int beatIndex) {
            if(!canShowPreview) return;
            float y = levelEditor.BeatMarkerManager.GetBeatMarkerHeightFromBeatIndex(beatIndex, levelEditor.BeatMarkerManager.ShowMiniBeatMarkers); ;
            Lane l = levelEditor.LaneManager.GetLaneAtBeatIndex(beatIndex, Input.mousePosition.x);
            if(l == null) {
                previewNote.position = new Vector2(-999, -999);
                return;
            }

            float x = l.transform.position.x;
            Vector2 location = new Vector2(x, y);
            previewNote.position = location;
            bool isValid = levelEditor.CurrentEditorState == LevelEditorState.HoldNotePlacement ? HoldNoteEditor.Instance.IsValidPlacement(beatIndex)
                : levelEditor.IsCurrentPointerPositionValidNotePosition(beatIndex);
            UpdateColor(isValid);
        }

        private void UpdatePreviewPositionOnLaneUnderPointerChanged(Lane lane) {
            if(!canShowPreview || lane == null) return;
            int beatIndex = levelEditor.MarkerUnderPointerBeatIndex;
            float y = levelEditor.BeatMarkerManager.GetBeatMarkerHeightFromBeatIndex(beatIndex, levelEditor.BeatMarkerManager.ShowMiniBeatMarkers);
            Vector2 location = new Vector2(lane.transform.position.x, y);
            previewNote.position = location;
            bool isValid = levelEditor.CurrentEditorState == LevelEditorState.HoldNotePlacement ? HoldNoteEditor.Instance.IsValidPlacement(beatIndex)
                : levelEditor.IsCurrentPointerPositionValidNotePosition(beatIndex);
            UpdateColor(isValid);
        }

        private void UpdateColor(bool isValid) {
            if(isValid) previewNoteIcon.color = initialColor;
            else previewNoteIcon.color = invalidPlacementColor;
        }

        private void LoadSettings() {
            previewToggle.isOn = !levelEditor.Settings.ShowNotePreview;
            canShowPreview = levelEditor.Settings.ShowNotePreview;
        }

        private void SaveSettings() {
            levelEditor.Settings.ShowNotePreview = canShowPreview;
        }

        private void OnBeginHoldNoteEdit(int beatIndex) {
            ShowPreview(((EditorNote)levelEditor.GetNote(beatIndex, true)).Type);
        }

        private void OnEndHoldNoteEdit() {
            ShowPreview(levelEditor.NoteTypeSelector.ActiveNoteType);
        }

        private void OnPlaybackBegan() {
            StopPreview(true);
        }

        private void OnPlaybackEnded() {
            ShowPreview((NoteType)savedPreviewType);
        }
    }
}
