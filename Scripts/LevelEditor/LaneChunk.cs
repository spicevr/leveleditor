using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Author: Kevin Andersch
    /// Date: 04.04.22
    /// Version: 1.0
    /// </summary>
    public class LaneChunk : MonoBehaviour {
        [SerializeField] private RectTransform rectTransform;
        [SerializeField] private float positionOffsetPercentage;

        public RectTransform RectTransform { get => rectTransform; }

        public int StartBeatIndex { get; private set; }
        public int EndBeatIndex { get; private set; }
        public int LaneCount { get => lanes.Count; }

        private List<Lane> lanes;
        LevelEditor levelEditor;

        /// <summary>
        /// Initializes this lane chunk with all required values and references.
        /// Should be called right after instantiating it.
        /// </summary>
        /// <param name="numberOfLanes">The number of lanes this chunk has.</param>
        /// <param name="lanePrefab">The lane prefab reference.</param>
        /// <param name="levelEditorReference">The level editor of the scene.</param>
        /// <param name="startBeatIndex">The starting beat index where this chunk begins.</param>
        /// <param name="endBeatIndex">The ending beat index where this chunk ends.</param>
        public void Initialize(int numberOfLanes, GameObject lanePrefab, LevelEditor levelEditorReference, int startBeatIndex, int endBeatIndex) {
            lanes = new List<Lane>();
            levelEditor = levelEditorReference;
            this.StartBeatIndex = startBeatIndex;
            this.EndBeatIndex = endBeatIndex;
            for(int i = 0; i < numberOfLanes; i++) {
                Lane lane = Instantiate(lanePrefab, transform).GetComponent<Lane>();
                lane.Initialize(levelEditorReference, this, i, false);
                lanes.Add(lane);
            }

            Rescale();
        }

        /// <summary>
        /// Initializes this lane chunk with all required values and references as a temporary
        /// lane chunk with inactive lanes that will get deleted once this chunk becomes permanent.
        /// Should be called right after instantiating it.
        /// </summary>
        /// <param name="temporaryLaneCount">The number of total lanes this chunk has.</param>
        /// <param name="actualLaneCount">The number of lanes this chunk has after becoming permanent.</param>
        /// <param name="lanePrefab">The lane prefab reference.</param>
        /// <param name="levelEditorReference">The level editor of the scene.</param>
        /// <param name="startBeatIndex">The starting beat index where this chunk begins.</param>
        /// <param name="endBeatIndex">The ending beat index where this chunk ends.</param>
        public void Initialize(int temporaryLaneCount, int actualLaneCount, GameObject lanePrefab, LevelEditor levelEditorReference, int startBeatIndex, int endBeatIndex) {
            lanes = new List<Lane>();
            levelEditor = levelEditorReference;
            this.StartBeatIndex = startBeatIndex;
            this.EndBeatIndex = endBeatIndex;

            int totalLanes = Mathf.Max(temporaryLaneCount, actualLaneCount);
            for(int i = 0; i < totalLanes; i++) {
                Lane lane = Instantiate(lanePrefab, transform).GetComponent<Lane>();
                lane.Initialize(levelEditorReference, this, i, (i + 1) > actualLaneCount || (i + 1) > temporaryLaneCount, !((i + 1) > temporaryLaneCount));
                lanes.Add(lane);
            }

            Rescale();
        }

        /// <summary>
        /// Redirects the movement call to all lanes that are part of this chunk.
        /// </summary>
        /// <param name="moveForward">Whether to move downwards (true) or upwards (false).</param>
        public void MoveLaneNotes(bool moveForward) {
            for(int i = 0; i < lanes.Count; i++) {
                lanes[i].MoveNotes(moveForward);
            }
        }

        /// <summary>
        /// Destroys all temporary lanes.
        /// Returns true if the number of lanes changed (aka the width of this chunk
        /// needs to be rescaled).
        /// </summary>
        public bool DestroyTemporaryLanes() {
            List<Lane> toRemove = new List<Lane>();
            foreach(Lane lane in lanes) {
                if(lane.IsTemporaryLane) {
                    if(lane.GetsDeletedOnBecomingPermanent) toRemove.Add(lane);
                    else lane.ChangeToPermanent();
                }
            }

            foreach(Lane lane in toRemove) {
                lanes.Remove(lane);
                Destroy(lane.gameObject);
            }

            return toRemove.Count > 0;
        }

        /// <summary>
        /// Resizes and repositions this lane chunk according to the currently given view of the level in the editor.
        /// </summary>
        public void Rescale() {
            float startHeight = levelEditor.BeatMarkerManager.GetBeatMarkerAnchoredHeightFromBeatIndex(StartBeatIndex, true);
            float endHeight = levelEditor.BeatMarkerManager.GetBeatMarkerAnchoredHeightFromBeatIndex(EndBeatIndex, true);
            rectTransform.anchoredPosition = new Vector2(rectTransform.anchoredPosition.x, startHeight);
            rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x, endHeight - startHeight);
        }

        /// <summary>
        /// Resizes and repositions this lane chunk according to the currently given view of the level in the editor.
        /// </summary>
        public void RescaleWithOffset() {
            float startHeight = levelEditor.BeatMarkerManager.GetPlaybackMarkerAnchoredPositionFromBeatIndex(StartBeatIndex);
            float endHeight = levelEditor.BeatMarkerManager.GetPlaybackMarkerAnchoredPositionFromBeatIndex(EndBeatIndex);
            rectTransform.anchoredPosition = new Vector2(rectTransform.anchoredPosition.x, startHeight);
            rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x, endHeight - startHeight);
        }

        /// <summary>
        /// Returns true if this chunk now lies outside the visible area of the editor.
        /// </summary>
        public bool CheckIfNeedsToDespawn() {
            if(EndBeatIndex <= levelEditor.BeatMarkerManager.BottomMarkerBeatIndex) return true;
            if(StartBeatIndex >= levelEditor.BeatMarkerManager.TopMarkerBeatIndex) return true;

            return false;
        }

        /// <summary>
        /// Updates the endBeatIndex to its correct value (as the dictionary of levelEditor might have changed).
        /// </summary>
        public void UpdateData() {
            int? result = levelEditor.GetLaneChunkEndBeatIndex(StartBeatIndex);
            if(result != null) EndBeatIndex = (int)result;
            else {
                levelEditor.LaneManager.DeleteLaneChunk(this, true);
            }
        }

        /// <summary>
        /// Sets this lane chunk to be a temporary one, changing its display based on
        /// the passed in values.
        /// </summary>
        /// <param name="temporaryLaneCount">The amount of lanes in its temporary state.</param>
        public void SetToTemporary(int temporaryLaneCount, GameObject lanePrefab) {
            int laneCount = lanes.Count;
            if(temporaryLaneCount > laneCount) {
                int lanesNeeded = temporaryLaneCount - laneCount;
                for(int i = 0; i < lanesNeeded; i++) {
                    Lane lane = Instantiate(lanePrefab, transform).GetComponent<Lane>();
                    lane.Initialize(levelEditor, this, laneCount + i, true);
                    lanes.Add(lane);
                }
                levelEditor.LaneManager.ResizeChunk(this);
            } else if(temporaryLaneCount < laneCount) {
                for(int i = temporaryLaneCount; i < lanes.Count; i++) {
                    lanes[i].ChangeToTemporary();
                }
            }
        }

        /// <summary>
        /// Returns the lane at the passed in index.
        /// Returns null if the index is out of bounds.
        /// </summary>
        /// <param name="index">The index of the desired lane.</param>
        public Lane GetLane(int index, bool showErrorMessageIfOutOfBounds = false) {
            if(index < 0 || index > lanes.Count - 1) {
                if(showErrorMessageIfOutOfBounds) Debug.LogError($"Tried to get lane at index {index}, but this chunk only has {lanes.Count} lanes!");
                return null;
            }

            return lanes[index];
        }

        /// <summary>
        /// Returns the lane that is closest on the x-axis to the passed in position;
        /// </summary>
        /// <param name="xPosition">The x-position from which to get the closest lane.</param>
        public Lane GetLane(float xPosition) {
            float closestDistance = float.MaxValue;
            Lane closestLane = null;
            foreach(Lane lane in lanes) {
                float dst = Mathf.Abs(lane.transform.position.x - xPosition);
                if(dst < closestDistance) {
                    closestLane = lane;
                    closestDistance = dst;
                }
            }

            return closestLane;
        }

        /// <summary>
        /// Destroys all visible notes (only visuals, not data).
        /// </summary>
        public void DestroyAllNotes() {
            foreach(Lane lane in lanes) {
                lane.DestroyAllNotes();
            }
        }


        /// <summary>
        /// Decouples all notes of all lanes of this chunk.
        /// </summary>
        public List<RectTransform> DecoupleAllNotes() {
            List<RectTransform> notes = new List<RectTransform>();
            foreach(Lane lane in lanes) {
                notes.AddRange(lane.DecoupleAllNotes());
            }
            return notes;
        }

        /// <summary>
        /// Sets the end of this chunk and rescales it to the new correct size.
        /// Safe to use, has out of bounds checks.
        /// </summary>
        /// <param name="value">The new endBeatIndex</param>
        public void SetEndBeatIndex(int value) {
            value = Mathf.Clamp(value, 0, levelEditor.EndOfSongBeatIndex);
            EndBeatIndex = value;
            Rescale();
        }

        /// <summary>
        /// Returns all notes belonging to this chunk.
        /// </summary>
        public List<LaneNote> GetLaneNotes() {
            List<LaneNote> notes = new List<LaneNote>();
            foreach(Lane lane in lanes) {
                notes.AddRange(lane.GetNotes());
            }
            return notes;
        }

        /// <summary>
        /// Repositions alls notes of this chunk to their correct horizontal position
        /// </summary>
        /// <param name="laneCount"></param>
        public void RecalculateNotePositions() {
            HorizontalLayoutGroup group = GetComponent<HorizontalLayoutGroup>();
            group.enabled = false;
            group.enabled = true;
            Canvas.ForceUpdateCanvases();
            Invoke(nameof(InternalRecalculateNotePositions), Time.fixedDeltaTime);
        }

        private void InternalRecalculateNotePositions() {
            for(int i = 0; i < lanes.Count; i++) {
                lanes[i].RecalculateNotePositions();
            }
        }
    }
}
