using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Author: Kevin Andersch
    /// Date: 31.05.22
    /// Version: 1.0
    /// </summary>
    public class EventNoteVisuals : MonoBehaviour {
        [SerializeField] private TextMeshProUGUI eventText;

        private int storyID = -1;
        private NoteEvent eventType;

        public NoteEvent EventType {
            get => eventType;
            set {
                eventType = value;
                string id = storyID >= 0 ? $" {storyID}" : string.Empty;
                eventText.text = $"{eventType}{id}";
            }
        }

        public int StoryID {
            get => storyID;
            set {
                storyID = value;
                string id = storyID >= 0 ? $" {storyID}" : string.Empty;
                eventText.text = $"{eventType}{id}";
            }
        }
    }
}
