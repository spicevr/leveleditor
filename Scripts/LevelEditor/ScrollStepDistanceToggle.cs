using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Author: Kevin Andersch
    /// Date: 20.06.22
    /// Version: 1.0
    /// </summary>
    public class ScrollStepDistanceToggle : MonoBehaviour, IPointerClickHandler {
        [SerializeField] private LevelEditor levelEditor;
        [SerializeField] private GameObject minorBeatsActivePanel; 
        [SerializeField] private GameObject majorBeatsActivePanel;

        private bool majorBeatsActive;

        private void OnEnable() {
            LevelEditor.OnEditorWasOpened += LoadInitialValues;
        }

        public void OnPointerClick(PointerEventData eventData) {
            majorBeatsActive = !majorBeatsActive;
            levelEditor.Settings.ScrollingStepDistanceIsMajorBeats = majorBeatsActive;
            minorBeatsActivePanel.SetActive(!majorBeatsActive);
            majorBeatsActivePanel.SetActive(majorBeatsActive);
            SetScrollingStepDistance();
        }

        private void LoadInitialValues() {
            majorBeatsActive = levelEditor.Settings.ScrollingStepDistanceIsMajorBeats;
            minorBeatsActivePanel.SetActive(!majorBeatsActive);
            majorBeatsActivePanel.SetActive(majorBeatsActive);
            SetScrollingStepDistance();
            LevelEditor.OnEditorWasOpened -= LoadInitialValues;
        }

        private void SetScrollingStepDistance() {
            levelEditor.SetScrollingStepDistance(majorBeatsActive);
        }
    }
}
