using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(NoteData))]
public class NoteDataPropertyDrawer : PropertyDrawer {
    private const float timeProportion = 0.32f;
    private const float laneProportion = 0.25f;
    private const float noteTypeProportion = 0.43f;
    private const float propertyHorizontalSpacing = 2f;
    private const float propertyVerticalSpacing = 2f;
    private const float expandedArrayHeightOffset = 10f;

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
        float height = EditorGUIUtility.singleLineHeight;
        SerializedProperty noteType = property.FindPropertyRelative(nameof(NoteData.Type));
        if(ArcBreak.LevelEditor.LevelDataUtilities.CheckIfNoteTypeRequiresSpecialData(noteType.enumValueIndex)) {
            SerializedProperty specialData = property.FindPropertyRelative(nameof(NoteData.SpecialNoteData));
            height += propertyVerticalSpacing;
            if(specialData.isExpanded) {
                height += expandedArrayHeightOffset + EditorGUI.GetPropertyHeight(specialData, true);
            } else {
                height += EditorGUIUtility.singleLineHeight;
            }
        }
        return height;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        SerializedProperty beatIndex = property.FindPropertyRelative(nameof(NoteData.BeatIndex));
        SerializedProperty lane = property.FindPropertyRelative(nameof(NoteData.Lane));
        SerializedProperty noteType = property.FindPropertyRelative(nameof(NoteData.Type));
        SerializedProperty specialData = property.FindPropertyRelative(nameof(NoteData.SpecialNoteData));
        EditorGUIUtility.labelWidth *= 0.4f;

        Rect beatIndexPosition = new Rect(position.x, position.y, position.width * timeProportion, EditorGUIUtility.singleLineHeight);
        EditorGUI.BeginProperty(beatIndexPosition, label, beatIndex);
        EditorGUI.PropertyField(beatIndexPosition, beatIndex);
        EditorGUI.EndProperty();

        Rect lanePosition = new Rect(position.x + beatIndexPosition.width + propertyHorizontalSpacing, position.y, position.width * laneProportion, EditorGUIUtility.singleLineHeight);
        EditorGUI.BeginProperty(lanePosition, label, lane);
        EditorGUI.PropertyField(lanePosition, lane);
        EditorGUI.EndProperty();

        Rect noteTypePosition = new Rect(lanePosition.x + lanePosition.width + propertyHorizontalSpacing, position.y, position.width * noteTypeProportion, EditorGUIUtility.singleLineHeight);
        EditorGUI.BeginProperty(noteTypePosition, label, noteType);
        EditorGUI.PropertyField(noteTypePosition, noteType);
        EditorGUI.EndProperty();

        if(ArcBreak.LevelEditor.LevelDataUtilities.CheckIfNoteTypeRequiresSpecialData(noteType.enumValueIndex)) {
            Rect specialDataPosition = new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight + propertyVerticalSpacing,
                position.width, EditorGUIUtility.singleLineHeight);
            EditorGUI.BeginProperty(specialDataPosition, label, specialData);
            EditorGUI.PropertyField(specialDataPosition, specialData);
            EditorGUI.EndProperty();
        }
    }
}
