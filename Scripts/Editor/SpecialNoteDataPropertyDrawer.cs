using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/// <summary>
/// Author: Kevin Andersch
/// Date: 31.03.22
/// Version: 1.0
/// </summary>
[CustomPropertyDrawer(typeof(SpecialNoteData))]
public class SpecialNoteDataPropertyDrawer : PropertyDrawer {
    private const float beatIndexProportion = 0.5f;
    private const float laneProportion = 0.5f;
    private const float eventTypeProportion = 0.5f;
    private const float eventIDProportion = 0.5f;
    private const float propertyHorizontalSpacing = 4f;
    private const float propertyVerticalSpacing = 2f;

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
        SerializedProperty eventType = property.FindPropertyRelative(nameof(SpecialNoteData.EventType));
        NoteEvent noteEvent = (NoteEvent)System.Enum.GetValues(typeof(NoteEvent)).GetValue(eventType.enumValueIndex);

        float heightMultiplier = noteEvent == NoteEvent.None ? 2f : 2f;
        float heightAdditive = noteEvent == NoteEvent.None ? propertyVerticalSpacing : 0f;
        return EditorGUIUtility.singleLineHeight * heightMultiplier + heightAdditive;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        SerializedProperty eventType = property.FindPropertyRelative(nameof(SpecialNoteData.EventType));
        NoteEvent noteEvent = (NoteEvent)System.Enum.GetValues(typeof(NoteEvent)).GetValue(eventType.enumValueIndex);
        //EditorGUIUtility.labelWidth *= 0.8f;

        if(noteEvent != NoteEvent.None) {
            //EditorGUIUtility.labelWidth *= 2f;
            Rect eventTypePosition = new Rect(position.x, position.y,
                position.width * eventTypeProportion, EditorGUIUtility.singleLineHeight);
            EditorGUI.BeginProperty(eventTypePosition, label, eventType);
            EditorGUI.PropertyField(eventTypePosition, eventType);
            EditorGUI.EndProperty();

            if(noteEvent == NoteEvent.Story) {
                SerializedProperty storyID = property.FindPropertyRelative(nameof(SpecialNoteData.StoryEventID));

                Rect storyIDPosition = new Rect(position.x + eventTypePosition.width + propertyHorizontalSpacing, position.y,
                (position.width * eventIDProportion) - propertyHorizontalSpacing, EditorGUIUtility.singleLineHeight);
                EditorGUI.BeginProperty(storyIDPosition, label, storyID);
                EditorGUI.PropertyField(storyIDPosition, storyID);
                EditorGUI.EndProperty();
            }
        } else {
            SerializedProperty beatIndex = property.FindPropertyRelative(nameof(SpecialNoteData.BeatIndex));
            SerializedProperty lane = property.FindPropertyRelative(nameof(SpecialNoteData.Lane));
            SerializedProperty laneSwitchCount = property.FindPropertyRelative(nameof(SpecialNoteData.LaneSwitchCount));

            Rect beatIndexPosition = new Rect(position.x, position.y, position.width * beatIndexProportion, EditorGUIUtility.singleLineHeight);
            EditorGUI.BeginProperty(beatIndexPosition, label, beatIndex);
            EditorGUI.PropertyField(beatIndexPosition, beatIndex);
            EditorGUI.EndProperty();

            Rect lanePosition = new Rect(position.x + beatIndexPosition.width + propertyHorizontalSpacing, position.y, (position.width * laneProportion) - propertyHorizontalSpacing, EditorGUIUtility.singleLineHeight);
            EditorGUI.BeginProperty(lanePosition, label, lane);
            EditorGUI.PropertyField(lanePosition, lane);
            EditorGUI.EndProperty();

            Rect laneSwitchCountPosition = new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight + propertyVerticalSpacing,
                (position.width * laneProportion), EditorGUIUtility.singleLineHeight);
            EditorGUI.BeginProperty(laneSwitchCountPosition, label, laneSwitchCount);
            EditorGUI.PropertyField(laneSwitchCountPosition, laneSwitchCount);
            EditorGUI.EndProperty();
        }
    }
}
