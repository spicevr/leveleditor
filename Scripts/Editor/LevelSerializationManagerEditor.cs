using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

/// <summary>
/// Author: Kevin Andersch
/// Date: 28.03.22
/// Version: 1.0
/// </summary>
[CustomEditor(typeof(LevelSerializationManager))]
public class LevelSerializationManagerEditor : Editor {

    private void OnEnable() {
        LevelSerializationManager manager = (LevelSerializationManager)target;
        manager.LoadLevel();
    }

    public override void OnInspectorGUI() {
        SerializedProperty popupUtility = serializedObject.FindProperty("popupUtility");
        SerializedProperty level = serializedObject.FindProperty("levelAsset");
        SerializedProperty levelData = serializedObject.FindProperty("level");
        Object levelAsset = level.objectReferenceValue;

        LevelSerializationManager manager = (LevelSerializationManager)target;

        EditorGUILayout.PropertyField(popupUtility);

        EditorGUI.BeginChangeCheck();
        EditorGUILayout.PropertyField(level);
        if(EditorGUI.EndChangeCheck()) {
            if(levelAsset != level.objectReferenceValue) {
                if(levelAsset != null && manager.CheckIfChangesWereMade(levelAsset)) {
                    int selected = EditorUtility.DisplayDialogComplex("Level Change", "You have unsaved changes on the current level." +
                        "\nDo you want to save them first?", "Yes", "No", "Cancel");

                    if(selected == 0) {
                        // yes
                        if(manager.CheckIfDifferentFileWithSameNameAlreadyExists()) {
                            if(EditorUtility.DisplayDialog("Overwrite Level", "WARNING!\nA level with this name already exists!" +
                                "\nWould you like to overwrite it (this cannot be reverted)?", "Yes", "No")) {
                                manager.SaveLevel();
                                LoadLevelDataIntoInspector(levelData, level.objectReferenceValue);
                            } else {
                                EditorUtility.DisplayDialog("Action Aborted", "The level was not overwritten and the current level asset has not been changed.", "Ok");
                                level.objectReferenceValue = levelAsset;
                            }
                        } else {
                            manager.SaveLevel();
                            LoadLevelDataIntoInspector(levelData, level.objectReferenceValue);
                        }
                    } else if(selected == 1) {
                        // no
                        LoadLevelDataIntoInspector(levelData, level.objectReferenceValue);
                        LevelSerializationManager.DeleteTemporaryFile();
                    } else {
                        // cancel
                        level.objectReferenceValue = levelAsset;
                    }
                } else {
                    LoadLevelDataIntoInspector(levelData, level.objectReferenceValue);
                    LevelSerializationManager.DeleteTemporaryFile();
                }
            }
        }

        if(level.objectReferenceValue == null) return;
        EditorGUILayout.PropertyField(levelData, true);

        if(GUILayout.Button("Save Level")) {
            if(manager.CheckIfDifferentFileWithSameNameAlreadyExists()) {
                if(EditorUtility.DisplayDialog("Overwrite Level", "WARNING!\nA level with this name already exists!" +
                    "\nWould you like to overwrite it (this cannot be reverted)?", "Yes", "No")) {
                    manager.SaveLevel();
                }
            } else {
                manager.SaveLevel();
            }
        }

        if(GUILayout.Button("Create New Level")) {
            if(manager.CheckIfChangesWereMade(null)) {
                int selected = EditorUtility.DisplayDialogComplex("Discard Changes?", "Do you wish to keep the values of the current level and copy them into the new file?", "Copy Them", "Create From Scratch", "Cancel");
                if(selected == 0) {
                    manager.CreateNewLevel(false);
                } else if(selected == 1) {
                    manager.CreateNewLevel(true);
                }
            } else {
                manager.CreateNewLevel(true);
            }
        }

        serializedObject.ApplyModifiedProperties();
    }

    /// <summary>
    /// Deserializes the passed in asset into level data and sets all level data of the
    /// serialized object of this inspector to the corresponding values.
    /// </summary>
    /// <param name="levelDataProperty">The level data of the serialized object.</param>
    /// <param name="levelAsset">The .level file holding the data to deserialize.</param>
    public void LoadLevelDataIntoInspector(SerializedProperty levelDataProperty, Object levelAsset) {
        if(!levelAsset) return;

        LevelData data = null;
        string filePath = LevelSerializationManager.GetFinishedAssetPath(levelAsset);
        if(File.Exists(filePath)) {
            string fileContent = File.ReadAllText(filePath);
            data = JsonUtility.FromJson<LevelData>(fileContent);
        };

        if(data == null) return;

        SerializedProperty levelName = levelDataProperty.FindPropertyRelative("Name");
        SerializedProperty levelDifficulty = levelDataProperty.FindPropertyRelative("Difficulty");
        SerializedProperty levelNotes = levelDataProperty.FindPropertyRelative("Notes");

        levelName.stringValue = data.Name;
        levelDifficulty.enumValueIndex = System.Array.IndexOf(System.Enum.GetValues(typeof(LevelDifficulty)), data.Difficulty);
        levelNotes.ClearArray();
        levelNotes.arraySize = data.Notes.Length;
        for(int i = 0; i < data.Notes.Length; i++) {
            FillInNoteData(levelNotes.GetArrayElementAtIndex(i), data.Notes[i], data, i);
        }
    }

    /// <summary>
    /// Fills the data of a serialized note with the passed in values.
    /// </summary>
    /// <param name="noteArrayProperty">The serialized note.</param>
    /// <param name="noteData">The passed in values.</param>
    public void FillInNoteData(SerializedProperty noteArrayProperty, NoteData noteData, LevelData levelData, int index) {
        SerializedProperty time = noteArrayProperty.FindPropertyRelative(nameof(NoteData.BeatIndex));
        SerializedProperty lane = noteArrayProperty.FindPropertyRelative(nameof(NoteData.Lane));
        SerializedProperty noteType = noteArrayProperty.FindPropertyRelative(nameof(NoteData.Type));
        SerializedProperty specialNoteData = noteArrayProperty.FindPropertyRelative(nameof(NoteData.SpecialNoteData));

        time.intValue = noteData.BeatIndex;
        lane.intValue = noteData.Lane;
        noteType.enumValueIndex = (int)noteData.Type;
        NoteType typeOfNote = (NoteType)System.Enum.GetValues(typeof(NoteType)).GetValue(noteType.enumValueIndex);
        if(typeOfNote != NoteType.LaneSwitch && typeOfNote != NoteType.Hold && typeOfNote != NoteType.Event) return;

        SpecialNoteData[] data = levelData.Notes[index].SpecialNoteData;
        specialNoteData.ClearArray();
        specialNoteData.arraySize = data.Length;
        for(int i = 0; i < data.Length; i++) {
            FillInSpecialNoteData(specialNoteData.GetArrayElementAtIndex(i), data[i], typeOfNote == NoteType.Event);
        }
    }

    /// <summary>
    /// Fills the data of a serilized special note with the passed in data.
    /// </summary>
    /// <param name="property">The serialized special note.</param>
    /// <param name="data">The data of the special note.</param>
    /// <param name="isEventNote">Whether this is an event note or a hold / lane switch note.</param>
    public void FillInSpecialNoteData(SerializedProperty property, SpecialNoteData data, bool isEventNote) {
        if(isEventNote) {
            SerializedProperty eventType = property.FindPropertyRelative(nameof(SpecialNoteData.EventType));
            SerializedProperty storyID = property.FindPropertyRelative(nameof(SpecialNoteData.StoryEventID));

            eventType.enumValueIndex = (int)data.EventType;
            storyID.intValue = data.StoryEventID;
        } else {
            SerializedProperty beatIndex = property.FindPropertyRelative(nameof(SpecialNoteData.BeatIndex));
            SerializedProperty lane = property.FindPropertyRelative(nameof(SpecialNoteData.Lane));
            SerializedProperty laneSwitchCount = property.FindPropertyRelative(nameof(SpecialNoteData.LaneSwitchCount));

            beatIndex.intValue = data.BeatIndex;
            lane.intValue = data.Lane;
            laneSwitchCount.intValue = data.LaneSwitchCount;
        }
    }
}
