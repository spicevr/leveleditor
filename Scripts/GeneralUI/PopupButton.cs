using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Author: Kevin Andersch
    /// Date: 31.03.22
    /// Version: 1.0
    /// </summary>
    public class PopupButton : MonoBehaviour {
        [Header("Reference Setup")]
        [SerializeField] private TextMeshProUGUI buttonText;
        [SerializeField] private Button button;

        public void Initialize(string text, System.Action<int> onClickAction, int onClickValue) {
            this.buttonText.text = text;
            button.onClick.AddListener(delegate { onClickAction?.Invoke(onClickValue); });
        }
    }
}
