using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Author: Kevin Andersch
    /// Date: 26.04.22
    /// Version: 1.0
    /// </summary>
    public class BPMAdjuster : MonoBehaviour {
        [Header("References")]
        [SerializeField] private GameObject bpmAdjustmentPanel;
        [SerializeField] private TMP_InputField inputField;
        [SerializeField] private Slider slider;
        [SerializeField] private ToggleButtonWithVolume indicatorButton;
        [SerializeField] private ToggleButtonWithVolume backgroundMusicButton;
        [SerializeField] private LevelEditor levelEditor;
        [SerializeField] private Slider timeScaleSlider;

        [Header("BPM Setup")]
        [SerializeField] private int minBPM;
        [SerializeField] private int maxBPM;

        private System.Action<bool, int> onCompleteAction;
        private AudioSource indicatorSource;
        private int currentBPM;
        private Coroutine indicatorRoutine;

        private void Start() {
            indicatorSource = GetComponent<AudioSource>();
            slider.minValue = minBPM;
            slider.maxValue = maxBPM;
            indicatorButton.OnVolumeChanged += OnIndicatorVolumeChanged;
            backgroundMusicButton.OnVolumeChanged += OnBackgroundMusicVolumeChanged;
        }

        private void OnDisable() {
            indicatorButton.OnVolumeChanged -= OnIndicatorVolumeChanged;
            backgroundMusicButton.OnVolumeChanged -= OnBackgroundMusicVolumeChanged;
        }

        /// <summary>
        /// Shows the bpm adjustment panel so the user can change the bpm values.
        /// </summary>
        /// <param name="initialValue">The initial value of the bpm. This is clamped by min and max BPM values.</param>
        /// <param name="onCompleteAction">Called with the new bpm value and
        /// true if the user wants to accept the changes and false if the user wants to discard them.</param>
        public void ShowAdjustmentPanel(int initialValue, System.Action<bool, int> onCompleteAction = null) {
            if(initialValue < minBPM) initialValue = minBPM;
            if(initialValue > maxBPM) initialValue = maxBPM;

            this.onCompleteAction = onCompleteAction;
            currentBPM = initialValue;
            inputField.text = initialValue.ToString();
            slider.value = initialValue;
            timeScaleSlider.value = 1f;

            indicatorButton.Initialize(levelEditor.Settings.BPMAdjustmentIndicatorVolume);
            backgroundMusicButton.Initialize(levelEditor.Settings.BPMAdjustmentBackgroundAudioVolume);
            LevelEditorMusicPlayer.Instance.PlayBackgroundMusic();
            indicatorRoutine = StartCoroutine(PlayIndicatorSounds());

            bpmAdjustmentPanel.SetActive(true);
        }

        private void CloseAdjustmentPanel(bool saveChanges) {
            SaveVolumeToSettings();
            StopCoroutine(indicatorRoutine);
            LevelEditorMusicPlayer.Instance.StopBackgroundMusic();
            int value = int.Parse(inputField.text);
            onCompleteAction?.Invoke(saveChanges, value);
            bpmAdjustmentPanel.SetActive(false);
            LevelEditorMusicPlayer.Instance.SetPitch(1f);
        }

        private void SaveVolumeToSettings() {
            levelEditor.Settings.BPMAdjustmentIndicatorVolume = indicatorButton.Volume;
            levelEditor.Settings.BPMAdjustmentBackgroundAudioVolume = backgroundMusicButton.Volume;
        }

        private IEnumerator PlayIndicatorSounds() {
            float nextBeatTime = 0f;
            int currentBeat = 0;
            int ownBPM = currentBPM;
            while(true) {
                if(LevelEditorMusicPlayer.Instance.GetCurrentSongTime() >= nextBeatTime) {
                    indicatorSource.Play();
                    currentBeat++;
                    nextBeatTime = LevelDataUtilities.GetTimeOfMajorBeat(currentBeat, ownBPM);
                }

                yield return null;

                if(ownBPM != currentBPM) {
                    float songTime = LevelEditorMusicPlayer.Instance.GetCurrentSongTime();
                    currentBeat = (int)(songTime / LevelDataUtilities.GetBPMFrequency(currentBPM));
                    currentBeat++;
                    nextBeatTime = LevelDataUtilities.GetTimeOfMajorBeat(currentBeat, currentBPM);
                    ownBPM = currentBPM;
                }
            }
        }

        #region Callbacks
        // These are callbacks and should not be called on their own

        /// <summary>
        /// Input field value changed callback.
        /// </summary>
        public void OnInputFieldValueChanged() {
            if(!int.TryParse(inputField.text, out int value)) return;

            if(value > maxBPM) {
                value = maxBPM;
                inputField.text = value.ToString();
            } else if(value < minBPM) {
                value = minBPM;
                inputField.text = value.ToString();
            }

            slider.value = value;
            currentBPM = value;
        }

        /// <summary>
        /// Time Scale Slider value changed callback.
        /// </summary>
        public void OnTimeScaleSliderValueChanged() {
            LevelEditorMusicPlayer.Instance.SetPitch(timeScaleSlider.value);
        }

        /// <summary>
        /// Slider value changed callback.
        /// </summary>
        private void OnSliderValueChanged() {
            inputField.text = slider.value.ToString();
            currentBPM = (int)slider.value;
        }

        /// <summary>
        /// Increment button onClick callback.
        /// </summary>
        private void OnIncrementClick() {
            int value = int.Parse(inputField.text);
            if(value < maxBPM) value++;
            inputField.text = value.ToString();
            slider.value = value;
        }

        /// <summary>
        /// Decrement button onClick callback.
        /// </summary>
        private void OnDecrementClick() {
            int value = int.Parse(inputField.text);
            if(value > minBPM) value--;
            inputField.text = value.ToString();
            slider.value = value;
        }

        /// <summary>
        /// Save button onClick callback.
        /// </summary>
        private void OnSaveClick() {
            CloseAdjustmentPanel(true);
        }

        /// <summary>
        /// Discard button onClick callback.
        /// </summary>
        private void OnDiscardClick() {
            CloseAdjustmentPanel(false);
        }

        /// <summary>
        /// Indicator volume changed callback.
        /// </summary>
        private void OnIndicatorVolumeChanged(float volume) {
            indicatorSource.volume = volume;
        }

        /// <summary>
        /// Background music volume changed callback.
        /// </summary>
        private void OnBackgroundMusicVolumeChanged(float volume) {
            LevelEditorMusicPlayer.Instance.SetBackgroundMusicVolume(volume);
        }
        #endregion
    }
}
