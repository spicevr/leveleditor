using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using UnityEngine.UI;

namespace ArcBreak.LevelEditor {
    /// <summary>
    /// Author: Kevin Andersch
    /// Date: 26.04.22
    /// Version: 1.0
    /// </summary>
    public class ToggleButtonWithVolume : MonoBehaviour {
        [Header("References")]
        [SerializeField] private TextMeshProUGUI volumeText;
        [SerializeField] private Image[] sliderComponents;
        [SerializeField] private Slider volumeSlider;
        [SerializeField] private GameObject volumePanel;
        [SerializeField] private GameObject enabledIcon;
        [SerializeField] private GameObject disabledIcon;

        [Header("Setup")]
        [SerializeField] private float fadeInDuration;
        [SerializeField] private float fadeOutDuration;
        [SerializeField] private float mouseScrollVolumeChangeAmount;

        public static bool IsCursorOverAnyVolumeToggle { get; private set; }

        public float Volume {
            get => volume;
            private set {
                float newValue = Mathf.Clamp(value, 0f, 1f);
                volume = newValue;
                volumeSlider.value = volume;
                OnVolumeChanged?.Invoke(volume);
            }
        }

        private float volume;
        private float volumeBeforeMute = 0.1f;

        private Coroutine routine;
        private bool routineRunning;

        private bool isActive;

        // Events
        /// <summary>
        /// 0: The new value for the volume.
        /// </summary>
        public System.Action<float> OnVolumeChanged;

        private void Update() {
            if(!isActive) return;

            float scrollY = Input.mouseScrollDelta.y;
            if(scrollY > 0f) {
                if(Volume <= 0.01) {
                    enabledIcon.SetActive(true);
                    disabledIcon.SetActive(false);
                }
                Volume += mouseScrollVolumeChangeAmount;
            } else if(scrollY < 0f) {
                Volume -= mouseScrollVolumeChangeAmount;
                if(Volume <= 0.01f) {
                    enabledIcon.SetActive(false);
                    disabledIcon.SetActive(true);
                    volumeBeforeMute = 0.1f;
                }
            }
        }

        /// <summary>
        /// Initialize this component with all necessary values.
        /// </summary>
        /// <param name="volume">The volume for the associated audio source to have.</param>
        public void Initialize(float volume) {
            this.Volume = volume;
            volumeSlider.value = volume;

            enabledIcon.SetActive(volume > 0f);
            disabledIcon.SetActive(volume <= 0f);

            Color col = volumeText.color;
            col.a = 0f;
            volumeText.color = col;
            for(int i = 0; i < sliderComponents.Length; i++) {
                col = sliderComponents[i].color;
                col.a = 0f;
                sliderComponents[i].color = col;
            }
        }

        /// <summary>
        /// OnPointerEnter callback.
        /// Shows the volume slider.
        /// </summary>
        public void PointerEnter() {
            if(routineRunning) StopCoroutine(routine);
            routine = StartCoroutine(FadeIn(fadeInDuration));
            isActive = true;
            IsCursorOverAnyVolumeToggle = true;
        }

        /// <summary>
        /// OnPointerExit callback.
        /// Hides the volume slider.
        /// </summary>
        public void PointerExit() {
            if(routineRunning) StopCoroutine(routine);
            routine = StartCoroutine(FadeOut(fadeOutDuration));
            isActive = false;
            IsCursorOverAnyVolumeToggle = false;
        }

        /// <summary>
        /// OnClick callback.
        /// Mutes or unmutes the associated audio source.
        /// </summary>
        public void OnClick() {
            if(Volume > 0f) {
                volumeBeforeMute = Volume;
                Volume = 0f;
                enabledIcon.SetActive(false);
                disabledIcon.SetActive(true);
            } else {
                Volume = volumeBeforeMute;
                enabledIcon.SetActive(true);
                disabledIcon.SetActive(false);
            }
        }

        private IEnumerator FadeIn(float duration) {
            routineRunning = true;

            float alphaPerSecond = 1f / duration;
            for(float t = volumeText.color.a; t < 1f; t += alphaPerSecond * Time.deltaTime) {
                Color c = volumeText.color;
                c.a = t;
                volumeText.color = c;
                for(int i = 0; i < sliderComponents.Length; i++) {
                    c = sliderComponents[i].color;
                    c.a = t;
                    sliderComponents[i].color = c;
                }
                yield return null;
            }

            Color col = volumeText.color;
            col.a = 1f;
            volumeText.color = col;
            for(int i = 0; i < sliderComponents.Length; i++) {
                col = sliderComponents[i].color;
                col.a = 1f;
                sliderComponents[i].color = col;
            }

            routineRunning = false;
        }

        private IEnumerator FadeOut(float duration) {
            routineRunning = true;

            float alphaPerSecond = 1f / duration;
            for(float t = volumeText.color.a; t > 0f; t -= alphaPerSecond * Time.deltaTime) {
                Color c = volumeText.color;
                c.a = t;
                volumeText.color = c;
                for(int i = 0; i < sliderComponents.Length; i++) {
                    c = sliderComponents[i].color;
                    c.a = t;
                    sliderComponents[i].color = c;
                }
                yield return null;
            }

            Color col = volumeText.color;
            col.a = 0f;
            volumeText.color = col;
            for(int i = 0; i < sliderComponents.Length; i++) {
                col = sliderComponents[i].color;
                col.a = 0f;
                sliderComponents[i].color = col;
            }

            routineRunning = false;
        }
    }
}
