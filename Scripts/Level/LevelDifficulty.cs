/// <summary>
/// WARNING: Do not change this script unless given permission to do so!
/// 
/// Author: Kevin Andersch
/// Date: 28.03.22
/// Version: 1.0
/// </summary>
public enum LevelDifficulty {
    Easy = 2,
    Medium = 4,
    Hard = 6
}
