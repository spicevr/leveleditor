/// <summary>
/// The information about a given level for the purpose of serialization.
/// WARNING: Do not change this script unless given permission!
/// 
/// Author: Kevin Andersch
/// Date: 28.03.22
/// Version: 1.0
/// </summary>
[System.Serializable]
public class LevelData {
    public string Name;
    public LevelDifficulty Difficulty = LevelDifficulty.Easy;
    public int AudioTrackID = -1;
    public int InitialNumberOfLanes = 3;
    public int Bpm = 100;
    public int MinorPerMajorBeatCount = 3;
    public NoteData[] Notes;
}
