using System.IO;
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Handles saving and loading .level files.
/// 
/// Author: Kevin Andersch
/// Date: 28.03.22
/// Version: 1.0
/// </summary>
public class LevelSerializationManager : MonoBehaviour {
    [Header("Reference Setup")]
    [SerializeField] private ArcBreak.LevelEditor.PopupUtility popupUtility;

    [Header("Level Setup")]
    [SerializeField] private Object levelAsset;
    [SerializeField] private LevelData level;

    public LevelData Level { get => level; }
    public bool DecidedToDiscardChanges;

    private const string LEVEL_FOLDER_NAME = "/leveleditor/Level Data/";
    private const string FINISHED_LEVEL_FOLDER_NAME = "/Resources/Levels/";
    private const string FILE_EXTENSION = ".txt";

    // Events
    public static System.Action OnLevelWasSaved;
    public static System.Action OnLevelDataNeedsToUpdate;
    public static System.Action OnLevelWasLoaded;
    public static System.Action OnLevelWasPublished;
    public static System.Action OnTemporaryFileWasResotred;

    private void Awake() {
        LoadLevel();

        if(File.Exists(GetTemporaryFilePath())) {
            popupUtility.ShowPopup("WARNING!", "The level editor was not quit properly last time (most likely by quitting" +
                "\nvia the unity editor) and there were unsaved changes." +
                "\nAlways close the level editor via the appropriate buttons to prevent file corruption!" +
                "\nDo you want to restore those changes or discard them?", "Restore", "Discard", RestoreTemporaryFile, true);
        }
    }

    private void OnApplicationQuit() {
        string temporaryFilePath = GetTemporaryFilePath();

        if(!DecidedToDiscardChanges && levelAsset != null && CheckIfChangesWereMade(levelAsset)) {
            string jsonString = JsonUtility.ToJson(level, true);
            File.WriteAllText(temporaryFilePath, jsonString);
            Debug.LogError("The editor was forced to close (via the unity editor) with unsaved changes. Changes were changed to the temporary .level-file. Never quit the level editor via the unity editor!");
        } else if(File.Exists(temporaryFilePath)) File.Delete(temporaryFilePath);
    }

    /// <summary>
    /// Returns the file path of the currently assigned .level file (in the inspector).
    /// </summary>
    public static string GetAssetPath(Object asset) => Application.dataPath + LEVEL_FOLDER_NAME + asset.name + FILE_EXTENSION;

    /// <summary>
    /// Returns the file path of the currently assigned .level file (in the inspector) for the resources folder.
    /// </summary>
    public static string GetFinishedAssetPath(Object asset) => Application.dataPath + FINISHED_LEVEL_FOLDER_NAME + asset.name + FILE_EXTENSION;

    /// <summary>
    /// Returns the file path for a level in the passed in difficulty with the passed in name.
    /// </summary>
    /// <param name="levelName">The name of the level to load.</param>
    /// <param name="levelDifficulty">The difficulty of the level to load.</param>
    public static string GetLevelFilePath(string levelName, string levelDifficulty) => Application.dataPath + LEVEL_FOLDER_NAME + $"{levelName} ({levelDifficulty})" + FILE_EXTENSION;

    /// <summary>
    /// Returns the file path for a finished level (from the resources folder) in the passed in difficulty with the passed in name.
    /// </summary>
    /// <param name="levelName">The name of the level to load.</param>
    /// <param name="levelDifficulty">The difficulty of the level to load.</param>
    public static string GetFinishedLevelFilePath(string levelName, string levelDifficulty) => Application.dataPath + FINISHED_LEVEL_FOLDER_NAME + $"{levelName} ({levelDifficulty})" + FILE_EXTENSION;

    /// <summary>
    /// Returns the file path to the file where temporary data is stored.
    /// </summary>
    public static string GetTemporaryFilePath() => Application.persistentDataPath + "temp" + FILE_EXTENSION;

    /// <summary>
    /// Returns the default folder name into which .level files are saved.
    /// </summary>
    public static string GetSaveFolderPath() => Application.dataPath + LEVEL_FOLDER_NAME;

    /// <summary>
    /// Returns the default folder name into which .level files are saved.
    /// </summary>
    public static string GetFinishedSaveFolderPath() => Application.dataPath + FINISHED_LEVEL_FOLDER_NAME;

    /// <summary>
    /// Returns the appropriate file name for a .level file.
    /// </summary>
    /// <param name="data">The level data for which the name should be created.</param>
    public static string GetCorrectFileName(LevelData data) => $"{data.Name} ({data.Difficulty})" + FILE_EXTENSION;

    /// <summary>
    /// Returns the correct path to the .level file of the given data.
    /// Specific to unity editor only, since unity uses different asset data paths.
    /// </summary>
    /// <param name="data">The level data for which to retrieve the correct path.</param>
    public static string GetUnitySpecificFilePath(LevelData data) => $"Assets/leveleditor/Levels/{GetCorrectFileName(data)}";

    /// <summary>
    /// Returns the correct path to the .level file of the given data to the resources folder.
    /// Specific to unity editor only, since unity uses different asset data paths.
    /// </summary>
    /// <param name="data">The level data for which to retrieve the correct path.</param>
    public static string GetUnitySpecificFinishedFilePath(LevelData data) => $"Levels/{data.Name} ({data.Difficulty})";

    /// <summary>
    /// Returns the correct path to the .level file of the given data to the resources folder.
    /// Specific to unity editor only, since unity uses different asset data paths.
    /// </summary>
    /// <param name="data">The level data for which to retrieve the correct path.</param>
    public static string GetUnitySpecificFinishedFilePathForAssetDatabase(LevelData data) => "Assets" + FINISHED_LEVEL_FOLDER_NAME + $"{data.Name} ({data.Difficulty})" + FILE_EXTENSION;

    /// <summary>
    /// Returns the correct path to the .level file of the given data to the resources folder.
    /// Specific to unity editor only, since unity uses different asset data paths.
    /// </summary>
    /// <param name="levelName">The name of the level to load.</param>
    /// <param name="levelDifficulty">The difficulty of the level to load.</param>
    public static string GetUnitySpecificFinishedFilePath(string levelName, LevelDifficulty levelDifficulty) => $"Levels/{levelName} ({levelDifficulty})";

    /// <summary>
    /// Checks whether the serialization manager of the scene has a .level-file selected or not.
    /// </summary>
    public bool HasLevelAssetSelected() => levelAsset != null;

    /// <summary>
    /// Serializes the currently held level.
    /// Renames the file to the correct file name if neccessary.
    /// Also sets the level asset to the correct object afterwards.
    /// </summary>
    /// <param name="ignoreOverwriteWarnings">If the saving of the current level would overwrite another already existing one, should we overwrite it?</param>
    public void SaveLevel(ArcBreak.LevelEditor.ErrorMessageDisplayType overwriteError = ArcBreak.LevelEditor.ErrorMessageDisplayType.Ignore) {
        string folderPath = GetFinishedSaveFolderPath();
        string jsonString = JsonUtility.ToJson(level, true);
        string levelName = GetCorrectFileName(Level);
        string filePath = folderPath + levelName;
        bool deleted = false;

        // Rename the file (aka delete the old one and create a new one)
        if((!File.Exists(filePath) && levelAsset)) {
            File.Delete(GetFinishedAssetPath(levelAsset));
            deleted = true;
        }

        if(CheckIfDifferentFileWithSameNameAlreadyExists()) {
            if(overwriteError == ArcBreak.LevelEditor.ErrorMessageDisplayType.Ignore) {
                File.Delete(GetFinishedAssetPath(levelAsset));
                deleted = true;
            } else {
                if(overwriteError == ArcBreak.LevelEditor.ErrorMessageDisplayType.Console) {
                    Debug.LogError("A level would have to overwrite another one in order to be saved. Action cancelled.");
                } else if(overwriteError == ArcBreak.LevelEditor.ErrorMessageDisplayType.Popup) {
                    popupUtility.ShowPopup("Level Already Exists!", "");
                } else {
                    popupUtility.ShowOnScreenMessage("The Level your are trying to save already exists!", isErrorMessage: true);
                }
                return;
            }
        }

        File.WriteAllText(filePath, jsonString);

#if UNITY_EDITOR
        AssetDatabase.Refresh();
        if(!levelAsset || deleted) {
            levelAsset = AssetDatabase.LoadAssetAtPath(GetUnitySpecificFinishedFilePathForAssetDatabase(level), typeof(TextAsset));
            
        }
#endif

        OnLevelWasSaved?.Invoke();
    }

    /// <summary>
    /// Creates a new .level file to serialize data into. Then either a blank slate of a level is created or the
    /// already given values are saved into the new file.
    /// </summary>
    /// <param name="emptyLevelData">Should the already set data be discarded (do you want to start from scratch)?</param>
    public void CreateNewLevel(bool emptyLevelData = false) {
        string folderPath = GetFinishedSaveFolderPath();
        string filePath = folderPath;

        // find the correct name for the level
        if(emptyLevelData) {
            filePath += $"New Level ({System.Enum.GetNames(typeof(LevelDifficulty))[0]}){FILE_EXTENSION}";

            int newLevelIndex = 0;
            while(true) {
                if(!File.Exists(filePath)) break;

                newLevelIndex++;
                filePath = folderPath + $"New Level {newLevelIndex} ({System.Enum.GetNames(typeof(LevelDifficulty))[0]}){FILE_EXTENSION}";
            }

            level = new LevelData();
            level.Name = newLevelIndex == 0 ? "New Level" : $"New Level {newLevelIndex}";
        } else {
            filePath += GetCorrectFileName(level);
        }

        int currentIndex;
        string initialName = SplitLevelNameViaEndingNumeral(level.Name, out currentIndex);
        while(true) {
            if(!emptyLevelData && File.Exists(filePath)) {
                // TODO: tell the user this change was made
                currentIndex++;
                level.Name = $"{initialName} {currentIndex}";
                filePath = folderPath + GetCorrectFileName(level);
            } else break;
        }

        string jsonString = JsonUtility.ToJson(level, true);
        File.WriteAllText(filePath, jsonString);

#if UNITY_EDITOR
        AssetDatabase.Refresh();
        levelAsset = AssetDatabase.LoadAssetAtPath(GetUnitySpecificFinishedFilePathForAssetDatabase(level), typeof(TextAsset));
#endif

        OnLevelWasSaved?.Invoke();
    }

    /// <summary>
    /// Deletes the desired .level-file.
    /// WARNING: straight up deletes the file. This action is not reversable!
    /// </summary>
    /// <param name="levelName">The name of the level to be deleted.</param>
    /// <param name="levelDifficulty">The difficulty of the level to be deleted.</param>
    public void DeleteLevelFile(string levelName, LevelDifficulty levelDifficulty) {
        string filePath = GetFinishedLevelFilePath(levelName, levelDifficulty.ToString());
        File.Delete(filePath);
    }

    /// <summary>
    /// Deletes the temporary .level-file from this machine.
    /// Safe to use, checks whether such a file even exists.
    /// </summary>
    public static void DeleteTemporaryFile() {
        string filePath = GetTemporaryFilePath();
        if(File.Exists(filePath)) File.Delete(filePath);
    }

    /// <summary>
    /// Checks whether the passed in level file is within the resources folder.
    /// </summary>
    /// <param name="levelAsset">The level file to check.</param>
    public static bool CheckIfFilePathIsFinishedLevel(Object levelAsset) {
        string filePath = GetAssetPath(levelAsset);
        return filePath.Contains("Resources");
    }

    /// <summary>
    /// Deserializes the level data at the assigned asset path (in the inspector).
    /// </summary>
    public void LoadLevel() {
        TextAsset asset = levelAsset != null
            ? (TextAsset)Resources.Load(GetUnitySpecificFinishedFilePath(level))
            : (TextAsset)Resources.Load(GetUnitySpecificFinishedFilePath(level.Name, level.Difficulty));
        if(asset == null) return;
        level = JsonUtility.FromJson<LevelData>(asset.text);

        OnLevelWasLoaded?.Invoke();
    }

    public void LoadLevelAndLevelAsset(string levelName, LevelDifficulty levelDifficulty) {
        TextAsset asset = (TextAsset)Resources.Load(GetUnitySpecificFinishedFilePath(levelName, levelDifficulty));
        level = JsonUtility.FromJson<LevelData>(asset.text);
        levelAsset = asset;
    }

    /// <summary>
    /// Returns the level data for the given level, identified by its name and difficulty.
    /// Returns null if no such level exists or the file is corrupted.
    /// </summary>
    /// <param name="levelName">The name of the level to load. Case sensitive.</param>
    /// <param name="levelDifficulty">The difficulty of the level to load.</param>
    public static LevelData LoadLevel(string levelName, LevelDifficulty levelDifficulty) {
        TextAsset asset = (TextAsset)Resources.Load(GetUnitySpecificFinishedFilePath(levelName, levelDifficulty));
        if(asset == null) return null;
        LevelData level = JsonUtility.FromJson<LevelData>(asset.text);

        return level;
    }

    /// <summary>
    /// Returns an array with the (unique) names of all levels in the resources folder.
    /// </summary>
    public static List<string> GetAllLevelNames() {
        Object[] allLevels = Resources.LoadAll("Levels");
        HashSet<string> names = new HashSet<string>();
        foreach(Object item in allLevels) {
            string levelName = item.name;
            levelName = levelName.Split('(')[0];
            levelName = levelName.Remove(levelName.Length - 1);
            if(!names.Contains(levelName)) names.Add(levelName);
        }
        return names.ToList();
    }

    /// <summary>
    /// Tries to find a new .level-file and assign it as the current one.
    /// Returns whether it was successful or not.
    /// </summary>
    /// <param name="levelname">The level name of the new .level-file to assign.</param>
    /// <param name="levelDifficulty">The level difficulty of the new .level-file to assign.</param>
    public bool AssignNewLevelAsset(string levelname, LevelDifficulty levelDifficulty) {
        if(levelAsset) return false;

        //level = LoadLevel(levelname, levelDifficulty);
        TextAsset asset = (TextAsset)Resources.Load(GetUnitySpecificFinishedFilePath(levelname, levelDifficulty));
        if(asset == null) return false;
        level = JsonUtility.FromJson<LevelData>(asset.text);
        return true;
    }

    /// <summary>
    /// Checks whether the passed in .level file has different values than the currently serialized level values.
    /// Returns true if changes were made.
    /// </summary>
    /// <param name="oldLevelAsset">The .level file to compare values to.</param>
    public bool CheckIfChangesWereMade(Object oldLevelAsset) {
        OnLevelDataNeedsToUpdate?.Invoke();

        if(!oldLevelAsset) {
            if(!level.Name.Equals(string.Empty) && !level.Name.StartsWith("New Level")) return true;
            if(!(level.Difficulty != (LevelDifficulty)0)) return true;
            if((level.Notes.Length > 0)) return true;

            return false;
        }

        //string oldFilePath = GetAssetPath(oldLevelAsset);
        string oldFilePath = GetFinishedAssetPath(oldLevelAsset);
        if(!File.Exists(oldFilePath)) return false;

        string fileContent = File.ReadAllText(oldFilePath);
        LevelData oldLevel = JsonUtility.FromJson<LevelData>(fileContent);
        if(!oldLevel.Name.Equals(level.Name)) return true;
        if(!(oldLevel.Difficulty == level.Difficulty)) return true;
        if(!(oldLevel.Notes.Length == level.Notes.Length)) return true;

        for(int i = 0; i < level.Notes.Length; i++) {
            if(!(oldLevel.Notes[i].BeatIndex == level.Notes[i].BeatIndex)) return true;
            if(!(oldLevel.Notes[i].Lane == level.Notes[i].Lane)) return true;
            if(!(oldLevel.Notes[i].Type == level.Notes[i].Type)) return true;
        }

        return false;
    }

    /// <summary>
    /// Returns true if there already exists a .level file that is not the current file being worked on and
    /// if that file has the exact same name as the current one has (aka if it would be overwritten if the
    /// current one got saved).
    /// </summary>
    public bool CheckIfDifferentFileWithSameNameAlreadyExists() {
#if UNITY_EDITOR
        Object fileAtSavePath = AssetDatabase.LoadAssetAtPath(GetUnitySpecificFilePath(level), typeof(DefaultAsset));
        if(!fileAtSavePath) return false;
        return fileAtSavePath != levelAsset;
#else
        return false;
#endif
    }

    /// <summary>
    /// Returns true if the notes of the current level are in ascending order regarding their spawn time.
    /// </summary>
    public bool CheckIfNotesAreInOrder() {
        int time = 0;
        for(int i = 0; i < level.Notes.Length; i++) {
            if(level.Notes[i].BeatIndex < time) return false;
            else time = level.Notes[i].BeatIndex;
        }

        return true;
    }

    /// <summary>
    /// Seperates the given name from its ending numeral.
    /// E.g. "New Level 15" returns "New Level"
    /// </summary>
    /// <param name="levelName">The name to split.</param>
    /// <param name="endingNumeral">The ending numeral (15 in the example).</param>
    /// <returns></returns>
    public static string SplitLevelNameViaEndingNumeral(string levelName, out int endingNumeral) {
        string[] nameParts = levelName.Split(' ');
        if(int.TryParse(nameParts[nameParts.Length - 1], out endingNumeral)) {
            string actualName = nameParts[0];
            for(int i = 1; i < nameParts.Length - 1; i++) {
                actualName += $" {nameParts[i]}";
            }
            return actualName;
        } else {
            endingNumeral = 0;
            return levelName;
        }
    }

    /// <summary>
    /// Restores the temporary file into the currently active file.
    /// Runtime only. Does not check whether that file exists.
    /// Should be used by the Serialization Manager only!
    /// </summary>
    /// <param name="restoreFile">Whether the file should be restored.</param>
    public void RestoreTemporaryFile(bool restoreFile) {
        if(!restoreFile) return;

        string filePath = GetTemporaryFilePath();
        string jsonContent = File.ReadAllText(filePath);
        level = JsonUtility.FromJson<LevelData>(jsonContent);
        OnTemporaryFileWasResotred?.Invoke();
    }
}
